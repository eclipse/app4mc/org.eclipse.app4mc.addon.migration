/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters097.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.base.IPostProcessor;
import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters097.impl.ComponentsModelConverter;
import org.eclipse.app4mc.amalthea.converters097.utils.AmltModelReferencePostProcessor;
import org.eclipse.app4mc.amalthea.converters097.utils.ComponentPortInterfaceCacheBuilder;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class ComponentsModelConverterTest extends Abstract097ConverterTest {

	private static final String INSTANCE = "instance";

	@Parameterized.Parameters(name = "{index}: Test file: {0}")
	public static Collection<Object[]> getTestData() {

		return Collections.singletonList(
				data("/component/model.amxmi", true));
	}

	public ComponentsModelConverterTest(final String xmlFileRelativeLocation, final boolean canExecuteTestCase) {
		super(canExecuteTestCase, xmlFileRelativeLocation);
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class, ComponentsModelConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

	@Override
	protected void modelFileVerificationHook(Document document) {
		super.modelFileVerificationHook(document);

		// verify that the interfaces are created in democar-common
		List<Element> interfaces = HelperUtil.getXpathResult(
				document.getRootElement(),
				"./componentsModel/interfaces",
				Element.class,
				AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), "am"),
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		assertEquals(2, interfaces.size());

		// collect element names
		ArrayList<String> interfaceNames = new ArrayList<>();
		for (Element element : interfaces) {
			interfaceNames.add(element.getAttribute("name").getValue());
		}

		assertEquals(2, interfaceNames.size());
		assertTrue(interfaceNames.contains("x-control"));
		assertTrue(interfaceNames.contains("y-control"));

		// verify that component instance references are updated with parent
		List<Element> connectors = HelperUtil.getXpathResult(
				document.getRootElement(),
				"./componentsModel/components/connectors",
				Element.class,
				AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), "am"),
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (Element element : connectors) {
			if ("".equals(element.getAttribute("name").getValue())) {
				Element child = element.getChild("sourcePort");
				assertNotNull(child);
				assertEquals("Composite1/CompositeComp?type=ComponentInstance", child.getAttribute(INSTANCE).getValue());
				child = element.getChild("targetPort");
				assertNotNull(child);
				assertEquals("Composite1/CompositeComp?type=ComponentInstance", child.getAttribute(INSTANCE).getValue());
			} else if ("con".equals(element.getAttribute("name").getValue())) {
				Element child = element.getChild("sourcePort");
				assertNotNull(child);
				assertEquals("abc%3E%3Edef/inst+1?type=ComponentInstance", child.getAttribute(INSTANCE).getValue());
			}
		}
	}

	@Override
	protected List<ICache> buildCaches() {
		List<ICache> caches = super.buildCaches();

		ComponentPortInterfaceCacheBuilder cacheBuilder = new ComponentPortInterfaceCacheBuilder();
		cacheBuilder.buildCache(this.fileDocumentMapping);
		caches.add(cacheBuilder);

		// only cache the interfaces for one of the files to convert
		assertEquals(1, cacheBuilder.getCacheMap().size());

		Map<String, Object> interfaceMap = cacheBuilder.getCacheMap().values().iterator().next();
		assertEquals(2, interfaceMap.size());
		assertTrue(interfaceMap.containsKey(ComponentPortInterfaceCacheBuilder.INTERFACE_CACHE_KEY));
		assertTrue(interfaceMap.containsKey(ComponentPortInterfaceCacheBuilder.INSTANCE_PARENT_CACHE_KEY));

		Collection<String> interfaceNames = cacheBuilder.getCachedInterfaceNames();
		assertEquals(2, interfaceNames.size());
		assertTrue(interfaceNames.contains("x-control"));
		assertTrue(interfaceNames.contains("y-control"));

		Map<String, String> parentNames = cacheBuilder.getCachedComponentInstanceParentNames();
		assertEquals(2, parentNames.size());
		assertTrue(parentNames.containsKey("CompositeComp"));
		assertTrue(parentNames.containsKey("inst+1"));
		assertEquals("Composite1", parentNames.get("CompositeComp"));
		assertEquals("abc%3E%3Edef", parentNames.get("inst+1"));

		return caches;
	}

	// accessibility updates in test cases are ok here, at runtime the reference is set by the SCR
	@SuppressWarnings("squid:S3011")
	@Override
	protected void setCache(IConverter converter, List<ICache> caches)
			throws IllegalAccessException, NoSuchFieldException {

		if (converter instanceof ComponentsModelConverter) {

			for (ICache iCache : caches) {
				if (iCache instanceof ComponentPortInterfaceCacheBuilder) {
					Field declaredField = ((ComponentsModelConverter) converter).getClass().getDeclaredField("cache");
					declaredField.setAccessible(true);
					declaredField.set(converter, iCache);
					return;
				}
			}
		}
	}

	@Override
	protected List<IPostProcessor> buildPostProcessors() {
		List<IPostProcessor> postProcessors = new ArrayList<>();
		postProcessors.add(new AmltModelReferencePostProcessor());
		return postProcessors;
	}

}
