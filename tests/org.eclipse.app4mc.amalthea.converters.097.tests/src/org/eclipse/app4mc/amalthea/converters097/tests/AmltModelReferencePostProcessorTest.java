/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters097.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.base.IPostProcessor;
import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters097.impl.ComponentsModelConverter;
import org.eclipse.app4mc.amalthea.converters097.utils.AmltModelReferencePostProcessor;
import org.eclipse.app4mc.amalthea.converters097.utils.ComponentPortInterfaceCacheBuilder;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class AmltModelReferencePostProcessorTest extends Abstract097ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test file: {0}")
	public static Collection<Object[]> getTestData() {

		return Collections.singletonList(
				data("/modelreference/model.amxmi", true));
	}

	public AmltModelReferencePostProcessorTest(final String xmlFileRelativeLocation, final boolean canExecuteTestCase) {
		super(canExecuteTestCase, xmlFileRelativeLocation);
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class, ComponentsModelConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

	@Override
	protected void modelFileVerificationHook(Document document) {
		super.modelFileVerificationHook(document);

		// verify that the interfaces are created in democar-common
		List<Element> modules = HelperUtil.getXpathResult(
				document.getRootElement(),
				"./hwModel/structures/structures/structures/modules",
				Element.class,
				AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), "am"),
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		assertEquals(14, modules.size());

		// definition of every modules is an attribute instead of a child
		for (Element element : modules) {
			Attribute definition = element.getAttribute("definition");
			assertNotNull(definition);
			assertFalse("definition attribute contains amlt reference", definition.getValue().startsWith("amlt"));
		}

		modules = HelperUtil.getXpathResult(
				document.getRootElement(),
				"./hwModel/structures/structures/structures/modules/accessElements",
				Element.class,
				AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), "am"),
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		// destination of every accessElements is an attribute instead of a child
		for (Element element : modules) {
			Attribute destination = element.getAttribute("destination");
			assertNotNull(destination);
			assertFalse("destination attribute contains amlt reference", destination.getValue().startsWith("amlt"));
		}
	}

	@Override
	protected List<ICache> buildCaches() {
		List<ICache> caches = super.buildCaches();

		ComponentPortInterfaceCacheBuilder cacheBuilder = new ComponentPortInterfaceCacheBuilder();
		cacheBuilder.buildCache(this.fileDocumentMapping);
		caches.add(cacheBuilder);

		return caches;
	}

	// accessibility updates in test cases are ok here, at runtime the reference is set by the SCR
	@SuppressWarnings("squid:S3011")
	@Override
	protected void setCache(IConverter converter, List<ICache> caches)
			throws IllegalAccessException, NoSuchFieldException {

		if (converter instanceof ComponentsModelConverter) {
			for (ICache iCache : caches) {
				if (iCache instanceof ComponentPortInterfaceCacheBuilder) {
					Field declaredField = ((ComponentsModelConverter) converter).getClass().getDeclaredField("cache");
					declaredField.setAccessible(true);
					declaredField.set(converter, iCache);
					return;
				}
			}
		}
	}

	@Override
	protected List<IPostProcessor> buildPostProcessors() {
		List<IPostProcessor> postProcessors = new ArrayList<>();
		postProcessors.add(new AmltModelReferencePostProcessor());
		return postProcessors;
	}
}
