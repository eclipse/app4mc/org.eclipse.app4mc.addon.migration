/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters090.tests;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IPostProcessor;
import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters090.impl.HwConverter;
import org.eclipse.app4mc.amalthea.converters090.impl.HwReferencesConverter;
import org.eclipse.app4mc.amalthea.converters090.impl.RootElementConverter;
import org.eclipse.app4mc.amalthea.converters090.utils.HWCacheBuilder;
import org.eclipse.app4mc.amalthea.converters090.utils.UpdateCustomPropsPostProcessor;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class HWReferencesConverterTest extends Abstract090ConverterTest {


	@Parameterized.Parameters(name = "{index}: Test file: {0}")
	public static Collection<Object[]> getTestData() {

		return Collections.singletonList(
				data("/hw_references/refsModel.amxmi", true));
	}

	public HWReferencesConverterTest(final String xmlFileRelativeLocation, final boolean canExecuteTestCase) {
		super(canExecuteTestCase, xmlFileRelativeLocation);
	}

	@Test
	public void testConversion() {
		super.testConversion(RootElementConverter.class, HwConverter.class, HwReferencesConverter.class, NamespaceConverter.class);
	}

	private HWCacheBuilder cacheBuilder;

	@Override
	protected List<ICache> buildCaches() {

		final List<ICache> caches = new ArrayList<>();

		this.cacheBuilder = new HWCacheBuilder();
		caches.add(this.cacheBuilder);

		for (final ICache iCache : caches) {
			iCache.buildCache(this.fileDocumentMapping);
		}

		return caches;
	}

	@Override
	protected List<IPostProcessor> buildPostProcessors() {
		final List<IPostProcessor> processors = super.buildPostProcessors();
		SessionLogger logger = new SessionLogger();
		UpdateCustomPropsPostProcessor cppp = new UpdateCustomPropsPostProcessor();
		try {
			Field loggerField = UpdateCustomPropsPostProcessor.class.getDeclaredField("logger");
			loggerField.setAccessible(true);
			loggerField.set(cppp, logger);
			Field cacheField = UpdateCustomPropsPostProcessor.class.getDeclaredField("cache");
			cacheField.setAccessible(true);
			cacheField.set(cppp, this.cacheBuilder);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		processors.add(cppp);
		return processors;
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

}
