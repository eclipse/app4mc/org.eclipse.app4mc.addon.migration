/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters093.tests;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters093.impl.OsConverter;
import org.eclipse.app4mc.amalthea.converters093.impl.SwConverter;
import org.eclipse.app4mc.amalthea.converters093.utils.HWCacheBuilder;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class ActivationConverterTest extends Abstract093ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test file: {0}")
	public static Collection<Object[]> getTestData() {

		return Collections.singletonList(
				data("/activations/default.amxmi", true));
	}

	public ActivationConverterTest(final String xmlFileRelativeLocation, final boolean canExecuteTestCase) {
		super(canExecuteTestCase, xmlFileRelativeLocation);
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class, SwConverter.class,OsConverter.class);
	}

	@Override
	protected List<ICache> buildCaches() {
		List<ICache> caches = super.buildCaches();

		HWCacheBuilder cacheBuilder = new HWCacheBuilder();
		cacheBuilder.buildCache(this.fileDocumentMapping);
		caches.add(cacheBuilder);

		return caches;
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

}
