/*********************************************************************************
 * Copyright (c) 2015-2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters080.tests;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters080.impl.PropertyConstraintsConverter;
import org.eclipse.app4mc.amalthea.converters080.impl.RootElementConverter;
import org.eclipse.app4mc.amalthea.converters080.impl.SwConverter;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class PropertyConstraintsConverterTest extends Abstract080ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test data ID: {0}. Description : {3}")
	public static Collection<Object[]> getTestData() {

		String[] inputFiles = { "/propertyConstraints/propertyConstraints.amxmi" };

		return Collections.singletonList(
			data("Models with PropertyConstraints", true, inputFiles, "Migration of Models containing PropertyConstraints"));
	}

	public PropertyConstraintsConverterTest(String testDataID, boolean canExecuteTestCase, String[] xmlFiles, String description) {
		super(canExecuteTestCase, xmlFiles);
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class, RootElementConverter.class, SwConverter.class, PropertyConstraintsConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

	@Override
	protected void modelFileVerificationHook(final Document document) {
		super.modelFileVerificationHook(document);

		/*
		 * (For further details, check : Bug 514779)
		 */

		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./propertyConstraintsModel/allocationConstraints/hwConstraint");
		xpathBuffer.append("|");
		xpathBuffer.append("./propertyConstraintsModel/mappingConstraints/hwConstraint");


		final List<Element> elements = HelperUtil.getXpathResult(document.getRootElement(), xpathBuffer.toString(),
				Element.class, AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));


		assertEquals("Model migration of \"Property Constraints\" element is not successful in model file : "
				+ document.getBaseURI(), 0, elements.size());

	}

}
