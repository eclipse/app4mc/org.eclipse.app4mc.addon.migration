/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters200.tests;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters200.impl.SchedulerConverter;
import org.eclipse.app4mc.amalthea.converters200.utils.SchedulerCache;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class SchedulerConverterTest extends Abstract200ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test file: {0}")
	public static Collection<Object[]> getTestData() {

		return Arrays.asList(
			data(strings("/schedulers/model1.amxmi", "/schedulers/model2.amxmi", "/schedulers/model3.amxmi"), true),
			data(strings("/schedulers/osek/osek.amxmi"), true),
			data(strings("/schedulers/hierarchical/hierarchicalExample.amxmi"), true),
			data(strings("/schedulers/partitioned_fpp/partitioned_fpp.amxmi"), true),
			data(strings("/schedulers/reservation_based_server/rbs.amxmi"), true));
	}

	public SchedulerConverterTest(final String[] xmlFileRelativeLocation, final boolean canExecuteTestCase) {
		super(canExecuteTestCase, xmlFileRelativeLocation);
	}

	@Override
	protected List<ICache> buildCaches() {
		List<ICache> caches = super.buildCaches();

		SchedulerCache cache = new SchedulerCache();
		cache.buildCache(this.fileDocumentMapping);
		caches.add(cache);

		return caches;
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class, SchedulerConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

}
