/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters100.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters100.impl.EventModelConverter;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class EventModelConverterTest extends Abstract100ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test file: {0}")
	public static Collection<Object[]> getTestData() {

		return Collections.singletonList(
				data("/events/model.amxmi", true));
	}

	public EventModelConverterTest(final String xmlFileRelativeLocation, final boolean canExecuteTestCase) {
		super(canExecuteTestCase, xmlFileRelativeLocation);
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class, EventModelConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

	@Override
	protected void modelFileVerificationHook(Document document) {
		super.modelFileVerificationHook(document);

		// verify that all events are still there
		List<Element> events = HelperUtil.getXpathResult(
				document.getRootElement(),
				"./eventModel/events",
				Element.class,
				AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), "am"),
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		assertEquals(8, events.size());

		// check eventType
		for (Element element : events) {
			String eventType = element.getAttributeValue("eventType");
			assertNotEquals("deadline", eventType);
		}
	}

}
