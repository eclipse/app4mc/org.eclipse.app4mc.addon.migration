/*********************************************************************************
 * Copyright (c) 2015-2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters095.tests;

import java.io.File;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class NamespaceConverterTest extends Abstract095ConverterTest {

	static final String OUTPUT_FOLDER = "namespace";
	static final String OUTPUT_FOLDER_PREFIX = File.separator + OUTPUT_FOLDER + File.separator;

	@BeforeClass
	public static void setup() {
		cleanOutputDirectory(OUTPUT_FOLDER);
		AmaltheaNamespaceRegistry.getAllNamespaces();
	}

	@Parameterized.Parameters(name = "{index}: Test file: {0}")
	public static Collection<Object[]> getTestData() {

		return Collections.singletonList(
				data(OUTPUT_FOLDER_PREFIX + "model.amxmi", true));
	}

	public NamespaceConverterTest(final String xmlFileRelativeLocation, final boolean canExecuteTestCase) {
		super(canExecuteTestCase, xmlFileRelativeLocation);
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

}
