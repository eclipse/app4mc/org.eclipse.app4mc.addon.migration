/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters094.tests.debug;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters094.impl.SwConverter;
import org.jdom2.Document;

// For debugging (run as Java application):
// This class shows how to call several converters without caught exceptions

public class RunModeConditionConverter {

	// only for testing so accessibility updates are ok here
	@SuppressWarnings("squid:S3011")
	public static void main(String[] args) throws Exception {
		final List<ICache> caches = new ArrayList<>();

		final String inputGlobalTestsDirectory = "TestModels/input";
		final String outputGlobalTestsDirectory = "TestModels/output";

		// *** Enter files here ***
		final List<String> inputFiles = Arrays.asList(
				"/swModel/mode-conditions/conditions.amxmi",
				"/swModel/mode-conditions/modes2.amxmi");

		// *** Add converters here ***
		final NamespaceConverter converter1 = new NamespaceConverter();
		final SwConverter converter2 = new SwConverter();

		// set the model version properties via activate()
		HashMap<String, Object> properties = new HashMap<>();
		properties.put(ServiceConstants.INPUT_MODEL_VERSION_PROPERTY, "0.9.3");
		properties.put(ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY, "0.9.4");

		Method activateMethod = converter1.getClass().getDeclaredMethod("activate", Map.class);
		activateMethod.setAccessible(true);
		activateMethod.invoke(converter1, properties);

		activateMethod = converter2.getClass().getDeclaredMethod("activate", Map.class);
		activateMethod.setAccessible(true);
		activateMethod.invoke(converter2, properties);

		HashMap<File, Document> loadedDocumentsMapping = new HashMap<>();

		// Run converters for every file

		for (String file : inputFiles) {
			String inputXmlFilePath = inputGlobalTestsDirectory + file;
			String outputXmlFilePath = outputGlobalTestsDirectory + file;

			File inputFile = new File(inputXmlFilePath).getCanonicalFile();

			Document xmlDoc = HelperUtil.loadFile(inputFile.getAbsolutePath());
			loadedDocumentsMapping.put(inputFile.getCanonicalFile(), xmlDoc);

			// Run converters
			converter1.convert(inputFile, loadedDocumentsMapping, caches);
			converter2.convert(inputFile, loadedDocumentsMapping, caches);

			HelperUtil.saveFile(loadedDocumentsMapping.get(inputFile), outputXmlFilePath, true, true);
		}
	}

}
