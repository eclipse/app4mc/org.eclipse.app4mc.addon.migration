Marker file for activation of profile to skip deployment of test projects.
Needed because the check for "target/surefire.properties" does not work on Jenkins.