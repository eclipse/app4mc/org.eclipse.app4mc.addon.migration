/*********************************************************************************
 * Copyright (c) 2015-2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters091.tests;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.tests.AbstractConverterTest;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class CustomPropertiesConverterTest extends AbstractConverterTest {

	@Parameterized.Parameters(name = "{index}: Test file: {0}")
	public static Collection<Object[]> getTestData() {

		return Collections.singletonList(
				data("/customproperties/model.amxmi", true));
	}

	public CustomPropertiesConverterTest(final String xmlFileRelativeLocation, final boolean canExecuteTestCase) {
		super(canExecuteTestCase, xmlFileRelativeLocation);
	}

	@Override
	public ModelVersion getInputModelVersion() {
		return ModelVersion.VERSION_090;
	}

	@Override
	public ModelVersion getOutputModelVersion() {
		return ModelVersion.VERSION_091;
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

	@Override
	protected void modelFileVerificationHook(final Document document) {
		super.modelFileVerificationHook(document);

		final StringBuilder xpathBuilder = new StringBuilder();

		xpathBuilder.append(".//value[@xsi:type=\"am:TimeObject\"]");
		xpathBuilder.append("|");
		xpathBuilder.append(".//values[@xsi:type=\"am:TimeObject\"]");


		final List<Element> elements = HelperUtil.getXpathResult(
				document.getRootElement(),
				xpathBuilder.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			assertTrue( "CustomProperties migration to 0.9.1 is not successful" , !elements.isEmpty());
	}
}
