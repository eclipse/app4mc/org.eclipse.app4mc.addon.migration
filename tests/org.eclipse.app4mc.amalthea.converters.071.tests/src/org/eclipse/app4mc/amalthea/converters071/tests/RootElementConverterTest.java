/*********************************************************************************
 * Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters071.tests;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters071.impl.RootElementConverter;
import org.jdom2.Document;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class RootElementConverterTest extends Abstract071ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test file: {0}")
	public static Collection<Object[]> getTestData() {

		return Arrays.asList(
				data("/non-amalthea-root-tag/components.amxmi", true),
				data("/non-amalthea-root-tag/config.amxmi", true),
				data("/non-amalthea-root-tag/constraints.amxmi", true),
				data("/non-amalthea-root-tag/eventmodel.amxmi", true),
				data("/non-amalthea-root-tag/hwmodel.amxmi", true),
				data("/non-amalthea-root-tag/mappingmodel.amxmi", true),
				data("/non-amalthea-root-tag/osmodel.amxmi", true),
				data("/non-amalthea-root-tag/propertyconstraints.amxmi", true),
				data("/non-amalthea-root-tag/stimuli.amxmi", true),
				data("/non-amalthea-root-tag/swmodel.amxmi", true));
	}

	public RootElementConverterTest(final String xmlFileRelativeLocation, final boolean canExecuteTestCase) {
		super(canExecuteTestCase, xmlFileRelativeLocation);
	}

	@Override
	public ModelVersion getOutputModelVersion() {
		return ModelVersion.VERSION_070;
	}

	@Test
	public void testConversion() {
		super.testConversion(RootElementConverter.class);
	}


	@Override
	@Test
	public void verification() {

		parseGeneratedXMLFiles();

		final Map<File, Document> map = this.fileDocumentMapping;

		final Set<File> keySet = map.keySet();

		for (final File file1 : keySet) {
			final Document document = map.get(file1);

			final String name = document.getRootElement().getName();

			assertEquals("unable to migrate root element of file to \"Amalthea\" : " + file1.getAbsolutePath(),
					"Amalthea", name);

			assertEquals(
					"unable to copy amalthea namespace to the newly created root element :  \"Amalthea\" : "
							+ file1.getAbsolutePath(),
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_070, "am"),
					document.getRootElement().getNamespace());


		}
	}


}
