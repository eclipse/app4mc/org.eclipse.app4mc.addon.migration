/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters071.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters071.impl.HwConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.PropertyConstraintsConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.RootElementConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.SwConverter;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class PropertyConstraintsConverterTest extends Abstract071ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test file: {0}")
	public static Collection<Object[]> getTestData() {

		return Arrays.asList(
				data("/property-constraints/memorytype_size.amxmi", true),
				data("/property-constraints/pc_frequency_quartz.amxmi", true));
	}

	public PropertyConstraintsConverterTest(final String xmlFileRelativeLocation, final boolean canExecuteTestCase) {
		super(canExecuteTestCase, xmlFileRelativeLocation);
	}

	@Test
	public void testConversion() {
		super.testConversion(RootElementConverter.class, NamespaceConverter.class, SwConverter.class, HwConverter.class,
				PropertyConstraintsConverter.class);
	}


	@Override
	@Test
	public void verification() {

		super.verification();

		verifyQuartzFrequency("./propertyConstraintsModel/mappingConstraints/hwConstraint//quartzes");
	}

	/**
	 * This method is used to verify if the Model Migration is successful w.r.t migration of frequency attribute inside
	 * Quartz Element
	 *
	 * @param xpath
	 *            as Quartz can exists in both HW Model and PropertyConstraints Model, appropriate Xpath is supplied
	 *            each time this method should be invoked
	 */
	protected void verifyQuartzFrequency(final String xpath) {
		for (final Entry<File, Document> entry : this.fileDocumentMapping.entrySet()) {
			final File file = entry.getKey();
			final Document document = entry.getValue();

			/*- Fetching the Quartz elements inside PropertyConstraints model */
			final List<Element> quartzElements = HelperUtil.getXpathResult(document.getRootElement(), xpath,
					Element.class, AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, "am"));

			if (!quartzElements.isEmpty()) {
				for (final Element quartzElement : quartzElements) {
					boolean frequencyFound = false;
					final Element frequencyElement = quartzElement.getChild("frequency");
					if (frequencyElement == null) {
						/*- it is expected that Frequency element exists as a child of Quartz */
						assertNotNull(
								"Model migration is not performed for \"frequency of Quartz\" elements in file : "
										+ file.getName(),
								quartzElement.getAttribute("frequency"));
					}
					else {
						frequencyFound = true;
						final String attributeValue = frequencyElement.getAttributeValue("value");
						assertNotNull(
								"Appropriate value not set for the \"value\" attribute of Quartz " + file.getName(),
								attributeValue);
					}
					assertTrue("Frequency not found inside Quartz element", frequencyFound);
				}
			}
		}
	}
}
