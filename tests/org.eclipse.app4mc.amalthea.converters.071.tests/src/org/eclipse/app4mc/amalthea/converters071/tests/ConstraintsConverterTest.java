/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters071.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters071.impl.ConstraintsConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.HwConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.PropertyConstraintsConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.RootElementConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.SwConverter;
import org.jdom2.Element;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class ConstraintsConverterTest extends Abstract071ConverterTest {

	private static final String AM = "am";
	private static final String XSI = "xsi";

	@Parameterized.Parameters(name = "{index}: Test data ID: {0}. Description : {3}")
	public static Collection<Object[]> getTestData() {

		final String[] inputFiles500501 = {
				"/constraints/500501/constraints_500501.amxmi",
				"/constraints/500501/constraints_2_500501.amxmi" };

		final String[] inputFiles500502 = {
				"/constraints/500502/constraints_500502.amxmi",
				"/constraints/500502/constraints_sw_model.amxmi" };

		final String[] inputFiles500506 = {
				"/constraints/500506/constraints_500506.amxmi" };

		return Arrays.asList(
				data("500501", true, inputFiles500501, "AffinityConstraint change"),
				data("500502", true, inputFiles500502, "RunnableSequencingConstraint change"),
				data("500506", true, inputFiles500506, "Timing Constraints  change"));
	}

	public ConstraintsConverterTest(final String testDataID, final boolean canExecuteTestCase, final String[] xmlFiles,
			final String description) {
		super(canExecuteTestCase, xmlFiles);
	}

	@Test
	public void testConversion() {
		super.testConversion(RootElementConverter.class, NamespaceConverter.class, SwConverter.class,
				PropertyConstraintsConverter.class, HwConverter.class, ConstraintsConverter.class);
	}

	@Override
	@Test
	public void verification() {

		super.verification();

		final Set<File> keySet = this.fileDocumentMapping.keySet();

		for (final File file : keySet) {

			final Element rootElement = this.fileDocumentMapping.get(file).getRootElement();

			/*-
			 * ===Verifying existence of SchedulerSeparationConstraint and SchedulerPairingConstraint content===
			 */
			verifyAffinityConstraints(file, rootElement);

			/*-
			 * ===Verifying existence of OrderConstraint content===
			 */
			verifyOrderConstraints(file, rootElement);

			/*-
			 * ===Verifying existence of EventChainLatencyConstraint content (Age/Reaction), DelayConstraints -> mappingType, SynchronisationConstraint
			===
			 */
			verifyTimingConstraints(file, rootElement);


			/* ============Verifying ProcessRunnableGroupEntry content ========== */

			verifyProcessRunnableGroupEntry(file, rootElement);

			/* ============Verifying ProcessRunnableGroup content ========== */

			verifyProcessRunnableGroup(file, rootElement);
		}
	}

	private void verifyTimingConstraints(final File file, final Element rootElement) {

		final List<Element> ageReactionConstraints = HelperUtil.getXpathResult(
				rootElement,
				"./constraintsModel/timingConstraints[@xsi:type=\"am:AgeConstraint\" or @xsi:type=\"am:ReactionConstraint\"]",
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI),
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, AM));

		assertEquals("Age/Reaction Constraints are not migrated to 0.7.1 format in model file : " + file.getName(),
				0, ageReactionConstraints.size());


		final List<Element> delayConstraints = HelperUtil.getXpathResult(
				rootElement,
				"./constraintsModel/timingConstraints[@xsi:type=\"am:DelayConstraint\" and not(@mappingType)]",
				Element.class, AmaltheaNamespaceRegistry.getGenericNamespace(XSI),
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, AM));

		assertEquals("DelayConstraints are not migrated to 0.7.1 format in model file : " + file.getName(),
				0, delayConstraints.size());


		final List<Element> synchronisationConstraints = HelperUtil.getXpathResult(
				rootElement,
				"./constraintsModel/timingConstraints[@xsi:type=\"am:SynchronisationConstraint\"]",
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI),
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, AM));

		assertEquals(
				"SynchronisationConstraints are not migrated to 0.7.1 format in model file : " + file.getName(),
				0, synchronisationConstraints.size());
	}

	private void verifyOrderConstraints(final File file, final Element rootElement) {

		final List<Element> orderConstraints = HelperUtil.getXpathResult(
				rootElement,
				"./constraintsModel/timingConstraints[@xsi:type=\"am:OrderConstraint\"]",
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI),
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, AM));

		assertEquals("OrderConstraint are not migrated to 0.7.1 format in model file : " + file.getName(),
				0, orderConstraints.size());


	}

	private void verifyProcessRunnableGroupEntry(final File file, final Element rootElement) {

		final List<Element> processRunnableGroupEntries = HelperUtil.getXpathResult(
				rootElement,
				"./constraintsModel/runnableSequencingConstraints/runnableGroups/entries",
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI),
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, AM));

		for (final Element processRunnableGroupEntry : processRunnableGroupEntries) {

			assertTrue(
					"ProcessRunnableGroupEntry is not migrated to 0.7.1 format in model file : " + file.getName(),
					processRunnableGroupEntry.getAttribute("processScope") == null
							|| processRunnableGroupEntry.getChildren("processScope").isEmpty());
		}
	}

	private void verifyProcessRunnableGroup(final File file, final Element rootElement) {

		final List<Element> runnableGroups = HelperUtil.getXpathResult(
				rootElement,
				"./constraintsModel/runnableSequencingConstraints/runnableGroups[@groupingType]",
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI),
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, AM));


		assertEquals("ProcessRunnableGroup is not migrated to 0.7.1 format in model file : " + file.getName(),
				0, runnableGroups.size());
	}

	private void verifyAffinityConstraints(final File file, final Element rootElement) {

		final List<Element> separationConstraints = HelperUtil.getXpathResult(
				rootElement,
				"./constraintsModel/affinityConstraints[@xsi:type=\"am:SchedulerPairingConstraint\" or @xsi:type=\"am:SchedulerSeparationConstraint\" ]",
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI),
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, AM));

		assertEquals(
				"SchedulerPairing and SchedulerSeparation constraints are not migrated to 0.7.1 format in model file : " + file.getName(),
				0, separationConstraints.size());
	}
}
