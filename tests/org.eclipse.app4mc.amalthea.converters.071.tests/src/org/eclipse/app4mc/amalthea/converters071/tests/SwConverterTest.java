/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters071.tests;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters071.impl.HwConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.PropertyConstraintsConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.RootElementConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.SwConverter;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class SwConverterTest extends Abstract071ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test data ID: {0}")
	public static Collection<Object[]> getTestData() {

		final String[] inputFilesSection = { "/sw/AMALTHEA_Democar.amxmi" };

		final String[] inputFilesModeSwitch = {
				"/sw/modeswitch/modeswitch.amxmi",
				"/sw/modeswitch/modeliterals.amxmi",
				"/sw/modeswitch/modeswtich_isrs.amxmi" };

		return Arrays.asList(
				data("Sections : 1", true, inputFilesSection),
				data("ModeSwitchEntry_Scenario : 2", true, inputFilesModeSwitch));
	}

	public SwConverterTest(final String testDataID, final boolean canExecuteTestCase, final String[] xmlFiles) {
		super(canExecuteTestCase, xmlFiles);
	}

	@Test
	public void testConversion() {
		super.testConversion(RootElementConverter.class, NamespaceConverter.class, SwConverter.class, HwConverter.class,
				PropertyConstraintsConverter.class);
	}


	@Override
	@Test
	public void verification() {
		super.verification();

		verifyModeSwitchEntry();
	}

	private void verifyModeSwitchEntry() {

		for (final Document document : this.fileDocumentMapping.values()) {

			final Element rootElement = document.getRootElement();

			final List<Element> xpathResult = HelperUtil.getXpathResult(rootElement,
					".//callGraph//graphEntries//entries[@value]|.//callGraph//graphEntries//entries/value|.//callGraph//graphEntries//entries[@default]",
					Element.class,
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, "am"));

			if (!xpathResult.isEmpty()) {

				final List<Attribute> xpathResult2 = HelperUtil.getXpathResult(xpathResult.get(0),
						"./ancestor::tasks[1]/@name|./ancestor::isrs[1]/@name", Attribute.class);


				assertEquals(
						"Task \"" + xpathResult2.get(0).getValue()
								+ "\" is not migrated completely.. and is not valid as per 0.7.1",
						0, xpathResult.size());
			}
		}
	}

}
