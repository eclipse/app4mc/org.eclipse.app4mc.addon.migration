/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters071.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters071.impl.HwConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.PropertyConstraintsConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.RootElementConverter;
import org.eclipse.app4mc.amalthea.converters071.impl.SwConverter;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class SwSectionConverterTest extends Abstract071ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test data ID: {0}")
	public static Collection<Object[]> getTestData() {

		final String[] inputFiles = {
				"/sw.sections/1.amxmi",
				"/sw.sections/2.amxmi",
				"/sw.sections/3.amxmi" };

		return Collections.singletonList(
				data("Sections-Constraints-Conversion : 1", true, inputFiles));
	}

	public SwSectionConverterTest(final String testDataID, final boolean canExecuteTestCase, final String[] xmlFiles) {
		super(canExecuteTestCase, xmlFiles);
	}

	@Test
	public void testConversion() {
		super.testConversion(RootElementConverter.class, NamespaceConverter.class, SwConverter.class, HwConverter.class,
				PropertyConstraintsConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();

		verifyMemoryElementSectionData();
		verifySectionData();
		verifySectionMappingData();
		verifySectionMappingConstraintsData();
	}

	/**
	 * This method is used to verify if the SectionMappingConstraint data elements are removed based on the model
	 * migration step
	 */
	private void verifySectionMappingConstraintsData() {

		for (final Document document : this.fileDocumentMapping.values()) {

			final Element rootElement = document.getRootElement();

			final StringBuilder xpathBuffer = new StringBuilder();
			xpathBuffer.append("./propertyConstraintsModel/mappingConstraints[@xsi:type=\"am:SectionMappingConstraint\"]");

			final List<Element> sectionMappingElements = HelperUtil.getXpathResult(
					rootElement,
					xpathBuffer.toString(),
					Element.class,
					AmaltheaNamespaceRegistry.getGenericNamespace("xsi"),
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, "am"));

			if (sectionMappingElements != null && !sectionMappingElements.isEmpty()) {
				assertTrue(
						"SectionMappingConstraints data is not migrated completely.. and is not valid as per 0.7.1",
						false);
			}
		}
	}

	/**
	 * This method is used to verify if there exists SectionMapping elements inside the Mapping model <br>
	 * As per Model Migration to 0.7.1 : All the SectionMapping elements should be removed from the Mapping Model
	 */
	private void verifySectionMappingData() {


		for (final Document document : this.fileDocumentMapping.values()) {

			final Element rootElement = document.getRootElement();

			final StringBuilder xpathBuffer = new StringBuilder();

			xpathBuffer.append("./mappingModel/mapping[@xsi:type=\"am:SectionMapping\"]");

			final List<Element> sectionMappingElements = HelperUtil.getXpathResult(
					rootElement,
					xpathBuffer.toString(),
					Element.class,
					AmaltheaNamespaceRegistry.getGenericNamespace("xsi"),
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, "am"));

			if (sectionMappingElements != null && !sectionMappingElements.isEmpty()) {
				assertTrue("SectionMapping data is not migrated completely.. and is not valid as per 0.7.1",
						false);
			}
		}
	}

	/**
	 * This method is used to verify if the section content is migrated successfully
	 */
	private void verifySectionData() {

		boolean checkData = false;

		for (final Document document : this.fileDocumentMapping.values()) {

			final Element rootElement = document.getRootElement();

			final List<Element> xpathResult = HelperUtil.getXpathResult(
					rootElement,
					".//swModel/sections",
					Element.class,
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, "am"));

			for (final Element element : xpathResult) {

				checkData = true;

				final List<Element> labels = element.getChildren("labels");
				final List<Element> runnables = element.getChildren("runEntities");
				final Element dataSize = element.getChild("size");

				if (!labels.isEmpty() || !runnables.isEmpty() || dataSize != null) {
					assertTrue("Section " + element.getAttributeValue("name")
							+ " is not migrated completely.. and is not valid as per 0.7.1", false);
				}
			}
		}
		assertTrue("No section info found in the provided test data", checkData);
	}

	/**
	 * This method is used to verify whether model migration of Section content is successful i.e. valid section is
	 * associated to Label/Runnable elements
	 */

	public void verifyMemoryElementSectionData() {

		final Map<String, String> labelToExpectedSectionMap = new HashMap<>();

		labelToExpectedSectionMap.put("label1", "Section1");
		labelToExpectedSectionMap.put("label2", "Section2");
		labelToExpectedSectionMap.put("label3", "Section6");
		labelToExpectedSectionMap.put("label6", "Section6");

		for (final Entry<String, String> entry : labelToExpectedSectionMap.entrySet()) {
			final String memoryElementName = entry.getKey();
			final String sectionName = entry.getValue();

			boolean checkData = false;

			for (final Document document : this.fileDocumentMapping.values()) {

				final Element rootElement = document.getRootElement();

				final List<Element> xpathResult = HelperUtil.getXpathResult(
						rootElement,
						".//swModel/labels[@name=\"" + memoryElementName + "\"]",
						Element.class,
						AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_071, "am"));

				if (!xpathResult.isEmpty()) {

					checkData = true;
					final Element element = xpathResult.get(0);

					if (!(("label6".equals(memoryElementName)) || ("label1".equals(memoryElementName))
							|| ("label2".equals(memoryElementName)))) {

						final Element child = element.getChild("section");

						if (!(child != null && ("amlt:/#" + sectionName + "?type=Section").equals(
								child.getAttributeValue("href")))) {
							assertTrue("Migration is not valid for Label : " + memoryElementName
									+ ", as the correponding Section : "
									+ sectionName + " is not associated to it",
									false);
						}
					} else {
						/*-
						 * This is a case where label and section are defined in the same file
						 *  e.g: label6 is a special case -> as both Section6 and label6 are defined in the same file
						 */

						final String attributeValue = element.getAttributeValue("section");

						assertEquals(
								"Migration is not valid for Label : " + memoryElementName
										+ ", as the correponding Section : "
										+ sectionName + " is not associated to it",
								sectionName + "?type=Section",
								attributeValue);
					}
				}
			}

			assertTrue(
					"Label : " + memoryElementName
							+ " is not found in the migrated data. Unable to test the migration of section information",
					checkData);
		}
	}
}
