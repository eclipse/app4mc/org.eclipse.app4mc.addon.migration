/**
 ********************************************************************************
 * Copyright (c) 2019-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common.tests;

import static org.junit.Assert.assertEquals;

import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.jdom2.Namespace;
import org.junit.Test;

public class AmaltheaNamespaceRegistryTest {

	private static final String HTTP_AMALTHEA_0_7_0 = "http://app4mc.eclipse.org/amalthea/0.7.0";
	private static final String HTTP_AMALTHEA_0_7_1 = "http://app4mc.eclipse.org/amalthea/0.7.1";
	private static final String HTTP_AMALTHEA_0_7_2 = "http://app4mc.eclipse.org/amalthea/0.7.2";
	private static final String HTTP_AMALTHEA_0_8_0 = "http://app4mc.eclipse.org/amalthea/0.8.0";
	private static final String HTTP_AMALTHEA_3_0_0 = "http://app4mc.eclipse.org/amalthea/3.0.0";

	@Test
	public void shouldReturnNamespacesBefore() {
		Namespace[] allNamespacesBefore = AmaltheaNamespaceRegistry.getAllNamespacesBefore(ModelVersion.VERSION_080, false, false);

		assertEquals(3, allNamespacesBefore.length);

		assertEquals("am", allNamespacesBefore[0].getPrefix());
		assertEquals(HTTP_AMALTHEA_0_7_0, allNamespacesBefore[0].getURI());
		assertEquals("am", allNamespacesBefore[1].getPrefix());
		assertEquals(HTTP_AMALTHEA_0_7_1, allNamespacesBefore[1].getURI());
		assertEquals("am", allNamespacesBefore[2].getPrefix());
		assertEquals(HTTP_AMALTHEA_0_7_2, allNamespacesBefore[2].getURI());
	}

	@Test
	public void shouldReturnNamespacesBeforeInclude() {
		Namespace[] allNamespacesBefore = AmaltheaNamespaceRegistry.getAllNamespacesBefore(ModelVersion.VERSION_080, true, false);

		assertEquals(4, allNamespacesBefore.length);

		assertEquals("am", allNamespacesBefore[0].getPrefix());
		assertEquals(HTTP_AMALTHEA_0_7_0, allNamespacesBefore[0].getURI());
		assertEquals("am", allNamespacesBefore[1].getPrefix());
		assertEquals(HTTP_AMALTHEA_0_7_1, allNamespacesBefore[1].getURI());
		assertEquals("am", allNamespacesBefore[2].getPrefix());
		assertEquals(HTTP_AMALTHEA_0_7_2, allNamespacesBefore[2].getURI());
		assertEquals("am", allNamespacesBefore[3].getPrefix());
		assertEquals(HTTP_AMALTHEA_0_8_0, allNamespacesBefore[3].getURI());
	}

	@Test
	public void shouldReturnNamespacesBeforeIncludeGeneric() {
		Namespace[] allNamespacesBefore = AmaltheaNamespaceRegistry.getAllNamespacesBefore(ModelVersion.VERSION_080, true, true);

		assertEquals(6, allNamespacesBefore.length);

		assertEquals("xsi", allNamespacesBefore[0].getPrefix());
		assertEquals("http://www.w3.org/2001/XMLSchema-instance", allNamespacesBefore[0].getURI());

		assertEquals("xmi", allNamespacesBefore[1].getPrefix());
		assertEquals("http://www.omg.org/XMI", allNamespacesBefore[1].getURI());

		assertEquals("am", allNamespacesBefore[2].getPrefix());
		assertEquals(HTTP_AMALTHEA_0_7_0, allNamespacesBefore[2].getURI());
		assertEquals("am", allNamespacesBefore[3].getPrefix());
		assertEquals(HTTP_AMALTHEA_0_7_1, allNamespacesBefore[3].getURI());
		assertEquals("am", allNamespacesBefore[4].getPrefix());
		assertEquals(HTTP_AMALTHEA_0_7_2, allNamespacesBefore[4].getURI());
		assertEquals("am", allNamespacesBefore[5].getPrefix());
		assertEquals(HTTP_AMALTHEA_0_8_0, allNamespacesBefore[5].getURI());
	}

	@Test
	public void getModelVersion() {
		Namespace namespace = Namespace.getNamespace("am", HTTP_AMALTHEA_3_0_0);
		ModelVersion version = AmaltheaNamespaceRegistry.getModelVersion(namespace);
		
		assertEquals(ModelVersion.VERSION_300, version);
	}

}
