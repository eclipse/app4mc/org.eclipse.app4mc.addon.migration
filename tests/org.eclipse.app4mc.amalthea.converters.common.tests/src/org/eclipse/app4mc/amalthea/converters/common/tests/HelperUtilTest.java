/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common.tests;

import static org.junit.Assert.assertEquals;

import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.junit.Test;

public class HelperUtilTest {

	private static final String ABC_ABC = "abc?abc";
	private static final String ABC_DEF = "abc?def";

	@Test
	public void checkTrimStartAndEnd() {
		assertEquals(ABC_DEF, HelperUtil.trimStartAndEnd(ABC_DEF, null, null));
		assertEquals("c?abc", HelperUtil.trimStartAndEnd(ABC_ABC, "ab", null));
		assertEquals("abc?ab", HelperUtil.trimStartAndEnd(ABC_ABC, null, "c"));

		assertEquals(ABC_DEF, HelperUtil.trimStartAndEnd(ABC_DEF, "", ""));
		assertEquals("c?abc", HelperUtil.trimStartAndEnd(ABC_ABC, "ab", ""));
		assertEquals("abc?ab", HelperUtil.trimStartAndEnd(ABC_ABC, "", "c"));

		assertEquals(ABC_ABC, HelperUtil.trimStartAndEnd(ABC_ABC, "b", "b"));
		assertEquals(ABC_ABC, HelperUtil.trimStartAndEnd(ABC_ABC, "x", "y"));

		assertEquals("c?a", HelperUtil.trimStartAndEnd(ABC_ABC, "ab", "bc"));
		assertEquals("efg", HelperUtil.trimStartAndEnd("abcdefg", "abcd", "defg"));
	}

}
