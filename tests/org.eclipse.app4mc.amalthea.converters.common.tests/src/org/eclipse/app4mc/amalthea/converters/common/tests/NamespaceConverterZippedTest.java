/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.MigrationHelper;
import org.eclipse.app4mc.amalthea.converters.common.MigrationInputFile;
import org.eclipse.app4mc.amalthea.converters.common.MigrationSettings;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.junit.Test;

public class NamespaceConverterZippedTest {

	public static final String GLOBAL_TEST_INPUT_DIRECTORY = "./TestModels/input";

	public static final Path ZIP_FILE = Paths.get("./TestModels/input/zipped/model.amxmi").toAbsolutePath();
	public static final Path NON_ZIP_FILE = Paths.get("./TestModels/input/namespace/model.amxmi").toAbsolutePath();

	@Test
	public void shouldCheckZipFile() {
		assertTrue(MigrationHelper.isZipFile(ZIP_FILE.toFile()));
		assertFalse(MigrationHelper.isZipFile(NON_ZIP_FILE.toFile()));
		assertFalse(MigrationHelper.isZipFile(null));
	}

	@Test
	public void shouldGetModelVersionFromZipFile() {
		String version = MigrationHelper.getModelVersion(ZIP_FILE.toFile());
		assertEquals(ModelVersion.VERSION_096.getVersion(), version);
	}

	@Test
	public void shouldGetModelVersionFromModelFile() {
		String version = MigrationHelper.getModelVersion(NON_ZIP_FILE.toFile());
		assertEquals(ModelVersion.VERSION_096.getVersion(), version);
	}

	@Test
	public void shouldGetInvalidModelVersionFromNull() {
		String version = MigrationHelper.getModelVersion(null);
		assertEquals(MigrationHelper.INVALID, version);
	}

	@Test
	public void shouldGetInvalidModelVersionFromNonExisting() {
		String version = MigrationHelper.getModelVersion(new File("./TestModels/input/zipped/model_xxx.amxmi"));
		assertEquals(MigrationHelper.INVALID, version);
	}

	@Test
	public void shouldGetInvalidModelVersionFromWrongFileExtension() {
		String version = MigrationHelper.getModelVersion(new File("./TestModels/input/invalid/model.amx"));
		assertEquals(MigrationHelper.INVALID, version);
	}

	@Test
	public void shouldGetInvalidModelVersion() {
		String version = MigrationHelper.getModelVersion(new File("./TestModels/input/invalid/model_invalid_version.amxmi"));
		assertEquals(MigrationHelper.INVALID, version);
	}

	@Test
	public void shouldTemporaryUnzipFile() throws IOException {
		MigrationInputFile migModelFile = new MigrationInputFile();
		migModelFile.setFile(ZIP_FILE.toFile().getCanonicalFile(), ZIP_FILE.getParent().toFile());

		// as the input is a zip file we expect the temporary unzipped file
		File unzipped = migModelFile.getFile();
		assertNotNull(unzipped);
		assertTrue(unzipped.exists());
		assertEquals("unzipped_model.amxmi", unzipped.getName());

		unzipped.deleteOnExit();
	}

	@Test
	public void shouldPopulateModel() throws Exception {
		MigrationInputFile input = null;
		try (MigrationSettings migrationSettings = new MigrationSettings()) {
			migrationSettings.setProject(ZIP_FILE.getParent().toFile());
			migrationSettings.setMigrationModelVersion(ModelVersion.VERSION_097.getVersion());

			List<MigrationInputFile> inputFiles = MigrationHelper.populateModels(Arrays.asList(ZIP_FILE.toFile()), migrationSettings);
			migrationSettings.getMigModelFiles().addAll(inputFiles);

			assertEquals(1, inputFiles.size());

			input = inputFiles.get(0);
			assertTrue(input.isZipFile());
			assertEquals(ModelVersion.VERSION_096.getVersion(), input.getModelVersion());
			assertEquals("model.amxmi", input.getOriginalFile().getName());

			// the file should not yet be unzipped, only call to getFile() should unzip lazily
			Path unzipped = Paths.get("./TestModels/input/zipped/unzipped_model.amxmi").toAbsolutePath();
			assertFalse(Files.exists(unzipped));

			assertEquals("unzipped_model.amxmi", input.getFile().getName());

			assertTrue(Files.exists(unzipped));
			assertTrue(input.getFile().exists());
		}

		// test that disposing really deletes the temporary created files
		assertFalse(input.getFile().exists());
	}

	@Test
	public void shouldCreateBackupOfZip() throws Exception {
		MigrationSettings migrationSettings = new MigrationSettings();
		migrationSettings.setProject(ZIP_FILE.getParent().toFile());
		migrationSettings.setMigrationModelVersion(ModelVersion.VERSION_097.getVersion());

		List<MigrationInputFile> inputFiles = MigrationHelper.populateModels(Arrays.asList(ZIP_FILE.toFile()), migrationSettings);

		assertEquals(1, inputFiles.size());

		MigrationInputFile input = inputFiles.get(0);

		String backupFileName = MigrationHelper.getBackupFileName(input);

		assertEquals("model_0.9.6.amxmi", backupFileName);

		assertTrue(MigrationHelper.createBackupFile(input));

		Path backupPath = Paths.get(ZIP_FILE.getParent().toString(), backupFileName);
		File backupFile = backupPath.toFile();
		assertTrue(backupFile.exists());
		assertTrue(MigrationHelper.isZipFile(backupFile));

		backupFile.deleteOnExit();
		input.getFile().deleteOnExit();
	}
}
