/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.junit.Test;

public class ModelVersionTest {

	@Test
	public void shouldGetAllVersionsBefore090() {
		ModelVersion[] before090 = ModelVersion.getVersionsBefore(ModelVersion.VERSION_090, false);
		assertEquals(7, before090.length);

		assertEquals(ModelVersion.VERSION_070, before090[0]);
		assertEquals(ModelVersion.VERSION_071, before090[1]);
		assertEquals(ModelVersion.VERSION_072, before090[2]);
		assertEquals(ModelVersion.VERSION_080, before090[3]);
		assertEquals(ModelVersion.VERSION_081, before090[4]);
		assertEquals(ModelVersion.VERSION_082, before090[5]);
		assertEquals(ModelVersion.VERSION_083, before090[6]);
	}

	@Test
	public void shouldGetAllVersionsBefore090Include() {
		ModelVersion[] before090 = ModelVersion.getVersionsBefore(ModelVersion.VERSION_090, true);
		assertEquals(8, before090.length);

		assertEquals(ModelVersion.VERSION_070, before090[0]);
		assertEquals(ModelVersion.VERSION_071, before090[1]);
		assertEquals(ModelVersion.VERSION_072, before090[2]);
		assertEquals(ModelVersion.VERSION_080, before090[3]);
		assertEquals(ModelVersion.VERSION_081, before090[4]);
		assertEquals(ModelVersion.VERSION_082, before090[5]);
		assertEquals(ModelVersion.VERSION_083, before090[6]);
		assertEquals(ModelVersion.VERSION_090, before090[7]);
	}

	@Test
	public void shouldIdentifyValidVersion() {
		assertTrue(ModelVersion.isValidVersion("0.7.0"));
		assertTrue(ModelVersion.isValidVersion("0.8.1"));
		assertTrue(ModelVersion.isValidVersion("0.9.6"));
	}

	@Test
	public void shouldIdentifyInvalidVersion() {
		assertFalse(ModelVersion.isValidVersion("0.1.0"));
		assertFalse(ModelVersion.isValidVersion("0.6.5"));
	}

}
