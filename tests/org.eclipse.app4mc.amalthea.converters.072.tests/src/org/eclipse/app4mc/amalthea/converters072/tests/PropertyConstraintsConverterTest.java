/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters072.tests;

import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters072.impl.CommonElementsConverter;
import org.eclipse.app4mc.amalthea.converters072.impl.PropertyConstraintsConverter;
import org.eclipse.app4mc.amalthea.converters072.impl.SwConverter;
import org.eclipse.app4mc.amalthea.converters072.utils.HwElementsCacheBuilder;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class PropertyConstraintsConverterTest extends Abstract072ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test file: {0}")
	public static Collection<Object[]> getTestData() {

		return Arrays.asList(
				data("/property-constraints/case1/AMALTHEA_Democar_MappingExample.amxmi", true),
				data("/property-constraints/case2/AMALTHEA_Democar_MappingExample-without-hwmodel.amxmi", true));
	}

	public PropertyConstraintsConverterTest(final String xmlFileRelativeLocation, final boolean canExecuteTestCase) {
		super(canExecuteTestCase, xmlFileRelativeLocation);
	}

	@Override
	protected List<ICache> buildCaches() {
		List<ICache> caches = super.buildCaches();

		HwElementsCacheBuilder cacheBuilder = new HwElementsCacheBuilder();
		cacheBuilder.buildCache(this.fileDocumentMapping);
		caches.add(cacheBuilder);

		return caches;
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class, SwConverter.class, CommonElementsConverter.class,
				PropertyConstraintsConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();

		verifyMemTypeAndCoreTypeMigration();

		verifyCoreElementsMigration();

		verifyMemoryElementsMigration();
	}

	private void verifyMemoryElementsMigration() {

		final StringBuilder xpathBuffer = new StringBuilder();
		xpathBuffer.append("./propertyConstraintsModel//firstConstraint[@xsi:type=\"am:HwMemoryProperty\"]/memory");
		xpathBuffer.append("|");
		xpathBuffer.append("./propertyConstraintsModel//secondConstraint[@xsi:type=\"am:HwMemoryProperty\"]/memory");
		xpathBuffer.append("|");
		xpathBuffer.append("./propertyConstraintsModel//hwConstraint[@xsi:type=\"am:HwMemoryProperty\"]/memory");


		for (final Document document : this.fileDocumentMapping.values()) {

			final Element rootElement = document.getRootElement();

			final List<Element> xpathResult = HelperUtil.getXpathResult(
					rootElement,
					xpathBuffer.toString(),
					Element.class,
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_072, "am"),
					AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			for (final Element element : xpathResult) {
				assertNotNull(
						"Memory element content migration is not successful --> as PropertyConstraints model is still having \"Memory definition\"-> instead of references. For further analysis check the migrated model file :"
								+ document.getBaseURI(),
						element.getAttribute("href"));
			}
		}
	}

	private void verifyCoreElementsMigration() {

		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./propertyConstraintsModel//firstConstraint[@xsi:type=\"am:HwCoreProperty\"]/core");
		xpathBuffer.append("|");
		xpathBuffer.append("./propertyConstraintsModel//secondConstraint[@xsi:type=\"am:HwCoreProperty\"]/core");
		xpathBuffer.append("|");
		xpathBuffer.append("./propertyConstraintsModel//hwConstraint[@xsi:type=\"am:HwCoreProperty\"]/core");


		for (final Document document : this.fileDocumentMapping.values()) {

			final Element rootElement = document.getRootElement();

			final List<Element> xpathResult = HelperUtil.getXpathResult(
					rootElement,
					xpathBuffer.toString(),
					Element.class,
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_072, "am"),
					AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			for (final Element element : xpathResult) {
				assertNotNull(
						"Core element content migration is not successful --> as PropertyConstraints model is still having \"Core definition\"-> instead of references. For further analysis check the migrated model file :"
								+ document.getBaseURI(),
						element.getAttribute("href"));
			}
		}
	}

	private void verifyMemTypeAndCoreTypeMigration() {
		for (final Document document : this.fileDocumentMapping.values()) {

			final Element rootElement = document.getRootElement();

			final List<Element> xpathResult = HelperUtil.getXpathResult(
					rootElement,
					"./propertyConstraintsModel/coreTypeDefinitions|./propertyConstraintsModel/memoryTypeDefinitions",
					Element.class,
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_072, "am"));

			if (!xpathResult.isEmpty()) {

				Assert.assertEquals(
						"MemType and CoreType content migration is not successful as PropertyConstraints model is still having them. For further analysis check the migrated model file :"
								+ document.getBaseURI(),
						0, xpathResult.size());
			}
		}
	}

}
