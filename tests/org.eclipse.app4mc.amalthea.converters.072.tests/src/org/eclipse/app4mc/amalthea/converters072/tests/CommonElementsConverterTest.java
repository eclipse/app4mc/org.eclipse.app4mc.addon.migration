/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters072.tests;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters072.impl.CommonElementsConverter;
import org.eclipse.app4mc.amalthea.converters072.impl.OSConverter;
import org.eclipse.app4mc.amalthea.converters072.impl.StimuliConverter;
import org.eclipse.app4mc.amalthea.converters072.impl.SwConverter;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class CommonElementsConverterTest extends Abstract072ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test file: {0}")
	public static Collection<Object[]> getTestData() {

		return Collections.singletonList(
				data("/common-elements/common-elements.amxmi", true));
	}

	public CommonElementsConverterTest(final String xmlFileRelativeLocation, final boolean canExecuteTestCase) {
		super(canExecuteTestCase, xmlFileRelativeLocation);
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class, OSConverter.class, SwConverter.class, StimuliConverter.class,
				CommonElementsConverter.class);

		verifyTagElements();
	}


	private void verifyTagElements() {

		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./componentsModel/tags");
		xpathBuffer.append("|");
		xpathBuffer.append("./hwModel/tags");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/tags");


		for (final Document document : this.fileDocumentMapping.values()) {

			final Element rootElement = document.getRootElement();

			final List<Element> xpathResult = HelperUtil.getXpathResult(
					rootElement,
					xpathBuffer.toString(),
					Element.class,
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_072, "am"),
					AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			if (!xpathResult.isEmpty()) {

				Assert.assertEquals(
						"Tag elements content migration is not successful --> as still it contains \"Tag elements inside -> componentsModel or hwModel or swModel \". For further analysis check the migrated model file :"
								+ document.getBaseURI(),
						0, xpathResult.size());

			}


		}


	}


	@Override
	@Test
	public void verification() {
		super.verification();


	}


}
