/*********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters081.tests;

import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.tests.AbstractConverterTest;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters081.utils.ConstraintElementsCacheBuilder;

abstract class Abstract081ConverterTest extends AbstractConverterTest {

	protected Abstract081ConverterTest(boolean canExecuteTestCase, String... xmlFilesRelative) {
		super(canExecuteTestCase, xmlFilesRelative);
	}

	@Override
	protected List<ICache> buildCaches() {
		List<ICache> caches = super.buildCaches();

		ConstraintElementsCacheBuilder cacheBuilder = new ConstraintElementsCacheBuilder();
		cacheBuilder.buildCache(this.fileDocumentMapping);
		caches.add(cacheBuilder);

		return caches;
	}

	@Override
	public ModelVersion getInputModelVersion() {
		return ModelVersion.VERSION_080;
	}

	@Override
	public ModelVersion getOutputModelVersion() {
		return ModelVersion.VERSION_081;
	}

}
