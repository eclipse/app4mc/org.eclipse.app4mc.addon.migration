/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters081.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters081.impl.ConstraintsConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.RootElementConverter;
import org.eclipse.app4mc.amalthea.converters081.utils.ConstraintElementsCacheBuilder;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class ConstraintsConverterTest extends Abstract081ConverterTest {

	private static final String VALUE = "value";
	private static final String VALUES = "values";
	private static final String SCOPE = "scope";
	private static final String CUSTOM_PROPERTIES = "customProperties";
	private static final String EVENT_CHAIN = "eventChain";
	private static final String EVENT_CHAIN_AMXMI = "eventChain.amxmi";
	private static final String UNABLE_TO_MIGRATE_SUB_EVENT_CHAIN_ELEMENT_REFERENCES_AS_CUSTOM_PROPERTY = "Unable to migrate Sub event chain element references as CustomProperty";

	private static final Namespace XSI_NAMESPACE = AmaltheaNamespaceRegistry.getGenericNamespace("xsi");

	@Parameterized.Parameters(name = "{index}: Test data ID: {0}. Description : {3}")
	public static Collection<Object[]> getTestData() {

		final String[] inputFiles = {
				"/eventChain/default.amxmi",
				"/eventChain/eventChain.amxmi",
				"/eventChain/democar.amxmi" };

		return Collections.singletonList(
			data("Models with Constraint Model", true, inputFiles, "Migration of Models containing Constraints Model"));
	}

	public ConstraintsConverterTest(final String testDataID, final boolean canExecuteTestCase, final String[] xmlFiles,
			final String description) {
		super(canExecuteTestCase, xmlFiles);
	}

	@Override
	protected List<ICache> buildCaches() {
		List<ICache> caches = super.buildCaches();

		ConstraintElementsCacheBuilder cacheBuilder = new ConstraintElementsCacheBuilder();
		cacheBuilder.buildCache(this.fileDocumentMapping);
		caches.add(cacheBuilder);

		return caches;
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class, RootElementConverter.class, ConstraintsConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

	@Override
	protected void modelFileVerificationHook(final Document document) {
		super.modelFileVerificationHook(document);

		/* ============= EventChain =================== */

		eventChainTest1(document);

		/* ========EventChainSynchronizationConstraints ========== */

		timingConstraintsTest1(document);

		timingConstraintsTest2(document);

		timingConstraintsTest3(document);

		timingConstraintstest5(document);

		/* ===========LatencyConstraints ================= */

		timingConstraintsTest4(document);

		// assertTrue("Model migration of \"OS Model\" element is not successful in
		// model file : " +
		// document.getBaseURI(),
		// elements.isEmpty());
	}

	/**
	 * This is the scenario in which EventChainReference element refers to the sub
	 * EventChain elements present in different model file.
	 *
	 * As a part of migration, following points should be considered:<br>
	 * -- sub EventChain references should not be referred anywhere (as it is not
	 * valid to have them as per the change in meta model from 0.8.1).<br>
	 * -- Removed reference string of sub event chain is stored as CustomProperty,
	 * inorder to avoid data loss
	 *
	 * @param document
	 */
	private void eventChainTest1(final Document document) {
		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append(".//eventChains[ @name=\"eventChain_4\"]");

		final List<Element> elements = HelperUtil.getXpathResult(document.getRootElement(), xpathBuffer.toString(),
				Element.class, XSI_NAMESPACE);

		if (document.getBaseURI().endsWith(EVENT_CHAIN_AMXMI)) {

			/*-
			 * EventChain migrated content:
			 *
			 *  <eventChains name="eventChain_4">
					<strands xsi:type="am:EventChainContainer">
						<eventChain name="strands_subeventchain_2" stimulus="label_event_2?type=LabelEvent" response="label_event_1?type=LabelEvent">
									<segments xsi:type="am:EventChainContainer">
										<eventChain name="eventChain_3" />
									</segments>
									<segments xsi:type="am:EventChainContainer">
										<eventChain>
										  <segments xsi:type="am:EventChainReference">
										    <customProperties key="eventChain">
										      <value xsi:type="am:StringObject" value="amlt:/#eventChain_from_second_file_sub_element?type=EventChain" />
										    </customProperties>
										  </segments>
										</eventChain>
									</segments>
							<strands xsi:type="am:EventChainReference" eventChain="eventChain_3?type=EventChain" />
						</eventChain>
					</strands>
			</eventChains>
			 *
			 */

			assertEquals("EventChain element should exists with name : eventChain_4", 1, elements.size());

			final Element eventChainElement = elements.get(0);

			final Element eventChainReferenceElement = eventChainElement.getChild("strands").getChild(EVENT_CHAIN)
					.getChildren("segments").get(1).getChild(EVENT_CHAIN).getChild("segments");

			/*- verification of EventChain existence inside EventChainReference */

			assertNull(
					"sub EventChain element still exists inside EventChainReference element (which is not allowed as per 0.8.1), is not migrated",
					eventChainReferenceElement.getChild(EVENT_CHAIN));

			/*- verification of EventChain existence inside EventChainReference */

			assertEquals(UNABLE_TO_MIGRATE_SUB_EVENT_CHAIN_ELEMENT_REFERENCES_AS_CUSTOM_PROPERTY,
					"amlt:/#eventChain_from_second_file_sub_element?type=EventChain",
					eventChainReferenceElement.getChild(CUSTOM_PROPERTIES).getChild(VALUE).getAttributeValue(VALUE));
		}
	}

	/**
	 * This is the scenario in which EventChainSynchronizationConstraint elements
	 * refers to two EventChain elements present in the same file. Among the two
	 * EventChain elements, one EventChain is root element and the other one is sub
	 * EventChain.
	 *
	 * As a part of migration, sub EventChain references should be removed from
	 * scope (as it is not valid to have them as per the change in meta model from
	 * 0.8.1) Removed references are stored as CustomProperties, in order to avoid
	 * data loss
	 *
	 * @param document
	 */
	private void timingConstraintsTest1(final Document document) {
		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append(
				".//timingConstraints[@xsi:type=\"am:EventChainSynchronizationConstraint\" and @name=\"ecc_1\"]");

		final List<Element> elements = HelperUtil.getXpathResult(document.getRootElement(), xpathBuffer.toString(),
				Element.class, XSI_NAMESPACE);

		if (document.getBaseURI().endsWith(EVENT_CHAIN_AMXMI)) {

			assertEquals("EventChainSynchronizationConstraint element should exists with name : ecc_1",
					1, elements.size());

			final Element eventChainSyncConstraintElement = elements.get(0);

			// "Scope should contains only global EventChain elements. If sub event chain
			// elements are present in the
			// input model, as a part of migration content is converted as CustomProperty",
			assertEquals("Scope contains sub event chain elements (which are not allowed as per 0.8.1)",
					"eventChain_1?type=EventChain",
					eventChainSyncConstraintElement.getAttributeValue(SCOPE));

			assertEquals(UNABLE_TO_MIGRATE_SUB_EVENT_CHAIN_ELEMENT_REFERENCES_AS_CUSTOM_PROPERTY,
					"subeventChain_2?type=EventChain",
					eventChainSyncConstraintElement.getChild(CUSTOM_PROPERTIES)
						.getChild(VALUE).getChild(VALUES).getAttributeValue(VALUE));
		}
	}

	/**
	 * This is the scenario in which EventChainSynchronizationConstraint elements
	 * refers to two EventChain elements present in the same file. Both the two
	 * EventChain elements are sub EventChain's.
	 *
	 * As a part of migration, sub EventChain references should be removed from
	 * scope (as it is not valid to have them as per the change in meta model from
	 * 0.8.1) Removed references are stored as CustomProperties, in order to avoid
	 * data loss
	 *
	 * @param document
	 */
	private void timingConstraintsTest2(final Document document) {
		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append(
				".//timingConstraints[@xsi:type=\"am:EventChainSynchronizationConstraint\" and @name=\"ecc2\"]");

		final List<Element> elements = HelperUtil.getXpathResult(document.getRootElement(), xpathBuffer.toString(),
				Element.class, XSI_NAMESPACE);

		if (document.getBaseURI().endsWith(EVENT_CHAIN_AMXMI)) {

			assertEquals("EventChainSynchronizationConstraint element should exists with name : ecc2",
					1, elements.size());

			final Element eventChainSyncConstraintElement = elements.get(0);

			// "Scope should contains only global EventChain elements. If sub event chain
			// elements are present in the
			// input model, as a part of migration content is converted as CustomProperty",
			assertNull(
					"Scope content is not migrated successfully (In a valid case, scope attribute should be removed as both sub eventchain references are removed)",
					eventChainSyncConstraintElement.getAttribute(SCOPE));

			final List<Element> values = eventChainSyncConstraintElement.getChild(CUSTOM_PROPERTIES).getChild(VALUE)
					.getChildren(VALUES);
			assertTrue(UNABLE_TO_MIGRATE_SUB_EVENT_CHAIN_ELEMENT_REFERENCES_AS_CUSTOM_PROPERTY,
					"subeventChain_2?type=EventChain".equals(values.get(0).getAttributeValue(VALUE))
							&& "subeventChain_3?type=EventChain".equals(values.get(1).getAttributeValue(VALUE)));

		}
	}

	/**
	 * This is the scenario in which EventChainSynchronizationConstraint elements
	 * refers to three EventChain elements two present in the same file and the
	 * third EventChain present in different file. Among these three EventChain
	 * elements, one EventChain is a sub EventChain element
	 *
	 * As a part of migration, sub EventChain references should be removed from
	 * scope (as it is not valid to have them as per the change in meta model from
	 * 0.8.1). Removed references are stored as CustomProperties, in order to avoid
	 * data loss
	 *
	 * @param document
	 */
	private void timingConstraintsTest3(final Document document) {
		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append(
				".//timingConstraints[@xsi:type=\"am:EventChainSynchronizationConstraint\" and @name=\"ecc3\"]");

		final List<Element> elements = HelperUtil.getXpathResult(document.getRootElement(), xpathBuffer.toString(),
				Element.class, XSI_NAMESPACE);

		if (document.getBaseURI().endsWith(EVENT_CHAIN_AMXMI)) {

			assertEquals("EventChainSynchronizationConstraint element should exists with name : ecc3",
					1, elements.size());

			final Element eventChainSyncConstraintElement = elements.get(0);

			assertEquals(
					"Scope content is not migrated successfully (In a valid case, scope attribute should be removed as both sub eventchain references are removed)",
					2, eventChainSyncConstraintElement.getChildren(SCOPE).size());

			final List<Element> values = eventChainSyncConstraintElement.getChild(CUSTOM_PROPERTIES).getChild(VALUE).getChildren(VALUES);
			assertEquals(UNABLE_TO_MIGRATE_SUB_EVENT_CHAIN_ELEMENT_REFERENCES_AS_CUSTOM_PROPERTY,
					"amlt:/#subeventChain_2?type=EventChain",
					values.get(0).getAttributeValue(VALUE));
		}
	}

	/**
	 * This is the scenario in which EventChainLatencyConstraint elements refers to
	 * three EventChain elements two present in the same file and the third
	 * EventChain present in different file. Among these three EventChain elements,
	 * one EventChain is a sub EventChain element
	 *
	 * As a part of migration, sub EventChain references should be removed from
	 * scope (as it is not valid to have them as per the change in Metamodel from
	 * 0.8.1). Removed references are stored as CustomProperties, inorder to avoid
	 * data loss
	 *
	 * @param document
	 */
	private void timingConstraintsTest4(final Document document) {
		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append(".//timingConstraints[@xsi:type=\"am:EventChainLatencyConstraint\" and @name=\"ecl1\"]");

		final List<Element> elements = HelperUtil.getXpathResult(document.getRootElement(), xpathBuffer.toString(),
				Element.class, XSI_NAMESPACE);

		if (document.getBaseURI().endsWith(EVENT_CHAIN_AMXMI)) {

			assertEquals("EventChainLatencyConstraint element should exists with name : ecl1", 1, elements.size());

			final Element eventChainLatencyConstraintElement = elements.get(0);

			assertNull(
					"Scope content is not migrated successfully (In a valid case, scope attribute should be removed as sub eventchain references are removed)",
					eventChainLatencyConstraintElement.getAttribute(SCOPE));

			final List<Element> values = eventChainLatencyConstraintElement.getChild(CUSTOM_PROPERTIES)
					.getChild(VALUE).getChildren(VALUES);
			assertEquals(UNABLE_TO_MIGRATE_SUB_EVENT_CHAIN_ELEMENT_REFERENCES_AS_CUSTOM_PROPERTY,
					"strands_subeventchain_2?type=EventChain",
					values.get(0).getAttributeValue(VALUE));
		}
	}

	private void timingConstraintstest5(Document document) {

		if (document.getBaseURI().endsWith("democar.amxmi")) {

			StringBuilder xpathBuffer = new StringBuilder();

			xpathBuffer.append(".//timingConstraints[@xsi:type=\"am:EventChainLatencyConstraint\" and @name=\"a\"]");

			List<Element> elements = HelperUtil.getXpathResult(document.getRootElement(), xpathBuffer.toString(),
					Element.class, XSI_NAMESPACE);

			assertEquals("EventChainLatencyConstraint element should exists with name : a", 1, elements.size());

			Element eventChainLatencyConstraintElement = elements.get(0);

			assertEquals(
					"Unable to migrate event chain element references for EventChainLatencyConstraint for element \"a\" - due to failure in encoding of the content",
					"Test+1?type=EventChain",
					eventChainLatencyConstraintElement.getAttributeValue(SCOPE));

			// second criteria

			xpathBuffer = new StringBuilder();

			xpathBuffer.append(".//timingConstraints[@xsi:type=\"am:EventChainLatencyConstraint\" and @name=\"d\"]");

			elements = HelperUtil.getXpathResult(document.getRootElement(), xpathBuffer.toString(), Element.class,
					XSI_NAMESPACE);

			assertEquals("EventChainLatencyConstraint element should exists with name : d", 1, elements.size());

			eventChainLatencyConstraintElement = elements.get(0);

			assertNull(
					"Unable to migrate event chain element references for EventChainLatencyConstraint for element \"d\" - due to failure in encoding of the content",
					eventChainLatencyConstraintElement.getAttributeValue(SCOPE));
		}
	}
}
