/*********************************************************************************
 * Copyright (c) 2015-2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters081.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters081.impl.ConstraintsConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.HwConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.MappingConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.RootElementConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.StimuliConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.SwConverter;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class StimuliConverterTest extends Abstract081ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test data ID: {0}. Description : {3}")
	public static Collection<Object[]> getTestData() {

		final String[] inputFiles = { "/stimuli/stimuli.amxmi" };

		final String[] inputFilesStimuli = {
				"/stimuli_event/eventModel.amxmi",
				"/stimuli_event/eventStimulus.amxmi" };

		return Arrays.asList(
			data("Models with Stimuli Model (Containing ModeValueListEntry elements)", true, inputFiles,
				"Migration of Amalthea models containing Stimuli model(Containing ModeValueListEntry elements)"),
			data("Models with Stimuli Model (Containing EventStimulus elements)", true, inputFilesStimuli,
						"Migration of Amalthea models containing Stimulus model(Containing EventStimulus elements)"));
	}

	public StimuliConverterTest(String testDataID, boolean canExecuteTestCase, String[] xmlFiles, String description) {
		super(canExecuteTestCase, xmlFiles);
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class, RootElementConverter.class, ConstraintsConverter.class,
				HwConverter.class, SwConverter.class, MappingConverter.class, StimuliConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

	@Override
	protected void modelFileVerificationHook(final Document document) {
		super.modelFileVerificationHook(document);

		/*-------------- Case 1 -----------------*/
		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./stimuliModel/stimuli/setModeValueList/entries[@xsi:type=\"am:ModeValue\"]");
		xpathBuffer.append("|");
		xpathBuffer.append("./stimuliModel/stimuli/enablingModeValueList/entries[@xsi:type=\"am:ModeValue\"]");
		xpathBuffer.append("|");
		xpathBuffer.append("./stimuliModel/stimuli/disablingModeValueList/entries[@xsi:type=\"am:ModeValue\"]");

		final List<Element> abstractProcessElements = HelperUtil.getXpathResult(document.getRootElement(),
				xpathBuffer.toString(), Element.class, AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		if (document.getBaseURI().endsWith("stimuli.amxmi")) {
			assertEquals("Unable to migrate Stimul model containing ModeValueListEntry elements ", 4,
					abstractProcessElements.size());
		}

		/*-------------- Case 2 -----------------*/

		final StringBuilder xpathBufferEventStimulus = new StringBuilder();

		xpathBufferEventStimulus.append("./stimuliModel/stimuli[@xsi:type=\"am:EventStimulus\"]");

		final List<Element> eventStimulusElements = HelperUtil.getXpathResult(document.getRootElement(),
				xpathBufferEventStimulus.toString(), Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (Element eventStimulusElement : eventStimulusElements) {

			boolean migStatusEventStimulus = (eventStimulusElement.getAttribute("trigger") == null)
					|| (eventStimulusElement.getChildren("trigger").size() == 0);

			assertTrue("Unable to migrate Stimul model containing EventStimulus elements ", migStatusEventStimulus);
		}

		if (document.getBaseURI().endsWith("eventStimulus.amxmi")) {

			final List<Element> custPropsObjRefs = HelperUtil.getXpathResult(document.getRootElement(),
					".//customProperties[@key=\"st3\"]/value/values/values/values/values/values/value", Element.class,
					AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			assertEquals("Expected to have CustomProperty", 1, custPropsObjRefs.size());

			String hrefValue = custPropsObjRefs.get(0).getAttributeValue("href");

			assertEquals("Stimulus reference not migrated", "amlt:/#foreignStimuli?type=InterProcessStimulus",
					hrefValue);
		}
	}

}
