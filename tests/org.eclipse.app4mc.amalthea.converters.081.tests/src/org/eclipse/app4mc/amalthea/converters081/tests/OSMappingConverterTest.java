/*********************************************************************************
 * Copyright (c) 2015-2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters081.tests;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters081.impl.ConstraintsConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.HwConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.MappingConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.OSConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.RootElementConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.SwConverter;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class OSMappingConverterTest extends Abstract081ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test data ID: {0}. Description : {3}")
	public static Collection<Object[]> getTestData() {

		final String[] inputFiles = { "/os_mapping/modeling_example_client_server_1.amxmi" };

		return Collections.singletonList(
			data("AMXMI with OS and Mapping Models", true, inputFiles, "Migration of Models containing OS and Mapping Models"));
	}

	public OSMappingConverterTest(String testDataID, boolean canExecuteTestCase, String[] xmlFiles, String description) {
		super(canExecuteTestCase, xmlFiles);
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class, RootElementConverter.class, ConstraintsConverter.class,
				HwConverter.class, SwConverter.class, OSConverter.class, MappingConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

	@Override
	protected void modelFileVerificationHook(final Document document) {
		super.modelFileVerificationHook(document);

		StringBuilder parameterExtensionsXpathBuffer = new StringBuilder();

		parameterExtensionsXpathBuffer.append("./osModel/operatingSystems/interruptControllers/schedulingAlgorithm[@xsi:type=\"am:UserSpecificSchedulingAlgorithm\"]/parameterExtensions");
		parameterExtensionsXpathBuffer.append("|");
		parameterExtensionsXpathBuffer.append("./osModel/operatingSystems/taskSchedulers/schedulingAlgorithm[@xsi:type=\"am:UserSpecificSchedulingAlgorithm\"]/parameterExtensions");

		List<Element> parameterExtensionElements = HelperUtil.getXpathResult(document.getRootElement(),
				parameterExtensionsXpathBuffer.toString(), Element.class, AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		assertTrue( "UserSpecificSchedulingAlgorithm element migration is not successful" , !parameterExtensionElements.isEmpty());

		StringBuilder responsibilityXpathBuffer = new StringBuilder();

		responsibilityXpathBuffer.append("./mappingModel/schedulerAllocation[/responsibility or @responsibility]");

		List<Element> responsibilityElements = HelperUtil.getXpathResult(document.getRootElement(),
				responsibilityXpathBuffer.toString(), Element.class, AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			assertTrue( "CoreAllocation (from 0.8.0) element migration is not successful" , !responsibilityElements.isEmpty());
	}

}
