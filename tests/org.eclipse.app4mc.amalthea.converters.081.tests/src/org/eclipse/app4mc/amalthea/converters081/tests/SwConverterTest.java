/*********************************************************************************
 * Copyright (c) 2015-2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters081.tests;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.app4mc.amalthea.converters.common.converter.NamespaceConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters081.impl.ConstraintsConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.HwConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.MappingConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.RootElementConverter;
import org.eclipse.app4mc.amalthea.converters081.impl.SwConverter;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class SwConverterTest extends Abstract081ConverterTest {

	@Parameterized.Parameters(name = "{index}: Test data ID: {0}. Description : {3}")
	public static Collection<Object[]> getTestData() {

		final String[] inputFiles = { "/sw/sw.amxmi", "/sw/sw2.amxmi" };
		final String[] inputFilesPriority = { "/sw_priority/sw_priority.amxmi" };
		final String[] inputFilesActivations = {
				"/activations/events.amxmi",
				"/activations/sw_activations.amxmi" };

		return Arrays.asList(
			data("Models with SW Model",
					true, inputFiles,
					"Migration of Amalthea models containing SW Model "),
			data("Models with SW Model - having sub-classes of AbstractProcess (e.g: Task, ISR, ProcessPrototyes)",
					true, inputFilesPriority,
					"Migration of Amalthea models containing SW Model (with AbstractProcess elements)"),
			data("Models with Activations & Triggers",
					true, inputFilesActivations,
					"Migration of Amalthea models containing PeriodicActivation's & EventActivation's"));
	}

	public SwConverterTest(String testDataID, boolean canExecuteTestCase, String[] xmlFiles, String description) {
		super(canExecuteTestCase, xmlFiles);
	}

	@Test
	public void testConversion() {
		super.testConversion(NamespaceConverter.class, RootElementConverter.class, ConstraintsConverter.class,
				HwConverter.class, SwConverter.class, MappingConverter.class);
	}

	@Override
	@Test
	public void verification() {
		super.verification();
	}

	@Override
	protected void modelFileVerificationHook(final Document document) {
		super.modelFileVerificationHook(document);

		final List<Element> runnableElements = HelperUtil.getXpathResult(document.getRootElement(),
				"./swModel/runnables[./activations]|./swModel/runnables[@activations]", Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		if (document.getBaseURI().endsWith("sw.amxmi")) {
			assertEquals("Runnable migration is not successful", 2, runnableElements.size());
		}

		// check for priority inside AbstractProcessElements

		final StringBuilder absProcessXpathBuffer = new StringBuilder();

		absProcessXpathBuffer.append("./swModel/tasks[@priority or @osekTaskGroup]");
		absProcessXpathBuffer.append("|");
		absProcessXpathBuffer.append("./swModel/isrs[@priority]");
		absProcessXpathBuffer.append("|");
		absProcessXpathBuffer.append("./swModel/processPrototypes[@priority]");

		final List<Element> abstractProcessElements = HelperUtil.getXpathResult(document.getRootElement(),
				absProcessXpathBuffer.toString(), Element.class, AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		assertEquals("Priority from AbstractProcess elements is not migrated properly", 0,
				abstractProcessElements.size());

		// check for priority inside TaskAllocation

		final List<Element> taskAllocationElements = HelperUtil.getXpathResult(document.getRootElement(),
				"./mappingModel/taskAllocation[@priority]", Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		assertEquals("Priority from TaskAllocation elements is not migrated properly", 0,
				taskAllocationElements.size());

		final List<Element> deadlineElements = HelperUtil.getXpathResult(document.getRootElement(),
				"./swModel/activations[@xsi:type=\"am:PeriodicActivation\"]/deadline", Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		assertEquals("PeriodicActivation is not migrated w.r.t. deadline", 0, deadlineElements.size());

		final List<Element> triggerElements = HelperUtil.getXpathResult(document.getRootElement(),
				"./swModel/activations[@xsi:type=\"am:EventActivation\"]/deadline", Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		assertEquals("EventActivation is not migrated w.r.t. trigger", 0, triggerElements.size());
	}

}
