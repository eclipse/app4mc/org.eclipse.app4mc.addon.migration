/*********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters300.tests;

import org.eclipse.app4mc.amalthea.converters.common.tests.AbstractConverterTest;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;

abstract class Abstract300ConverterTest extends AbstractConverterTest {

	protected Abstract300ConverterTest(boolean canExecuteTestCase, String... xmlFilesRelative) {
		super(canExecuteTestCase, xmlFilesRelative);
	}

	@Override
	public ModelVersion getInputModelVersion() {
		return ModelVersion.VERSION_220;
	}

	@Override
	public ModelVersion getOutputModelVersion() {
		return ModelVersion.VERSION_300;
	}

}
