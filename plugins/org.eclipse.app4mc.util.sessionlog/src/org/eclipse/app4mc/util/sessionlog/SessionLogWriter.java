/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.util.sessionlog;

import java.io.File;
import java.util.List;

/**
 * Service interface for writer that are registered with the SessionLogger to
 * flush the collected session log entries.
 */
public interface SessionLogWriter {

	/**
	 * Write the session log entries.
	 *
	 * @param sessionLogFile The log file to which the log entries should be
	 *                       written.
	 * @param log            The session log entries to write.
	 */
	void write(File sessionLogFile, List<SessionLogEntry> log);

}
