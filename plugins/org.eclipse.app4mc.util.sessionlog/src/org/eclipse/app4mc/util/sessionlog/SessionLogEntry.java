/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.util.sessionlog;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.function.Predicate;

public class SessionLogEntry implements Predicate<SessionLogEntry.Status> {

	public enum Status {
		DEBUG, INFO, WARNING, ERROR;

		public static Status parseStatus(String logLevel) {
			Status logStatus = Status.DEBUG;
			if (logLevel != null) {
				try {
					logStatus = SessionLogEntry.Status.valueOf(logLevel);
				} catch (IllegalArgumentException e) {
					// if the system property can not be parsed to a Status, we use the default
				}
			}
			return logStatus;
		}
	}

	public final Status status;
	public final String message;
	public final Throwable throwable;

	public SessionLogEntry(Status status, String message) {
		this(status, message, null);
	}

	public SessionLogEntry(Status status, String message, Throwable throwable) {
		this.status = status;
		this.message = message;
		this.throwable = throwable;
	}

	public String getStackTrace() {
		if (this.throwable != null) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			this.throwable.printStackTrace(pw);
			return sw.toString();
		}
		return "";
	}

	@Override
	public boolean test(SessionLogEntry.Status t) {
		switch (t) {
			case ERROR: return (this.status == Status.ERROR);
			case WARNING: return (this.status == Status.ERROR || this.status == Status.WARNING);
			case INFO: return (this.status == Status.ERROR || this.status == Status.WARNING || this.status == Status.INFO);
			case DEBUG: return true;
		}
		return true;
	}
}
