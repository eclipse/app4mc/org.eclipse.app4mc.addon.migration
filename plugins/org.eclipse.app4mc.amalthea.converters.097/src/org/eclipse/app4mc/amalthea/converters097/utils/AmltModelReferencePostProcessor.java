/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.converters097.utils;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.IPostProcessor;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.9.6",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.9.7"})

public class AmltModelReferencePostProcessor implements IPostProcessor {

	@Reference
	SessionLogger logger;

	@Override
	public void process(Map<File, Document> fileDocumentMapping) {
		//check if it is single file migration
		if (fileDocumentMapping.size() > 1) {
			return;
		}

		if (logger != null) {
			logger.info("Start Amlt Model Reference processing from 0.9.6 to 0.9.7");
		}

		for (Document document : fileDocumentMapping.values()) {
			 postProcessAmltRefs(document.getRootElement());
		}

		if (logger != null) {
			logger.info("End Amlt Model Reference processing from 0.9.6 to 0.9.7");
		}
	}

	private void postProcessAmltRefs(Element rootElement) {

		 List<Element> hrefs = HelperUtil.getXpathResult(
				 rootElement,
				 ".//*[@href]",
				 Element.class,
				 AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_097.getVersion(), "am"),
				 AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		 for (Element element : hrefs) {
			Element parentElement = element.getParentElement();
			//create an attribute with model reference in the parent element
			String hrefValue = element.getAttributeValue("href", AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_097.getVersion(), "amlt"));

			//href="amlt:/#modelElement?type=EnumMode"
			//"modelElement?type=EnumMode"
			hrefValue = hrefValue.replaceFirst("amlt:/#", "");
			//href="amlt://#GRAM?type=Memory"
			//"GRAM?type=Memory"
			hrefValue = hrefValue.replaceFirst("amlt://#", "");

			Attribute modelRefAttribute = new Attribute(element.getName(), hrefValue);
			parentElement.getAttributes().add(modelRefAttribute);

			element.detach();
		}
	}


}
