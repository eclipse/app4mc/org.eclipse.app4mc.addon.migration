/**
 ********************************************************************************
 * Copyright (c) 2019-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters095.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(property = { ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.9.4",
		ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.9.5" }, service = IConverter.class)

public class SwConverter extends AbstractConverter {

	private static final String TYPE = "type";
	private static final String ITEMS = "items";

	private static final Namespace XSI_NAMESPACE = AmaltheaNamespaceRegistry.getGenericNamespace("xsi");

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {
		logger.info("Migration from 0.9.4 to 0.9.5 : Executing Sw converter for model file : {0}",
				targetFile.getName());

		basicConvert(targetFile, filename2documentMap);
	}

	public void basicConvert(final File file, final Map<File, Document> map) {
		final Document document = map.get(file);
		if (document == null) {
			return;
		}

		final Element rootElement = document.getRootElement();

		updateGraphEntries(rootElement);
		updateCallSequences(rootElement);

		updateRunnableItems(rootElement);
		updateRunnableModeSwitches(rootElement);
		updateRunnableProbabilitySwitches(rootElement);
	}

	private void updateGraphEntries(final Element rootElement) {
		final String xpath = "./swModel/tasks/callGraph/graphEntries" + "|./swModel/isrs/callGraph/graphEntries";

		final List<Element> entries = HelperUtil.getXpathResult(rootElement, xpath, Element.class,
				AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), "am"), XSI_NAMESPACE);

		for (Element graphEntry : entries) {
			// graphEntries -> items
			graphEntry.setName(ITEMS);
		}
	}

	private void updateCallSequences(final Element rootElement) {
		final String xpath = "./swModel/tasks//*[@xsi:type=\"am:CallSequence\"]"
				+ "|./swModel/isrs//*[@xsi:type=\"am:CallSequence\"]";

		final List<Element> callSequences = HelperUtil.getXpathResult(rootElement, xpath, Element.class,
				AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), "am"), XSI_NAMESPACE);

		for (Element cs : callSequences) {
			// CallSequence -> Group
			cs.setAttribute(TYPE, "am:Group", XSI_NAMESPACE);

			// set name="CallSequence"
			String oldName = cs.getAttributeValue("name");
			if (oldName == null || oldName.isEmpty()) {
				cs.setAttribute("name", "CallSequence");
			}

			// set ordered="true"
			cs.setAttribute("ordered", "true");

			for (Element call : cs.getChildren()) {
				// calls -> items
				call.setName(ITEMS);

				// am:TaskRunnableCall -> am:RunnableCall
				String callType = call.getAttributeValue(TYPE, XSI_NAMESPACE);
				if ("am:TaskRunnableCall".equals(callType)) {
					call.setAttribute(TYPE, "am:RunnableCall", XSI_NAMESPACE);
				}
				// remove counter and copy the details into custom properties except for
				// InterProcessTrigger.
				if (!"am:InterProcessTrigger".equals(callType)) {
					// migrate counter into custom properties
					Element counter = call.getChild("counter");
					if (counter != null) {
						migrateCounterToCustomProps(call, counter);
					}
				}
			}
		}
	}

	private void migrateCounterToCustomProps(Element callSequenceItem, Element counter) {
		Attribute prescaler = counter.getAttribute("prescaler");
		Attribute offset = counter.getAttribute("offset");

		if (prescaler != null) {
			createCounterCustomProps(callSequenceItem, "counter-prescaler", prescaler.getValue());
		}
		if (offset != null) {
			createCounterCustomProps(callSequenceItem, "counter-offset", offset.getValue());
		}
		// remove counter
		counter.detach();

	}

	private void createCounterCustomProps(Element callSequenceItem, String key, String val) {
		Element customProps = new Element("customProperties");
		customProps.setAttribute("key", key);

		Element customVal = new Element("value");
		customVal.setAttribute(TYPE, "am:IntegerObject", XSI_NAMESPACE);
		customVal.setAttribute("value", val);

		customProps.addContent(customVal);
		callSequenceItem.addContent(customProps);
	}

	private void updateRunnableItems(final Element rootElement) {
		final String xpath = "./swModel/runnables";

		final List<Element> runnables = HelperUtil.getXpathResult(rootElement, xpath, Element.class,
				AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), "am"), XSI_NAMESPACE);

		for (Element runnable : runnables) {
			// create call graph
			Element callGraph = new Element("callGraph");
			runnable.addContent(callGraph);

			// copy list of runnable items !
			List<Element> runnableItems = new ArrayList<>(runnable.getChildren("runnableItems"));

			for (Element item : runnableItems) {
				// runnableItems -> items
				item.setName(ITEMS);

				// move to callGraph
				item.detach();
				callGraph.addContent(item);
			}
		}
	}

	private void updateRunnableModeSwitches(final Element rootElement) {
		final String xpath = "./swModel/runnables//*[@xsi:type=\"am:RunnableModeSwitch\"]";

		final List<Element> switches = HelperUtil.getXpathResult(rootElement, xpath, Element.class,
				AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), "am"), XSI_NAMESPACE);

		for (Element elem : switches) {
			// RunnableModeSwitch -> ModeSwitch
			elem.setAttribute(TYPE, "am:ModeSwitch", XSI_NAMESPACE);
		}
	}

	private void updateRunnableProbabilitySwitches(final Element rootElement) {
		final String xpath = "./swModel/runnables//*[@xsi:type=\"am:RunnableProbabilitySwitch\"]";

		final List<Element> switches = HelperUtil.getXpathResult(rootElement, xpath, Element.class,
				AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), "am"), XSI_NAMESPACE);

		for (Element elem : switches) {
			// RunnableProbabilitySwitch -> ProbabilitySwitch
			elem.setAttribute(TYPE, "am:ProbabilitySwitch", XSI_NAMESPACE);
		}
	}

}
