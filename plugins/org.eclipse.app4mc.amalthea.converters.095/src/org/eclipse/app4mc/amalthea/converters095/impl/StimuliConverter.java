/**
 ********************************************************************************
 * Copyright (c) 2019-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters095.impl;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.9.4",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.9.5"},
		service = IConverter.class)

public class StimuliConverter extends AbstractConverter {

	private static final String CLOCK = "clock";
	private static final String OLD_DEFINITION_V0_9_4 = "old_definition_v0.9.4";
	private static final String AM = "am";
	private static final String XSI = "xsi";
	private static final String VALUE = "value";
	private static final String UNIT = "unit";

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {
		logger.info("Migration from 0.9.4 to 0.9.5 : Executing Stimuli converter for model file : {0}", targetFile.getName());
		basicConvert(targetFile, filename2documentMap);
	}

	public void basicConvert(final File file, final Map<File, Document> map) {
		final Document document = map.get(file);
		if (document == null) {
			return;
		}

		final Element rootElement = document.getRootElement();
		// migrate scenario
		updateScenario(rootElement);

		// migrate clocks
		updateClocksMulitplierList(rootElement);
		updateClockSineFunction(rootElement);
		updateClockTriangleFunction(rootElement);
	}

	private void updateScenario(Element rootElement) {
		final String xpath = "./stimuliModel/stimuli[@xsi:type=\"am:VariableRateStimulus\"]/scenario";

		final List<Element> scenarios = HelperUtil.getXpathResult(rootElement, xpath, Element.class,
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_094, AM),
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI));

		for (Element element : scenarios) {
			Element recurrence = element.getChild("recurrence");
			if (recurrence != null) {
				recurrence.detach();

				String value = recurrence.getAttributeValue(VALUE);
				String unit = recurrence.getAttributeValue(UNIT);

				addCustomProperty(element, OLD_DEFINITION_V0_9_4, "recurrence=" + value + unit);
			}
			String clockAttribute = element.getAttributeValue(CLOCK);
			if (clockAttribute != null) {
				String[] split = clockAttribute.split("\\?type=Clock");
				if (split.length == 2) {
					String clockRef = split[0];
					String clockType = getNewClockType(split[1]);
					element.setAttribute(CLOCK , clockRef + "?type=Clock" + clockType);
				}
			}
			Element clockElement = getSingleChild(element, CLOCK);
			if (clockElement != null) {
				String hrefValue = clockElement.getAttributeValue("href");
				if (hrefValue != null) {
					Pattern p = Pattern.compile("amlt:/#(.+)\\?type=Clock(.+)");
					Matcher m = p.matcher(hrefValue);
					if (m.find()) {
						String clockRef = m.group(1);
						String clockType = getNewClockType(m.group(2));
						clockElement.setAttribute("type", "am:Clock" + clockType, AmaltheaNamespaceRegistry.getGenericNamespace(XSI));
						clockElement.setAttribute("href", "amlt:/#" + clockRef + "?type=Clock" + clockType);
					}
				}
			}
		}
	}

	private void updateClockSineFunction(Element rootElement) {
		final String xpath = "./stimuliModel/clocks[@xsi:type=\"am:ClockSinusFunction\"]";

		final List<Element> clocks = HelperUtil.getXpathResult(rootElement, xpath, Element.class,
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_094, AM),
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI));

		StringBuilder customPropsValue = new StringBuilder();
		for (Element element : clocks) {
			element.setAttribute("type", "am:ClockFunction", AmaltheaNamespaceRegistry.getGenericNamespace(XSI));
			element.setAttribute("curveType", "sine");

			Attribute amplitude = element.getAttribute("amplitude");
			amplitude.detach();
			customPropsValue.append(getStringValue(amplitude));
			customPropsValue.append(" ");

			Attribute offset = element.getAttribute("yOffset");
			offset.detach();
			customPropsValue.append(getStringValue(offset));
			customPropsValue.append(" ");

			customPropsValue.append(extractPeriodAndShift(element));

			addCustomProperty(element, OLD_DEFINITION_V0_9_4, customPropsValue.toString().trim());
		}
	}

	private void updateClockTriangleFunction(Element rootElement) {
		final String xpath = "./stimuliModel/clocks[@xsi:type=\"am:ClockTriangleFunction\"]";

		final List<Element> clocks = HelperUtil.getXpathResult(rootElement, xpath, Element.class,
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_094, AM),
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI));

		StringBuilder customPropsValue = new StringBuilder();
		for (Element element : clocks) {
			element.setAttribute("type", "am:ClockFunction", AmaltheaNamespaceRegistry.getGenericNamespace(XSI));
			element.setAttribute("curveType", "triangle");

			Attribute max = element.getAttribute("max");
			max.detach();
			customPropsValue.append(getStringValue(max));
			customPropsValue.append(" ");

			Attribute min = element.getAttribute("min");
			min.detach();
			customPropsValue.append(getStringValue(min));
			customPropsValue.append(" ");

			customPropsValue.append(extractPeriodAndShift(element));

			addCustomProperty(element, OLD_DEFINITION_V0_9_4, customPropsValue.toString().trim());
		}
	}

	private String extractPeriodAndShift(Element element) {
		StringBuilder str = new StringBuilder();
		List<Element> children = element.getChildren();
		for (Element e : children) {
			str.append(e.getName());
			str.append("=");
			str.append(e.getAttributeValue(VALUE));
			str.append(e.getAttributeValue(UNIT));
			str.append(" ");
		}

		element.removeChild("period");
		element.removeChild("shift");

		return str.toString();
	}

	private String getStringValue(Attribute attr) {
		return attr.getName() + "=" + attr.getValue();
	}

	private void updateClocksMulitplierList(final Element rootElement) {
		final String xpath = "./stimuliModel/clocks[@xsi:type=\"am:ClockMultiplierList\"]";

		final List<Element> clocks = HelperUtil.getXpathResult(rootElement, xpath, Element.class,
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_094, AM),
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI));

		for (Element element : clocks) {
			element.setAttribute("type", "am:ClockStepList", AmaltheaNamespaceRegistry.getGenericNamespace(XSI));
			updateClockEntries(element);

		}
	}

	private void updateClockEntries(Element clockElement) {
		List<Element> entries = clockElement.getChildren("entries");
		for (Element entry : entries) {
			Attribute multiplier = entry.getAttribute("multiplier");
			multiplier.detach();

			addCustomProperty(entry, OLD_DEFINITION_V0_9_4, "multiplier=" + multiplier.getValue());
		}
	}

	private void addCustomProperty(Element element, String key, String value) {
		Element customProps = new Element("customProperties");
		// set key
		customProps.setAttribute("key", key);
		// set value
		Element valueElement = new Element(VALUE);
		valueElement.setAttribute("type", "am:StringObject", AmaltheaNamespaceRegistry.getGenericNamespace(XSI));
		valueElement.setAttribute(VALUE, value);
		customProps.addContent(valueElement);

		element.addContent(customProps);
	}

	private Element getSingleChild(final Element parent, String name) {
		List<Element> list = parent.getChildren(name);
		if (list.size() == 1) {
			return list.get(0);
		} else {
			return null;
		}
	}

	private String getNewClockType(final String oldClockType) {
		switch (oldClockType) {
		case "SinusFunction": case "TriangleFunction" : return "Function";
		case "MultiplierList" : return "StepList";
		}
		return oldClockType;
	}

}
