/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters093.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters093.utils.HelperUtils093;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Parent;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.9.2",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.9.3"},
		service = IConverter.class)

public class HwConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {

		logger.info("Migration from 0.9.2 to 0.9.3 : Executing Hw converter for model file : {0}", targetFile.getName());

		final Document root = filename2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		updateLatency(rootElement);
	}

	private void updateLatency(Element rootElement) {

		final StringBuilder xpathBuffer = new StringBuilder();

		/*-
		 * As per the change in 0.9.3, Read, Write, Access Latency element definitions are changed
		 *
		 */

		xpathBuffer.append("./hwModel//accessLatency");
		xpathBuffer.append("|");
		xpathBuffer.append("./hwModel//readLatency");
		xpathBuffer.append("|");
		xpathBuffer.append("./hwModel//writeLatency");

		final List<Element> latencyElements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_093, "am"),
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (Element latencyElement : latencyElements) {

			Attribute typeAttribute = latencyElement.getAttribute("type", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			if (typeAttribute != null) {
				String type = typeAttribute.getValue();

				if ("am:LatencyConstant".equals(type)) {

					type = "am:DiscreteValueConstant";
					// setting the updated type value
					typeAttribute.setValue(type);

					Attribute cyclesAttribute = latencyElement.getAttribute("cycles");

					if (cyclesAttribute != null) {
						cyclesAttribute.setName("value");
					}

				} else if ("am:LatencyDeviation".equals(type)) {

					String elementName = latencyElement.getName();

					Parent parentElement = latencyElement.getParent();

					int indexOf = parentElement.indexOf(latencyElement);

					Element migratedElement = HelperUtils093.migrateDeviationElementContainingLongValue(
							latencyElement.getChild("cycles"), elementName, 1.0, logger);

					parentElement.removeContent(latencyElement);

					if (migratedElement != null) {
						parentElement.addContent(indexOf, migratedElement);
					}
				}
			}
		}
	}

}
