/**
 ********************************************************************************
 * Copyright (c) 2018-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.converters093.utils;

import java.util.HashMap;
import java.util.Map;

public class PUDefinitionIPCData {
	/*- Instructions/ipc_2?type=HwFeature: 2.0*/
	private final Map<String, Double> ipcFeatureValueMap = new HashMap<>();

	/*-  pudef : Instructions/ipc_2?type=HwFeature */
	private final Map<String, String> puDefinitionIPCFeatureMap = new HashMap<>();

	public Map<String, Double> getIpcFeatureValueMap() {
		return ipcFeatureValueMap;
	}

	public Map<String, String> getPuDefinitionIPCFeatureMap() {
		return puDefinitionIPCFeatureMap;
	}
}