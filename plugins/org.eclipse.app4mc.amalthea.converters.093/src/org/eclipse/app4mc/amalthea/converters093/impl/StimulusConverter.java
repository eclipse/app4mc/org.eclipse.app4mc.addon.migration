/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters093.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters093.utils.HelperUtils093;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(property = { ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.9.2",
		ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.9.3" }, service = IConverter.class)
public class StimulusConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {

		logger.info("Migration from 0.9.2 to 0.9.3 : Executing Stimulus converter for model file : {0}",
				targetFile.getName());

		final Document root = filename2documentMap.get(targetFile);
		if (root == null)
			return;

		final Element rootElement = root.getRootElement();
		updateStimuli(rootElement);

	}

	private void updateStimuli(final Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();
		xpathBuffer.append("./stimuliModel/stimuli");

		final List<Element> stimuliElements = HelperUtil.getXpathResult(rootElement, xpathBuffer.toString(),
				Element.class, AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_093, "am"),
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (Element stimuliElement : stimuliElements) {

			String stimuliType = stimuliElement.getAttributeValue("type",
					AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			if (stimuliType != null) {
				if ("am:PeriodicStimulus".equals(stimuliType)) {

					Element jitterDeviationElement = stimuliElement.getChild("jitter");

					if (jitterDeviationElement != null) {

						Element migratedElement = HelperUtils093
								.migrateDeviationElementContainingTimeValue(jitterDeviationElement, "jitter", logger);

						int indexOf = stimuliElement.indexOf(jitterDeviationElement);

						stimuliElement.removeContent(jitterDeviationElement);

						if (migratedElement != null) {
							// adding migrated element to the stimuli element
							stimuliElement.addContent(indexOf, migratedElement);
						}
					}

				} else if ("am:RelativePeriodicStimulus".equals(stimuliType)) {

					Element nextOccurrenceDeviationElement = stimuliElement.getChild("nextOccurrence");

					if (nextOccurrenceDeviationElement != null) {

						Element migratedElement = HelperUtils093.migrateDeviationElementContainingTimeValue(
								nextOccurrenceDeviationElement, "nextOccurrence", logger);

						int indexOf = stimuliElement.indexOf(nextOccurrenceDeviationElement);

						stimuliElement.removeContent(nextOccurrenceDeviationElement);

						if (migratedElement != null) {
							// adding migrated element to the stimuli element
							stimuliElement.addContent(indexOf, migratedElement);

						}
					}

				} else if ("am:VariableRateStimulus".equals(stimuliType)) {

					Element occurrencesPerStepDeviationElement = stimuliElement.getChild("occurrencesPerStep");

					if (occurrencesPerStepDeviationElement != null) {

						Element migratedElement = HelperUtils093.migrateDeviationElementContainingDoubleValue(
								occurrencesPerStepDeviationElement, "occurrencesPerStep", logger);

						int indexOf = stimuliElement.indexOf(occurrencesPerStepDeviationElement);

						stimuliElement.removeContent(occurrencesPerStepDeviationElement);

						if (migratedElement != null) {
							// adding migrated element to the stimuli element
							stimuliElement.addContent(indexOf, migratedElement);
						}

					}

					Element maxIncreasePerStepElement = stimuliElement.getChild("maxIncreasePerStep");

					Element maxDecreasePerStepElement = stimuliElement.getChild("maxDecreasePerStep");

					// As DoubleObject is changed to Double (wrapper class) in model. Instead of
					// child elements, xmi will have attributes for the below two parameters
					if (maxIncreasePerStepElement != null) {
						stimuliElement.setAttribute("maxIncreasePerStep",
								maxIncreasePerStepElement.getAttributeValue("value"));
						stimuliElement.removeContent(maxIncreasePerStepElement);
					}

					if (maxDecreasePerStepElement != null) {
						stimuliElement.setAttribute("maxDecreasePerStep",
								maxDecreasePerStepElement.getAttributeValue("value"));
						stimuliElement.removeContent(maxDecreasePerStepElement);
					}
				}
			}
		}
	}
}
