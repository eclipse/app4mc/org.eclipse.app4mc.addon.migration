/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters093.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters093.utils.HelperUtils093;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.9.2",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.9.3"},
		service = IConverter.class)

public class OsConverter extends AbstractConverter {

	private static final String OVERHEAD = "overhead";
	private static final String AM = "am";
	private static final String XSI = "xsi";
	private static final String TYPE = "type";
	private static final String VALUE = "value";
	private static final String AMLT_PREFIX = "amlt://#";

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {

		logger.info("Migration from 0.9.2 to 0.9.3 : Executing Os converter for model file : {0}",
				targetFile.getName());

		final Document root = filename2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		updateOsOverheadRef(rootElement);

		updateOsAPIInstructionsAndOsISRInstructions(rootElement);
	}

	private void updateOsOverheadRef(final Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();
		xpathBuffer.append("./osModel/operatingSystems");

		final List<Element> operatingSystems = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_093, AM),
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI));

		for (Element operatingSystemElement : operatingSystems) {

			String overheadElementName = HelperUtil.getSingleElementNameFromAttributeOrChildeElement(OVERHEAD,operatingSystemElement);

			operatingSystemElement.removeAttribute(OVERHEAD);
			operatingSystemElement.removeChild(OVERHEAD);

			if(overheadElementName!=null) {
				//creating OSOverhead elements reference

				Element osOverHeadRefElement=new Element(OVERHEAD);

				Attribute hrefAttribute=new Attribute("href", AMLT_PREFIX + overheadElementName + "?type=OsOverhead");

				osOverHeadRefElement.setAttribute(hrefAttribute);

				//Adding OsOverHead reference to the OperatingSystem element
				operatingSystemElement.addContent(osOverHeadRefElement);
			}
		}
	}


	private void updateOsAPIInstructionsAndOsISRInstructions(final Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiSendMessage");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiTerminateTask");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiSchedule");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiRequestResource");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiReleaseResource");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiSetEvent");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiWaitEvent");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiClearEvent");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiActivateTask");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiEnforcedMigration");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiSuspendOsInterrupts");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiResumeOsInterrupts");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiRequestSpinlock");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiReleaseSpinlock");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiSenderReceiverRead");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiSenderReceiverWrite");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiSynchronousServerCallPoint");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiIocRead");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiIocWrite");
		xpathBuffer.append("|");

		/* ---------------- Handling the Xpath's from isrCategory1Overhead and isrCategory2Overhead -------------*/

		xpathBuffer.append("./osModel/osOverheads/isrCategory1Overhead/preExecutionOverhead");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/isrCategory1Overhead/postExecutionOverhead");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/isrCategory2Overhead/preExecutionOverhead");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/isrCategory2Overhead/postExecutionOverhead");


		final List<Element> osInstructionsSubElements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_093, AM),
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI));

		for (Element osInstructionsSubElement : osInstructionsSubElements) {

			String typeAttributeValue = osInstructionsSubElement.getAttributeValue(TYPE,
					AmaltheaNamespaceRegistry.getGenericNamespace(XSI));

			String value = osInstructionsSubElement.getAttributeValue(VALUE);

			osInstructionsSubElement.removeAttribute(TYPE, AmaltheaNamespaceRegistry.getGenericNamespace(XSI));

			osInstructionsSubElement.removeAttribute(VALUE);

			/*-------------- Removing fechStatistic element  -------------------*/
			osInstructionsSubElement.removeChild("fetchStatistic");

			/*-------------- Based on the typeAttribute value, adding corresponding element  -------------------*/

			if ("am:InstructionsConstant".equals(typeAttributeValue)) {

				Element defaultElement = new Element("default");

				defaultElement.setAttribute(new Attribute(TYPE, "am:DiscreteValueConstant",
						AmaltheaNamespaceRegistry.getGenericNamespace(XSI)));

				defaultElement.setAttribute(VALUE, value!=null?value:"0");

				osInstructionsSubElement.addContent(defaultElement);

			} else if ("am:InstructionsDeviation".equals(typeAttributeValue)) {
				Element deviationElement = osInstructionsSubElement.getChild("deviation");

				if(deviationElement !=null) {

					Element migratedElement= HelperUtils093.migrateDeviationElementContainingLongValue(deviationElement, "default", 1.0, logger);

					osInstructionsSubElement.removeContent(deviationElement);

					if(migratedElement!=null) {
						osInstructionsSubElement.addContent(migratedElement);
					}
				}
			}
		}
	}

}
