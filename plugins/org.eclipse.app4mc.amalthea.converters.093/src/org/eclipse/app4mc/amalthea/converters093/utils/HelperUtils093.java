/**
 ********************************************************************************
 * Copyright (c) 2017-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters093.utils;

import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Element;

public final class HelperUtils093 {

	private static final String XSI = "xsi";
	private static final String TYPE = "type";
	private static final String VALUE = "value";
	private static final String DISTRIBUTION = "distribution";
	private static final String AVERAGE = "average";
	private static final String ALPHA = "alpha";
	private static final String BETA = "beta";
	private static final String MEAN = "mean";
	private static final String SD = "sd";
	private static final String LOWER_BOUND = "lowerBound";
	private static final String UPPER_BOUND = "upperBound";
	private static final String UNIT = "unit";
	private static final String SAMPLING_TYPE = "samplingType";
	private static final String P_REMAIN_PROMILLE = "pRemainPromille";
	private static final String AM_BETA_DISTRIBUTION = "am:BetaDistribution";
	private static final String AM_BOUNDARIES = "am:Boundaries";
	private static final String AM_GAUSS_DISTRIBUTION = "am:GaussDistribution";
	private static final String AM_UNIFORM_DISTRIBUTION = "am:UniformDistribution";
	private static final String AM_WEIBULL_ESTIMATORS = "am:WeibullEstimators";
	private static final String AM_WEIBULL_PARAMETERS = "am:WeibullParameters";
	private static final String ERROR_WEIBULL_PARAMETERS_CAN_NOT_BE_MIGRATED =
			"Deviation with \"WeibullParameters\" as distribution can not be migrated"
			+ "as the semantics of \"WeibullParameters\" were not clearly described";

	private HelperUtils093() {
		// empty private default constructor
	}

	/**
	 * This method migrates the data of Deviation element (and the corresponding sub-elements)
	 * into the equivalent semantics as per 0.9.3 <br>
	 * <b>Note:</b><br>
	 * Below are the mappings for distribution<br>
	 *
	 * <table>
	 * 		<th>0.9.2</th>					<th>0.9.3</th>
	 * <tr>	<td>Boundaries</td>				<td>DiscreteValueBoundaries</td> </tr>
	 * <tr>	<td>UniformDistribution</td>	<td>DiscreteValueUniformDistribution</td> </tr>
	 * <tr>	<td>BetaDistribution</td>		<td>DiscreteValueBetaDistribution</td> </tr>
	 * <tr>	<td>WeibullEstimators</td>		<td>DiscreteValueWeibullEstimatorsDistribution</td> </tr>
	 * <tr>	<td>GaussDistribution</td>		<td>DiscreteValueGaussDistribution</td> </tr>
	 * </table>
	 *
	 * @param oldDeviationElement
	 * @param newElementName
	 * @param ipcValue
	 * @return
	 */
	public static Element migrateDeviationElementContainingLongValue(Element oldDeviationElement, String newElementName,
			double ipcValue, SessionLogger logger) {

		if (oldDeviationElement == null) return null;

		Element newDeviationElement = new Element(newElementName);

		Element lowerBound = oldDeviationElement.getChild(LOWER_BOUND);
		if (lowerBound != null) {
			String value = lowerBound.getAttributeValue(VALUE);
			if (value != null) {
				newDeviationElement.setAttribute(
						new Attribute(LOWER_BOUND, getValueAfterApplyingIPC(value, ipcValue, logger)));
			}
		}

		Element upperBound = oldDeviationElement.getChild(UPPER_BOUND);
		if (upperBound != null) {
			String value = upperBound.getAttributeValue(VALUE);
			if (value != null) {
				newDeviationElement.setAttribute(
						new Attribute(UPPER_BOUND, getValueAfterApplyingIPC(value, ipcValue, logger)));
			}
		}

		Element oldDistributionElement = oldDeviationElement.getChild(DISTRIBUTION);
		if (oldDistributionElement == null) {
			// fallback if no distribution is specified
			setType(newDeviationElement, "am:DiscreteValueBoundaries");
			return newDeviationElement;
		}

		String oldDistributionType = oldDistributionElement.getAttributeValue(TYPE,
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI));


		if (AM_BETA_DISTRIBUTION.equals(oldDistributionType)) {
			/*-
			<deviation>
				<lowerBound xsi:type="am:LongObject" value="10"/>
				<upperBound xsi:type="am:LongObject" value="20"/>
				<distribution xsi:type="am:BetaDistribution" alpha="0.0" beta="0.0"/>
			</deviation>
			*/

			setType(newDeviationElement, "am:DiscreteValueBetaDistribution");

			String alpha = oldDistributionElement.getAttributeValue(ALPHA);
			if (alpha != null) {
				newDeviationElement.setAttribute(new Attribute(ALPHA, alpha));
			}

			String beta = oldDistributionElement.getAttributeValue(BETA);
			if (beta != null) {
				newDeviationElement.setAttribute(new Attribute(BETA, beta));
			}

		} else if (AM_BOUNDARIES.equals(oldDistributionType)) {
			/*-
			<deviation>
				<lowerBound xsi:type="am:LongObject" value="0"/>
				<upperBound xsi:type="am:LongObject" value="0"/>
				<distribution xsi:type="am:Boundaries" samplingType="WorstCase"/>
			</deviation>
			*/

			setType(newDeviationElement, "am:DiscreteValueBoundaries");

			String samplingType = oldDistributionElement.getAttributeValue(SAMPLING_TYPE);
			if (samplingType != null) {
				newDeviationElement.setAttribute(new Attribute(SAMPLING_TYPE, samplingType));
			}

		} else if (AM_GAUSS_DISTRIBUTION.equals(oldDistributionType)) {
			/*-
			<deviation>
				<lowerBound xsi:type="am:LongObject" value="10"/>
				<upperBound xsi:type="am:LongObject" value="20"/>
				<distribution xsi:type="am:GaussDistribution">
					<sd xsi:type="am:LongObject" value="5"/>
					<mean xsi:type="am:LongObject" value="4"/>
				</distribution>
			</deviation>
			*/

			setType(newDeviationElement, "am:DiscreteValueGaussDistribution");

			String mean = HelperUtil.getValueFromChildElement(oldDistributionElement, MEAN, VALUE);
			if (mean != null) {
				newDeviationElement.setAttribute(new Attribute(MEAN, getValueAfterApplyingIPC(mean, ipcValue, logger)));
			}

			String sd = HelperUtil.getValueFromChildElement(oldDistributionElement, SD, VALUE);
			if (sd != null) {
				newDeviationElement.setAttribute(new Attribute(SD, sd));
			}

		} else if (AM_UNIFORM_DISTRIBUTION.equals(oldDistributionType)) {
			/*-
			<deviation>
				<lowerBound xsi:type="am:LongObject" value="0"/>
				<upperBound xsi:type="am:LongObject" value="0"/>
				<distribution xsi:type="am:UniformDistribution"/>
			 </deviation>
			 */

			setType(newDeviationElement, "am:DiscreteValueUniformDistribution");

		} else if (AM_WEIBULL_ESTIMATORS.equals(oldDistributionType)) {
			/*-
			<deviation>
				<lowerBound xsi:type="am:LongObject" value="10"/>
				<upperBound xsi:type="am:LongObject" value="20"/>
				<distribution xsi:type="am:WeibullEstimators" pRemainPromille="5.0">
					<mean xsi:type="am:LongObject" value="20"/>
				</distribution>
			</deviation>
			*/

			setType(newDeviationElement, "am:DiscreteValueWeibullEstimatorsDistribution");

			String pRemainPromille = oldDistributionElement.getAttributeValue(P_REMAIN_PROMILLE);
			if (pRemainPromille != null) {
				newDeviationElement.setAttribute(new Attribute(P_REMAIN_PROMILLE, pRemainPromille));
			}

			String mean = HelperUtil.getValueFromChildElement(oldDistributionElement, MEAN, VALUE);
			if (mean != null) {
				newDeviationElement
						.setAttribute(new Attribute(AVERAGE, getValueAfterApplyingIPC(mean, ipcValue, logger)));
			}

		} else if (AM_WEIBULL_PARAMETERS.equals(oldDistributionType)) {

			logger.error(
					ERROR_WEIBULL_PARAMETERS_CAN_NOT_BE_MIGRATED);
			return null;
		}

		return newDeviationElement;
	}

	/**
	 * This method migrates the data of Deviation element (and the corresponding sub-elements)
	 * into the equivalent semantics as per 0.9.3 <br>
	 * <b>Note:</b><br>
	 * Below are the mappings for distribution<br>
	 *
	 * <table>
	 * 		<th>0.9.2</th>					<th>0.9.3</th>
	 * <tr>	<td>Boundaries</td>				<td>ContinuousValueBoundaries</td> </tr>
	 * <tr>	<td>UniformDistribution</td>	<td>ContinuousValueUniformDistribution</td> </tr>
	 * <tr>	<td>BetaDistribution</td>		<td>ContinuousValueBetaDistribution</td> </tr>
	 * <tr>	<td>WeibullEstimators</td>		<td>ContinuousValueWeibullEstimatorsDistribution</td> </tr>
	 * <tr>	<td>GaussDistribution</td>		<td>ContinuousValueGaussDistribution</td> </tr>
	 * </table>
	 *
	 * @param oldDeviationElement
	 * @param newElementName
	 * @return
	 */

	public static Element migrateDeviationElementContainingDoubleValue(Element oldDeviationElement, String newElementName, SessionLogger logger) {

		if (oldDeviationElement == null) return null;

		Element newDeviationElement = new Element(newElementName);

		Element lowerBound = oldDeviationElement.getChild(LOWER_BOUND);
		if (lowerBound != null) {
			String value = lowerBound.getAttributeValue(VALUE);
			if (value != null) {
				newDeviationElement.setAttribute(new Attribute(LOWER_BOUND, value));
			}
		}

		Element upperBound = oldDeviationElement.getChild(UPPER_BOUND);
		if (upperBound != null) {
			String value = upperBound.getAttributeValue(VALUE);
			if (value != null) {
				newDeviationElement.setAttribute(new Attribute(UPPER_BOUND, value));
			}
		}

		Element oldDistributionElement = oldDeviationElement.getChild(DISTRIBUTION);
		if (oldDistributionElement == null) {
			// fallback if no distribution is specified
			setType(newDeviationElement, "am:ContinuousValueBoundaries");
			return newDeviationElement;
		}

		String oldDistributionType = oldDistributionElement.getAttributeValue(TYPE,
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI));


		if (AM_BETA_DISTRIBUTION.equals(oldDistributionType)) {
			/*-
			<deviation>
				<lowerBound xsi:type="am:DoubleObject" value="10"/>
				<upperBound xsi:type="am:DoubleObject" value="20"/>
				<distribution xsi:type="am:BetaDistribution" alpha="0.0" beta="0.0"/>
			</deviation>
			*/

			setType(newDeviationElement, "am:ContinuousValueBetaDistribution");

			String alpha = oldDistributionElement.getAttributeValue(ALPHA);
			if (alpha != null) {
				newDeviationElement.setAttribute(new Attribute(ALPHA, alpha));
			}

			String beta = oldDistributionElement.getAttributeValue(BETA);
			if (beta != null) {
				newDeviationElement.setAttribute(new Attribute(BETA, beta));
			}

		} else if (AM_BOUNDARIES.equals(oldDistributionType)) {
			/*-
			<deviation>
				<lowerBound xsi:type="am:DoubleObject" value="0"/>
				<upperBound xsi:type="am:DoubleObject" value="0"/>
				<distribution xsi:type="am:Boundaries" samplingType="WorstCase"/>
			</deviation>
			*/

			setType(newDeviationElement, "am:ContinuousValueBoundaries");

			String samplingType = oldDistributionElement.getAttributeValue(SAMPLING_TYPE);
			if (samplingType != null) {
				newDeviationElement.setAttribute(new Attribute(SAMPLING_TYPE, samplingType));
			}

		} else if (AM_GAUSS_DISTRIBUTION.equals(oldDistributionType)) {
			/*-
			<deviation>
				<lowerBound xsi:type="am:DoubleObject" value="10"/>
				<upperBound xsi:type="am:DoubleObject" value="20"/>
				<distribution xsi:type="am:GaussDistribution">
					<sd xsi:type="am:DoubleObject" value="5"/>
					<mean xsi:type="am:DoubleObject" value="4"/>
				</distribution>
			</deviation>
			*/

			setType(newDeviationElement, "am:ContinuousValueGaussDistribution");

			String mean = HelperUtil.getValueFromChildElement(oldDistributionElement, MEAN, VALUE);
			if (mean != null) {
				newDeviationElement.setAttribute(new Attribute(MEAN, mean));
			}

			String sd = HelperUtil.getValueFromChildElement(oldDistributionElement, SD, VALUE);
			if (sd != null) {
				newDeviationElement.setAttribute(new Attribute(SD, sd));
			}

		} else if (AM_UNIFORM_DISTRIBUTION.equals(oldDistributionType)) {
			/*-
			<deviation>
				<lowerBound xsi:type="am:DoubleObject" value="0"/>
				<upperBound xsi:type="am:DoubleObject" value="0"/>
				<distribution xsi:type="am:UniformDistribution"/>
			</deviation>
			*/

			setType(newDeviationElement, "am:ContinuousValueUniformDistribution");

		} else if (AM_WEIBULL_ESTIMATORS.equals(oldDistributionType)) {
			/*-
			<deviation>
				<lowerBound xsi:type="am:DoubleObject" value="10"/>
				<upperBound xsi:type="am:DoubleObject" value="20"/>
				<distribution xsi:type="am:WeibullEstimators" pRemainPromille="5.0">
					<mean xsi:type="am:DoubleObject" value="20"/>
				</distribution>
			</deviation>
			*/

			setType(newDeviationElement, "am:ContinuousValueWeibullEstimatorsDistribution");

			String pRemainPromille = oldDistributionElement.getAttributeValue(P_REMAIN_PROMILLE);
			if (pRemainPromille != null) {
				newDeviationElement.setAttribute(new Attribute(P_REMAIN_PROMILLE, pRemainPromille));
			}

			String mean = HelperUtil.getValueFromChildElement(oldDistributionElement, MEAN, VALUE);
			if (mean != null) {
				newDeviationElement.setAttribute(new Attribute(AVERAGE, mean));
			}

		} else if (AM_WEIBULL_PARAMETERS.equals(oldDistributionType)) {

			logger.error(
					ERROR_WEIBULL_PARAMETERS_CAN_NOT_BE_MIGRATED);

			return null;
		}

		return newDeviationElement;
	}

	/**
	 * - This method is used to migrate the data of Deviation element (and the
	 * corresponding sub-element : Distribution, both referring to Time as value)
	 * into the equivalent semantics as per 0.9.3 <br>
	 * <b>Note:</b><br>
	 * Below are the mappings for distribution<br>
	 *
	 * <table>
	 * 		<th>0.9.2</th>					<th>0.9.3</th>
	 * <tr>	<td>Boundaries</td>				<td>TimeBoundaries</td> </tr>
	 * <tr>	<td>UniformDistribution</td>	<td>TimeUniformDistribution</td> </tr>
	 * <tr>	<td>BetaDistribution</td>		<td>TimeBetaDistribution</td> </tr>
	 * <tr>	<td>WeibullEstimators</td>		<td>TimeWeibullEstimatorsDistribution</td> </tr>
	 * <tr>	<td>GaussDistribution</td>		<td>TimeGaussDistribution</td> </tr>
	 * </table>
	 *
	 * @param oldDeviationElement
	 * @param newElementName
	 * @return
	 */

	public static Element migrateDeviationElementContainingTimeValue(Element oldDeviationElement, String newElementName, SessionLogger logger) {

		if (oldDeviationElement == null) return null;

		Element newDeviationElement = new Element(newElementName);

		Element lowerBound = oldDeviationElement.getChild(LOWER_BOUND);
		if (lowerBound != null) {
			Element lowerBoundElement = new Element(LOWER_BOUND);
			newDeviationElement.addContent(lowerBoundElement);

			String valueString = lowerBound.getAttributeValue(VALUE);
			if (valueString != null) {
				lowerBoundElement.setAttribute(VALUE, valueString);
			}

			String unitString = lowerBound.getAttributeValue(UNIT);
			if (unitString != null) {
				lowerBoundElement.setAttribute(UNIT, unitString);
			}
		}

		Element upperBound = oldDeviationElement.getChild(UPPER_BOUND);
		if (upperBound != null) {
			Element upperBoundElement = new Element(UPPER_BOUND);
			newDeviationElement.addContent(upperBoundElement);

			String valueString = upperBound.getAttributeValue(VALUE);
			if (valueString != null) {
				upperBoundElement.setAttribute(VALUE, valueString);
			}

			String unitString = upperBound.getAttributeValue(UNIT);
			if (unitString != null) {
				upperBoundElement.setAttribute(UNIT, unitString);
			}
		}

		Element oldDistributionElement = oldDeviationElement.getChild(DISTRIBUTION);
		if (oldDistributionElement == null) {
			// fallback if no distribution is specified
			setType(newDeviationElement, "am:TimeBoundaries");
			return newDeviationElement;
		}

		String oldDistributionType = oldDistributionElement.getAttributeValue(TYPE,
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI));


		if (AM_BETA_DISTRIBUTION.equals(oldDistributionType)) {
			/*-
			<nextOccurrence>
				<lowerBound xsi:type="am:Time" value="66" unit="us"/>
				<upperBound xsi:type="am:Time" value="2" unit="ms"/>
				<distribution xsi:type="am:BetaDistribution" alpha="0.0" beta="0.0"/>
			</nextOccurrence>
			*/

			setType(newDeviationElement, "am:TimeBetaDistribution");

			String alpha = oldDistributionElement.getAttributeValue(ALPHA);
			if (alpha != null) {
				newDeviationElement.setAttribute(new Attribute(ALPHA, alpha));
			}

			String beta = oldDistributionElement.getAttributeValue(BETA);
			if (beta != null) {
				newDeviationElement.setAttribute(new Attribute(BETA, beta));
			}

		} else if (AM_BOUNDARIES.equals(oldDistributionType)) {
			/*-
			<nextOccurrence>
				<lowerBound xsi:type="am:Time" value="0"/>
				<upperBound xsi:type="am:Time" value="0" unit="s"/>
				<distribution xsi:type="am:Boundaries" samplingType="BestCase"/>
			</nextOccurrence>
			*/

			setType(newDeviationElement, "am:TimeBoundaries");

			String samplingType = oldDistributionElement.getAttributeValue(SAMPLING_TYPE);
			if (samplingType != null) {
				newDeviationElement.setAttribute(new Attribute(SAMPLING_TYPE, samplingType));
			}

		} else if (AM_GAUSS_DISTRIBUTION.equals(oldDistributionType)) {
			/*-
			<nextOccurrence>
				<lowerBound xsi:type="am:Time" value="0"/>
				<upperBound xsi:type="am:Time" value="0" unit="s"/>
				<distribution xsi:type="am:GaussDistribution">
					<sd xsi:type="am:Time" value="55" unit="us"/>
					<mean xsi:type="am:Time" value="0"/>
				</distribution>
			</nextOccurrence>
			*/

			setType(newDeviationElement, "am:TimeGaussDistribution");

			String meanValue = HelperUtil.getValueFromChildElement(oldDistributionElement, MEAN, VALUE);
			String meanUnit = HelperUtil.getValueFromChildElement(oldDistributionElement, MEAN, UNIT);
			String sdValue = HelperUtil.getValueFromChildElement(oldDistributionElement, SD, VALUE);
			String sdUnit = HelperUtil.getValueFromChildElement(oldDistributionElement, SD, UNIT);

			if (meanValue != null) {
				Element meanElement = new Element(MEAN);
				meanElement.setAttribute(VALUE, meanValue);
				if (meanUnit != null) {
					meanElement.setAttribute(UNIT, meanUnit);
				}
				newDeviationElement.addContent(meanElement);
			}

			if (sdValue != null) {
				Element sdElement = new Element(SD);
				sdElement.setAttribute(VALUE, sdValue);
				if (sdUnit != null) {
					sdElement.setAttribute(UNIT, sdUnit);
				}
				newDeviationElement.addContent(sdElement);
			}

		} else if (AM_UNIFORM_DISTRIBUTION.equals(oldDistributionType)) {
			/*-
			<deviation>
				<lowerBound xsi:type="am:DoubleObject" value="0"/>
				<upperBound xsi:type="am:DoubleObject" value="0"/>
				<distribution xsi:type="am:UniformDistribution"/>
			 </deviation>
			 */

			setType(newDeviationElement, "am:TimeUniformDistribution");

		} else if (AM_WEIBULL_ESTIMATORS.equals(oldDistributionType)) {

			/*-
			<nextOccurrence>
				<lowerBound xsi:type="am:Time" value="0"/>
				<upperBound xsi:type="am:Time" value="0"/>
				<distribution xsi:type="am:WeibullEstimators" pRemainPromille="10.0">
					<mean xsi:type="am:Time" value="55" unit="us"/>
				</distribution>
			</nextOccurrence>
			*/

			setType(newDeviationElement, "am:TimeWeibullEstimatorsDistribution");

			String pRemainPromille = oldDistributionElement.getAttributeValue(P_REMAIN_PROMILLE);

			if (pRemainPromille != null) {
				newDeviationElement.setAttribute(new Attribute(P_REMAIN_PROMILLE, pRemainPromille));
			}

			String meanValue = HelperUtil.getValueFromChildElement(oldDistributionElement, MEAN, VALUE);
			String meanUnit = HelperUtil.getValueFromChildElement(oldDistributionElement, MEAN, UNIT);

			if (meanValue != null) {
				Element averageElement = new Element(AVERAGE);
				averageElement.setAttribute(new Attribute(VALUE, meanValue));
				if (meanUnit != null) {
					averageElement.setAttribute(new Attribute(UNIT, meanUnit));
				}

				newDeviationElement.addContent(averageElement);
			}

		} else if (AM_WEIBULL_PARAMETERS.equals(oldDistributionType)) {

			logger.error(
					ERROR_WEIBULL_PARAMETERS_CAN_NOT_BE_MIGRATED);

			return null;
		}

		return newDeviationElement;
	}

	public static String getValueAfterApplyingIPC(String value, double ipcValue, SessionLogger logger) {
		if (ipcValue != 0) {
			try {
				Double result = Double.parseDouble(value) / ipcValue;
				return result.longValue() + "";
			} catch (Exception e) {
				logger.error("error on IPC value conversion", e);
			}
		}
		return value;
	}

	private static void setType(Element deviation, String distributionType) {
		Attribute typeAttribute = new Attribute(TYPE, distributionType, AmaltheaNamespaceRegistry.getGenericNamespace(XSI));
		deviation.getAttributes().add(typeAttribute);
	}
}
