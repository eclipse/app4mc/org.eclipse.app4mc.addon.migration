/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters093.utils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(property = ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.9.2")
public class HWCacheBuilder implements ICache {

	@Reference
	SessionLogger logger;

	private final HashMap<File, Map<String, Object>> map = new HashMap<>();

	@Override
	public void buildCache(final Map<File, Document> fileDocumentMapping) {

		if (logger != null) {
			logger.info("Build up HWCache for 0.9.2");
		}

		HashMap<String, Object> hashMap = new HashMap<>();

		PUDefinitionIPCData cache = new PUDefinitionIPCData();
		hashMap.put("globalCache", cache);

		for (final Entry<File, Document> entry : fileDocumentMapping.entrySet()) {
			final File targetFile = entry.getKey();
			final Document document1 = entry.getValue();

			this.map.put(targetFile, hashMap);

			if (document1 == null) {
				// log error message
				continue;
			}

			Element rootElement = document1.getRootElement();
			if (rootElement != null) {
				Element hWModelElement = rootElement.getChild("hwModel");
				if (hWModelElement != null) {
					List<Element> featureCategoriesList = hWModelElement.getChildren("featureCategories");
					for (Element featureCategoriesElement : featureCategoriesList) {
						String featureCategoryName=featureCategoriesElement.getAttributeValue("name");
						List<Element> featuresList = featureCategoriesElement.getChildren("features");
						for (Element featureElement : featuresList) {
							String featureName=featureElement.getAttributeValue("name");
							String featureValue=featureElement.getAttributeValue("value");
							String ipcFeatureKey=HelperUtil.encodeName(featureCategoryName)+"/"+HelperUtil.encodeName(featureName)+"?type=HwFeature";

							double parseDouble = Double.parseDouble(featureValue);
							//adding ipcFeature data to cache map
							cache.getIpcFeatureValueMap().put(ipcFeatureKey, parseDouble);
						}
					}

					List<Element> definitionsList = hWModelElement.getChildren("definitions");
					for (Element definitionsElement : definitionsList) {
						String puDefinitionName = definitionsElement.getAttributeValue("name");
						Map<String, String> features = HelperUtil.getMultipleElementsNameandTypeFromAttributeOrChildeElement("features", definitionsElement);
						Set<String> keySet = features.keySet();
						List<String> ipcHwFeatures = keySet.stream()
								.filter(st -> (st.startsWith("Instructions/IPC") || st.startsWith("Instructions/ipc")))
								.collect(Collectors.toList());
						if (!ipcHwFeatures.isEmpty()) {
							if (ipcHwFeatures.size() > 1) {
								logger.error(
										"ProcessingUnitDefinition : \"{0}\" contains multiple HwFeatures of type IPC. In this case, only the first HwFeature : {1} will be considered for runtime info calculation",
										puDefinitionName, ipcHwFeatures.get(0));
							}

							/*- value is stored as :  Instructions/ipc_2?type=HwFeature  */
							cache.getPuDefinitionIPCFeatureMap().put(HelperUtil.encodeName(puDefinitionName), ipcHwFeatures.get(0) + "?type=HwFeature");
						}
					}
				}
			}
		}
	}

	@Override
	public Map<File, Map<String, Object>> getCacheMap() {
		return this.map;
	}

	@Override
	public void clearCacheMap() {
		this.map.clear();
	}

}
