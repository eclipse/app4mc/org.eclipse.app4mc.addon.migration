/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.util.sessionlog.console;

import java.io.File;
import java.util.List;

import org.eclipse.app4mc.util.sessionlog.SessionLogEntry;
import org.eclipse.app4mc.util.sessionlog.SessionLogEntry.Status;
import org.eclipse.app4mc.util.sessionlog.SessionLogWriter;
import org.osgi.service.component.annotations.Component;

/**
 * {@link SessionLogWriter} implementation that simply prints the session log entries to the console.
 */
@Component
public class SessionLogConsoleWriter implements SessionLogWriter {

	// S106  - Standard outputs should not be used directly to log anything
	//         Purpose of this method is to write log entries to console
	@SuppressWarnings( {"squid:S106"})
	@Override
	public void write(File sessionLogFile, List<SessionLogEntry> log) {
		String logLevel = System.getProperty("app4mc.log.level");
		Status logStatus = SessionLogEntry.Status.parseStatus(logLevel);

		log.stream().filter(entry -> entry.test(logStatus)).forEach(entry -> {
			System.out.println(entry.message);
			if (entry.throwable != null) {
				System.out.println(entry.getStackTrace());
			}
		});
	}

}
