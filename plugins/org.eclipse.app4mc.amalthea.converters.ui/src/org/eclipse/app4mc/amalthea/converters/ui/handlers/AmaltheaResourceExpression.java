/**
 ********************************************************************************
 * Copyright (c) 2020-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.ui.handlers;

import javax.inject.Named;

import org.eclipse.app4mc.amalthea.converters.common.MigrationHelper;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;

public class AmaltheaResourceExpression {

	@Evaluate
	public boolean evaluate(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection) {
		if (selection == null || selection.isEmpty() || selection.size() > 1
				|| !(selection.getFirstElement() instanceof IResource)) {
			return false;
		}

		IResource resource = (IResource) selection.getFirstElement();
		if (isFileWithModelExtension(resource)) {
			return true;
		}

		if (resource instanceof IContainer) {
			// check if there are amxmi files in the folder or project
			IContainer container = (IContainer) resource;

			try {
				for (IResource member : container.members()) {
					if (isFileWithModelExtension(member)) {
						return true;
					}
				}
			} catch (CoreException e) {
				Platform.getLog(getClass()).error("Failed to collect model files", e);
			}
		}

		return false;
	}

	private boolean isFileWithModelExtension(IResource resource) {
		return resource instanceof IFile && MigrationHelper.isModelFileExtension(resource.getFileExtension());
	}

}
