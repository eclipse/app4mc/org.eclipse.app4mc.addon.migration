/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.ui.dialog;

import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class MigrationResultDialog extends Dialog {

	private final Map<String, String> results;

	public MigrationResultDialog(Shell parentShell, Map<String, String> results) {
		super(parentShell);
		setShellStyle(SWT.APPLICATION_MODAL | SWT.DIALOG_TRIM | SWT.RESIZE);
		this.results = results;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("AMALTHEA Model Migration Result");
	}

	@Override
	protected Point getInitialSize() {
		return new Point(700, 400);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create only OK button
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		parent.setToolTipText("AMALTHEA Model Migration Result");

		Composite parentComposite = (Composite) super.createDialogArea(parent);
		GridLayout gridLayout = (GridLayout) parentComposite.getLayout();
		gridLayout.numColumns = 1;

		Composite tableComposite = createFileTableViewer(parentComposite);
		GridDataFactory
			.fillDefaults()
			.grab(true, true)
			.applyTo(tableComposite);

		return parentComposite;
	}

	private Composite createFileTableViewer(Composite parent) {
		Composite tableComposite = new Composite(parent, SWT.NONE);

		TableColumnLayout tableColumnLayout = new TableColumnLayout();
		tableComposite.setLayout(tableColumnLayout);

		TableViewer tableViewer = new TableViewer(tableComposite,
				SWT.BORDER | SWT.FULL_SELECTION | SWT.H_SCROLL | SWT.V_SCROLL);

		Table table = tableViewer.getTable();
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		TableViewerColumn filepathColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		filepathColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				@SuppressWarnings("unchecked")
				Entry<String, String> entry = (Entry<String, String>) element;
				return super.getText(entry.getKey());
			}
		});
		TableColumn column = filepathColumn.getColumn();
		column.setText("Relative file paths");

		tableColumnLayout.setColumnData(column, new ColumnWeightData(50, true));

		TableViewerColumn resultColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		resultColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				@SuppressWarnings("unchecked")
				Entry<String, String> entry = (Entry<String, String>) element;
				return super.getText(entry.getValue());
			}
		});

		column = resultColumn.getColumn();
		column.setText("Migration Result");
		tableColumnLayout.setColumnData(column, new ColumnWeightData(50, true));

		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setInput(results.entrySet());

		tableViewer.setComparator(new ViewerComparator());

		return tableComposite;
	}

}
