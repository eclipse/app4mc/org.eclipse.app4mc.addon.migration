/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.ui.addons;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.app4mc.amalthea.converters.common.MigrationProcessor;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters.ui.handlers.AmaltheaModelMigrationHandler;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.di.extensions.Service;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Display;

/**
 * This addon simply provides the model version of the latest supported AMALTHEA
 * model as a context value to the application context. It also registers an
 * event listener for the topic
 * <i>org/eclipse/app4mc/amalthea/converter/CONVERT</i> so a migration can be
 * triggered via event. This is to decouple the migration component from other
 * components in the UI.
 */
@SuppressWarnings("restriction")
public class ConverterUIAddon {

	@Inject
	public void init(IEclipseContext context) {
		context.set("APP4MC_MODEL_MIGRATION_VERSION", ModelVersion.getLatestVersion());
	}

	@Inject
	@Optional
	private void handleConvertEvent(
			@UIEventTopic("org/eclipse/app4mc/amalthea/converter/CONVERT") Map<String, String> convertionArgs,
			@Service MigrationProcessor migrationProcessor,
			@Named(IServiceConstants.ACTIVE_SELECTION) ISelection selection) {

		// whether it is a simplemigration or a dialogmigration
		String type = convertionArgs.get("type");
		// the version that is supported by the editor (aka version from meta-model)
		String version = convertionArgs.get("version");

		AmaltheaModelMigrationHandler handler = new AmaltheaModelMigrationHandler();
		handler.execute(Display.getDefault().getActiveShell(), migrationProcessor, selection, type, version);
	}
}
