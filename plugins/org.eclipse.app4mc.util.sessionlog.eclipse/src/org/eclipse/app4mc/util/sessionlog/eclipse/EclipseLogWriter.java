/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.util.sessionlog.eclipse;

import java.io.File;
import java.util.List;

import org.eclipse.app4mc.util.sessionlog.SessionLogEntry;
import org.eclipse.app4mc.util.sessionlog.SessionLogWriter;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * Custom Log4j appender that forwards log statements to the Eclipse logging mechanism.
 */
public class EclipseLogWriter implements SessionLogWriter {

	private int getStatus(SessionLogEntry.Status sessionLogStatus) {
		int status = IStatus.INFO;

		if (sessionLogStatus == SessionLogEntry.Status.ERROR) {
			status = IStatus.ERROR;
		}
		else if (sessionLogStatus == SessionLogEntry.Status.WARNING) {
			status = IStatus.WARNING;
		}
		return status;
	}

	@Override
	public void write(File sessionLogFile, List<SessionLogEntry> log) {
		String logLevel = System.getProperty("app4mc.log.level");
		SessionLogEntry.Status logStatus = SessionLogEntry.Status.parseStatus(logLevel);

		Bundle bundle = FrameworkUtil.getBundle(getClass());
		if (bundle != null) {
			log.stream().filter(entry -> entry.test(logStatus)).forEach(entry -> {
				final int status = getStatus(entry.status);
				if (entry.throwable != null) {
					Platform.getLog(bundle).log(
							new Status(status, bundle.getSymbolicName(), entry.message, entry.throwable));
				} else {
					Platform.getLog(bundle).log(
							new Status(status, bundle.getSymbolicName(), entry.message));
				}
			});
		}
	}

}
