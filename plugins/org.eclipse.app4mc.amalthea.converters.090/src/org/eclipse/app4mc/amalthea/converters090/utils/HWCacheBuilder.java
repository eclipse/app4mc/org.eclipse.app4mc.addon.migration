/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.converters090.utils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.8.3",
		service = { ICache.class, HWCacheBuilder.class })

public class HWCacheBuilder implements ICache {

	@Reference
	SessionLogger logger;

	public static final String CACHE_KEY = "PeriodicStimulus_Containing_Clock";

	private final HashMap<File, Map<String, Object>> map = new HashMap<>();

	@Override
	public void buildCache(final Map<File, Document> fileDocumentMapping) {

		if (logger != null) {
			logger.info("Build up HWTransformationCache for 0.8.3");
		}

		HWTransformationCache cache = new HWTransformationCache();

		for (final Entry<File, Document> entry : fileDocumentMapping.entrySet()) {
			final File targetFile = entry.getKey();
			final Document document = entry.getValue();

			if (document == null) {
				// log error message
				continue;
			}

			Element rootElement = document.getRootElement();

			if (rootElement != null) {

				Element oldHWModelElement = rootElement.getChild("hwModel");

				if (oldHWModelElement != null) {

					Element oldHWSystem = oldHWModelElement.getChild("system");

					if (oldHWSystem != null) {

						List<Element> oldHWQuartzes = oldHWSystem.getChildren("quartzes");

						for (Element oldHWQuartz : oldHWQuartzes) {

							String attributeValue = oldHWQuartz.getAttributeValue("name");

							if (attributeValue != null) {
								cache.getOldHwQuartzsMap().put(HelperUtil.encodeName(attributeValue), oldHWQuartz);
							}
						}

						List<Element> oldHWECUs = oldHWSystem.getChildren("ecus");

						for (Element oldHWECU : oldHWECUs) {
							List<Element> oldHWMicroControllers = oldHWECU.getChildren("microcontrollers");

							for (Element oldHWMicroController : oldHWMicroControllers) {

								List<Element> oldHWCores = oldHWMicroController.getChildren("cores");

								for (Element oldHWCore : oldHWCores) {
									String attributeValue = oldHWCore.getAttributeValue("name");
									if (attributeValue != null) {
										cache.getOldCoresMap().put(HelperUtil.encodeName(attributeValue), oldHWCore);
									}
								}
							}
						}
					}

					List<Element> oldHWMemoryTypes = oldHWModelElement.getChildren("memoryTypes");

					for (Element oldHWMemoryType : oldHWMemoryTypes) {
						String attributeValue = oldHWMemoryType.getAttributeValue("name");

						if (attributeValue != null) {
							cache.getOldMemoryTypesDefinitionMap().put(HelperUtil.encodeName(attributeValue),
									oldHWMemoryType);
						}
					}
				}
			}

			/*- Start : Cache initialization */

			HashMap<String, Object> hashMap = new HashMap<>();
			hashMap.put("globalCache", cache);
			this.map.put(targetFile, hashMap);
		}
	}

	@Override
	public Map<File, Map<String, Object>> getCacheMap() {
		return this.map;
	}

	@Override
	public void clearCacheMap() {
		this.map.clear();
	}

}
