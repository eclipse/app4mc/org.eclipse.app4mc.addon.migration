/**
 ********************************************************************************
 * Copyright (c) 2018-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters090.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters090.utils.HWCacheBuilder;
import org.eclipse.app4mc.amalthea.converters090.utils.HWTransformationCache;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * This class is responsible for converting the HW Model elements from 0.8.3 to
 * 0.9.0 version format of AMALTHEA model
 *
 * @author zmeer
 *
 */
@Component(property = {
		ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.8.3",
		ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.9.0",
		"service.ranking:Integer=90" }, service = IConverter.class)

public class HwConverter extends AbstractConverter {

	private static final String AM = "am";
	private static final String AMLT_PREFIX = "amlt:/#";
	private static final String TYPE = "type";
	private static final String NAME = "name";
	private static final String VALUE = "value";
	private static final String DEFINITION = "definition";
	private static final String DEFINITIONS = "definitions";
	private static final String STRUCTURES = "structures";
	private static final String STRUCTURE_TYPE = "structureType";
	private static final String AM_MEMORY = "am:Memory";
	private static final String CLASSIFIERS = "classifiers";
	private static final String COMPONENTS = "components";
	private static final String MEMORIES = "memories";
	private static final String MODULES = "modules";
	private static final String NETWORKS = "networks";

	private static final Namespace XSI_NAMESPACE = AmaltheaNamespaceRegistry.getGenericNamespace("xsi");

	@Reference
	SessionLogger logger;

	private HWTransformationCache hwTransformationCache;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	/*-
	 * As in 0.9.0, there is a major restructuring of HW data model ->
	 * 				all model files HW data is transformed at once and its content is stored only inside a single HW model
	 */

	@Override
	public void convert(File targetFile, Map<File, Document> fileDocumentMapping, List<ICache> caches) {

		logger.info("Migration from 0.8.3 to 0.9.0 : Executing HW model converter for model file : {0}",
				targetFile.getName());

		/*-getting the cache object */
		this.hwTransformationCache = getHWTransformationCache(caches);

		final Document root = fileDocumentMapping.get(targetFile);
		if (root == null)
			return;

		final Element rootElement = root.getRootElement();

		Element oldHWModelElement = rootElement.getChild("hwModel");

		migrateHWModel(rootElement, oldHWModelElement);
	}

	/**
	 * This method is used for the migration of HwModel element inside AMALTHEA
	 * model file. Contents of HwModel from 0.8.3 are extracted and are transformed
	 * into valid HwModel contents as per 0.9.0. <br>
	 * <b>Note</b>: If multiple AMALTHEA models have HwModel elements, then their
	 * contents are extracted and the below contents are cumulatively stored inside
	 * a first HwModel:
	 * <ol>
	 * <li>MemoryType</li>
	 * <li>Quartz</li>
	 * </ol>
	 *
	 *
	 * @param rootElement
	 * @param oldHWModelElement
	 */
	private void migrateHWModel(final Element rootElement, Element oldHWModelElement) {
		if (oldHWModelElement == null)
			return;

		final Element newHWModelElement = new Element("hwModel");

		migrateAllMemoryTypes(newHWModelElement);
		migrateCoreTypes(oldHWModelElement, newHWModelElement);
		migrateNetworkTypes(oldHWModelElement, newHWModelElement);

		Element oldHWSystem = oldHWModelElement.getChild("system");

		if (oldHWSystem != null) {
			migrateSystem(newHWModelElement, oldHWSystem);
		}

		migrateLatencyAccessPath(oldHWModelElement);

		int indexOf = rootElement.indexOf(oldHWModelElement);

		/*-removal of 0.8.3 HWModel content from AMALTHEA model */

		rootElement.removeContent(oldHWModelElement);

		/*-adding newly created hwModel -> 0.9.0 compatible into the AMALTHEA model file */

		rootElement.addContent(indexOf, newHWModelElement);

		/*-Migrating custom properties */
		migrateCustomProperties(oldHWModelElement, newHWModelElement);
	}

	/**
	 * This method is used to migrate all the MemoryType elements from 0.8.3 hwModel
	 * file to 0.9.0 hwModel
	 *
	 * <br>
	 * Before invocation of HwConverter, HwCacheBuilder is invoked and it has
	 * collected all the MemoryType objects across various HwModel's
	 *
	 * @param newHWModelElement Element. 0.9.0 compatible HwModel element
	 */
	private void migrateAllMemoryTypes(Element newHWModelElement) {

		Collection<Element> oldMemoryTypeDefinitions = hwTransformationCache.getOldMemoryTypesDefinitionMap()
				.values();

		for (Element oldHWModelElement : oldMemoryTypeDefinitions) {

			migrateMemoryTypes(oldHWModelElement, newHWModelElement);
		}

	}

	/**
	 * This method is used to migrate the LatencyAccessPath elements from 0.8.3 to
	 * 0.9.0.
	 *
	 * Equivalent element of LatencyAccessPath in 0.9.0 is : HwAccessElement.
	 *
	 * <br>
	 * <b>Note:</b>For HwAccessElement :
	 * <ul>
	 *
	 * <li>source is ProcessingUnit which is containing this HwAccessElement</li>
	 * <li>destination is HwDestination element -> which can be either Memory or
	 * ProcessingUnit</li>
	 * </ul>
	 *
	 * @param oldHWModelElement Element. JDOM Element equivalent to 0.8.3 HwModel
	 * @param newHWModelElement Element. JDOM Element equivalent to 0.9.0 HwModel
	 */
	private void migrateLatencyAccessPath(Element oldHWModelElement) {

		List<Element> oldAccessPathList = oldHWModelElement.getChildren("accessPaths");

		for (Element oldAccessPath : oldAccessPathList) {

			String oldAccessPathName = oldAccessPath.getAttributeValue(NAME);
			String oldAccessPathType = oldAccessPath.getAttributeValue(TYPE, XSI_NAMESPACE);

			if (oldAccessPathType != null && "am:LatencyAccessPath".equals(oldAccessPathType)) {

				Entry<String, String> oldSource = HelperUtil
						.getSingleElementsNameandTypeFromAttributeOrChildeElement("source", oldAccessPath);

				Entry<String, String> oldTarget = HelperUtil
						.getSingleElementsNameandTypeFromAttributeOrChildeElement("target", oldAccessPath);

				Element newAccessElement = null;

				if (oldSource != null) {

					String oldSourceName = oldSource.getKey();
					String oldSourceType = oldSource.getValue();

					if ("Core".equals(oldSourceType)) {

						Element newProcessingUnit = hwTransformationCache.getNewCoresMap().get(oldSourceName);

						if (newProcessingUnit != null) {
							newAccessElement = new Element("accessElements");
							newAccessElement.setAttribute(NAME, oldAccessPathName != null ? oldAccessPathName : "");
							newProcessingUnit.addContent(newAccessElement);
						}

					} else {
						logger.warn(
								"Unable to migrate LatencyAccessPath from 0.8.3, as the Source type is : {0}. Migration is supported only if source element is of type Core",
								oldSourceType);
					}
				}

				if (oldTarget != null) {
					Element newDestination = new Element("destination");

					String oldTargetName = oldTarget.getKey();
					String oldTargetType = oldTarget.getValue();

					if ("Memory".equals(oldTargetType)) {

						newDestination.setAttribute(TYPE, AM_MEMORY, XSI_NAMESPACE);
						newDestination.setAttribute("href",
								"amlt://#" + HelperUtil.encodeNameForReference(oldTargetName) + "?type=Memory");

					} else if ("Core".equals(oldTargetType)) {

						newDestination.setAttribute(TYPE, "am:ProcessingUnit", XSI_NAMESPACE);
						newDestination.setAttribute("href", "amlt://#"
								+ HelperUtil.encodeNameForReference(oldTargetName) + "?type=ProcessingUnit");

					} else {

						logger.warn(
								"Unable to migrate LatencyAccessPath destination from 0.8.3 successfully, as the destination type is : {0}. Migration is supported only if target element is of type Memory or Core",
								oldTargetType);
					}

					if (newAccessElement != null) {
						newAccessElement.addContent(newDestination);
					}
				}

				// now adding latencies
				if (newAccessElement != null) {

					List<Element> oldLatencyList = oldAccessPath.getChildren("latencies");

					for (Element oldHW_Latency : oldLatencyList) {
						String oldLatencyAccessType = oldHW_Latency.getAttributeValue("accessType");

						if (oldLatencyAccessType != null) {

							List<Element> newHWLatencies = new ArrayList<>();

							if ("R".equals(oldLatencyAccessType)) {

								newHWLatencies.add(new Element("readLatency"));

							} else {
								if ("W".equals(oldLatencyAccessType)) {

								} else if ("RW".equals(oldLatencyAccessType)) {

									newHWLatencies.add(new Element("readLatency"));

								} else {
									continue;
								}
								newHWLatencies.add(new Element("writeLatency"));
							}

							for (Element newHW_Latency : newHWLatencies) {

								/*- adding latency to HWAccessElement */
								newAccessElement.addContent(newHW_Latency);

								String oldLatencyType = oldHW_Latency.getAttributeValue(TYPE, XSI_NAMESPACE);

								if (oldLatencyType != null) {

									if ("am:LatencyConstant".equals(oldLatencyType)) {
										newHW_Latency.setAttribute(TYPE, "am:LatencyConstant", XSI_NAMESPACE);

										String oldLatencyValue = oldHW_Latency.getAttributeValue(VALUE);

										if (oldLatencyValue != null) {
											newHW_Latency.setAttribute("cycles", oldLatencyValue);
										}

									} else if ("am:LatencyDeviation".equals(oldLatencyType)) {
										newHW_Latency.setAttribute(TYPE, "am:LatencyDeviation", XSI_NAMESPACE);

										Element oldLatencyDeviation = oldHW_Latency.getChild("deviation");

										if (oldLatencyDeviation != null) {
											oldLatencyDeviation = oldLatencyDeviation.clone();
											oldLatencyDeviation.detach();

											oldLatencyDeviation.setName("cycles");

											newHW_Latency.addContent(oldLatencyDeviation);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * This method is used to migrate NetworkType element from 0.8.3 to
	 * ConnectionHandlerDefinition element as per 0.9.0
	 *
	 * @param oldHWModelElement Element. JDOM Element equivalent to 0.8.3 HwModel
	 * @param newHWModelElement Element. JDOM Element equivalent to 0.9.0 HwModel
	 */
	private void migrateNetworkTypes(Element oldHWModelElement, final Element newHWModelElement) {

		List<Element> oldHWNetworkTypes = oldHWModelElement.getChildren("networkTypes");

		for (Element oldHWNetworkType : oldHWNetworkTypes) {

			String oldNetworkTypeName = oldHWNetworkType.getAttributeValue(NAME);

			Element newHWNetworkType = new Element(DEFINITIONS);

			/*-adding newly created element to the hwmodel*/
			newHWModelElement.addContent(newHWNetworkType);

			newHWNetworkType.setAttribute(TYPE, "am:ConnectionHandlerDefinition", XSI_NAMESPACE);

			if (oldNetworkTypeName != null) {
				newHWNetworkType.setAttribute(NAME, oldNetworkTypeName);
			}

			String oldNetworkTypeSchedPolicy = oldHWNetworkType.getAttributeValue("schedPolicy");

			if (oldNetworkTypeSchedPolicy != null) {

				String policy = "_undefined_";

				if ("RROBIN".equals(oldNetworkTypeSchedPolicy)) {
					policy = "RoundRobin";
				} else if ("PRIORITY".equals(oldNetworkTypeSchedPolicy)) {
					policy = "PriorityBased";
				}
				newHWNetworkType.setAttribute("policy", policy);
			}

			/*-migrating custom properties */

			migrateCustomProperties(oldHWNetworkType, newHWNetworkType);
		}
	}

	/**
	 * This method is used to migrate CoreType element from 0.8.3 to
	 * ProcessingUnitDefinition element as per 0.9.0
	 *
	 * <b>Note:</b> value of InstructionsPerCycle is converted as HwFeatureCategory
	 * and HwFeature#s and the corresponding HwFeature is linked to the
	 * ProcessingUnitDefinition
	 *
	 * @param oldHWModelElement Element. JDOM Element equivalent to 0.8.3 HwModel
	 * @param newHWModelElement Element. JDOM Element equivalent to 0.9.0 HwModel
	 */
	private void migrateCoreTypes(Element oldHWModelElement, final Element newHWModelElement) {
		List<Element> oldCoreTypes = oldHWModelElement.getChildren("coreTypes");

		for (Element oldCoreType : oldCoreTypes) {

			String oldCoreTypeName = oldCoreType.getAttributeValue(NAME);

			Element newHWCoreType = new Element(DEFINITIONS);

			/*-adding newly created element to the hwmodel*/

			newHWModelElement.addContent(newHWCoreType);

			newHWCoreType.setAttribute(TYPE, "am:ProcessingUnitDefinition", XSI_NAMESPACE);

			if (oldCoreTypeName != null) {
				newHWCoreType.setAttribute(NAME, oldCoreTypeName);
			}

			newHWCoreType.setAttribute("puType", "CPU");

			Attribute oldCoreTypeClassifiers = oldCoreType.getAttribute(CLASSIFIERS);

			if (oldCoreTypeClassifiers != null) {

				newHWCoreType.setAttribute(oldCoreTypeClassifiers.clone());
			} else {
				List<Element> oldCoreTypeClassifiersList = oldCoreType.getChildren(CLASSIFIERS);

				for (Element oldCoreType_Classifier : oldCoreTypeClassifiersList) {

					newHWCoreType.addContent(oldCoreType_Classifier.clone());
				}
			}

			String oldCoreTypeICP = oldCoreType.getAttributeValue("instructionsPerCycle");

			if (oldCoreTypeICP != null) {

				String ipcValue = oldCoreTypeICP;

				oldCoreTypeICP = "IPC_" + ipcValue;

				String newHWFeatureCategoryName = "Instructions";

				Element newHWFeatureCategories = new Element("featureCategories");

				newHWFeatureCategories.setAttribute(NAME, newHWFeatureCategoryName);

				if (hwTransformationCache.getNewFeatureCategoriesMap().containsKey(newHWFeatureCategoryName)) {
					newHWFeatureCategories = hwTransformationCache.getNewFeatureCategoriesMap()
							.get(newHWFeatureCategoryName);
				} else {
					newHWModelElement.addContent(newHWFeatureCategories);
				}

				newHWFeatureCategories.setAttribute("featureType", "performance");

				// TODO: Add docu here. In new_feature_literals_Map key is the literal name with
				// convention as : FeatureName/LiteralName
				if (!hwTransformationCache.getNewFeaturesMap()
						.containsKey(newHWFeatureCategoryName + "/" + oldCoreTypeICP)) {

					Element newHWFeatureElement = new Element("features");

					newHWFeatureElement.setAttribute(NAME, oldCoreTypeICP);
					newHWFeatureElement.setAttribute(VALUE, ipcValue);

					newHWFeatureCategories.addContent(newHWFeatureElement);

					hwTransformationCache.getNewFeaturesMap().put(newHWFeatureCategoryName + "/" + oldCoreTypeICP,
							newHWFeatureElement);
				}

				hwTransformationCache.getNewFeatureCategoriesMap().put(newHWFeatureCategoryName,
						newHWFeatureCategories);

				// Adding HwFeature as a part of ProcessingUnitDefinition

				Element newHwFeaturesElement = new Element("features");

				newHwFeaturesElement.setAttribute("href",
						AMLT_PREFIX + (newHWFeatureCategoryName) + "/" + (oldCoreTypeICP) + "?type=HwFeature");
				newHWCoreType.addContent(newHwFeaturesElement);

			}

			/*-migrating custom properties */

			migrateCustomProperties(oldCoreType, newHWCoreType);

		}
	}

	/**
	 * This method is used to migrate MemoryType element from 0.8.3 to
	 * MemoryDefinition element as per 0.9.0
	 *
	 * <b>Note:</b>If the type of MemoryType element is CACHE, then during the
	 * migration CacheDefinition object is created instead of MemoryDefinition
	 *
	 * @param oldMemoryTypeElement Element. JDOM Element equivalent to 0.8.3
	 *                             MemoryType
	 * @param newHWModelElement    Element. JDOM Element equivalent to 0.9.0 HwModel
	 */
	private void migrateMemoryTypes(Element oldMemoryTypeElement, final Element newHWModelElement) {

		String oldMemoryTypeName = oldMemoryTypeElement.getAttributeValue(NAME);

		String oldMemoryTypeType = oldMemoryTypeElement.getAttributeValue(TYPE);

		Element newHWMemoryType = new Element(DEFINITIONS);

		/*-adding newly created element to the hwmodel*/
		newHWModelElement.addContent(newHWMemoryType);

		if ("CACHE".equals(oldMemoryTypeType)) {

			newHWMemoryType.setAttribute(TYPE, "am:CacheDefinition", XSI_NAMESPACE);

			hwTransformationCache.getNewCacheTypesDefinitionMap().put(HelperUtil.encodeName(oldMemoryTypeName),
					newHWMemoryType);

		} else {

			newHWMemoryType.setAttribute(TYPE, "am:MemoryDefinition", XSI_NAMESPACE);

			hwTransformationCache.getNewMemoryTypesDefinitionMap().put(HelperUtil.encodeName(oldMemoryTypeName),
					newHWMemoryType);

			Attribute oldMemoryTypeClassifiersAttribute = oldMemoryTypeElement.getAttribute(CLASSIFIERS);

			if (oldMemoryTypeClassifiersAttribute != null) {

				newHWMemoryType.setAttribute(oldMemoryTypeClassifiersAttribute.clone());
			} else {
				List<Element> oldMemoryTypeClassifiersList = oldMemoryTypeElement.getChildren(CLASSIFIERS);

				for (Element oldMemoryTypeClassifier : oldMemoryTypeClassifiersList) {
					newHWMemoryType.addContent(oldMemoryTypeClassifier.clone());
				}
			}
		}

		if (oldMemoryTypeName != null) {
			newHWMemoryType.setAttribute(NAME, oldMemoryTypeName);
		}

		Element oldMemoryTypeSize = oldMemoryTypeElement.getChild("size");

		if (oldMemoryTypeSize != null) {
			newHWMemoryType.addContent(oldMemoryTypeSize.clone());
		}

		/*-migrating custom properties */

		migrateCustomProperties(oldMemoryTypeElement, newHWMemoryType);

	}

	/**
	 * This method is used to migrate HwSystem element from 0.8.3 to HwStructure
	 * element as per 0.9.0 Below are the direct sub-elements of HwSystem which are
	 * migrated:
	 *
	 * <ul>
	 * <li>Quartz migrated as FrequencyDomain</li>
	 * <li>ECU migrated as HwStructure with type as ECU</li>
	 * <li>Memory objects migrated as Memory and Cache based on the type of
	 * MemoryDefinition linked to it</li>
	 * <li>Network objects migrated as ConnectionHandlers</li>
	 * <li>Port objects migrated as HwPort</li>
	 * </ul>
	 *
	 * @param newHWModelElement Element. JDOM Element equivalent to 0.9.0 HwModel
	 * @param oldHWSystem       Element. JDOM Element equivalent to 0.8.3 HwSystem
	 */
	private void migrateSystem(final Element newHWModelElement, Element oldHWSystem) {

		String oldHWSystemName = oldHWSystem.getAttributeValue(NAME);
		Element newHWSystem = new Element(STRUCTURES);

		if (oldHWSystemName != null) {
			newHWSystem.setAttribute(NAME, oldHWSystemName);
		}

		newHWSystem.setAttribute(STRUCTURE_TYPE, "System");

		newHWModelElement.addContent(newHWSystem);

		hwTransformationCache.getNewSystemsMap().put(HelperUtil.encodeName(oldHWSystemName), newHWSystem);

		migrateAllQuartzes(newHWModelElement);

		List<Element> oldHWecus = oldHWSystem.getChildren("ecus");

		for (Element oldHWEcu : oldHWecus) {

			migrateECU(newHWSystem, oldHWEcu);
		}

		/*-migrating memories*/
		migrateMemoriesAndCaches(oldHWSystem, newHWSystem, true, true);

		/*-migrating networks*/
		migrateNetworks(oldHWSystem, newHWSystem);

		/*-migrate ports*/

		migratePorts(oldHWSystem, newHWSystem);

		/*-migrating custom properties */

		migrateCustomProperties(oldHWSystem, newHWSystem);

	}

	/**
	 * This method is used to migrate HwPort, ComplexPort element from 0.8.3 to
	 * HwPort element as per 0.9.0
	 *
	 * @param oldHWModelElement Element. JDOM Element equivalent to 0.8.3 Core or
	 *                          ECU
	 * @param newHWModelElement Element. JDOM Element equivalent to 0.9.0
	 *                          ProcessingUnit or HwStructure( with type as ECU)
	 */

	private void migratePorts(Element oldHWElement, Element newHWElement) {

		List<Element> oldHWPortElementList = oldHWElement.getChildren("ports");

		for (Element oldHWPort : oldHWPortElementList) {

			Element newHWPort = new Element("ports");

			String oldHWPortName = oldHWPort.getAttributeValue(NAME);
			String oldHWPortBitWidth = oldHWPort.getAttributeValue("bitWidth");
			String oldHWPortMaster = oldHWPort.getAttributeValue("master");

			if (oldHWPortName != null) {
				newHWPort.setAttribute(NAME, oldHWPortName);
			}
			if (oldHWPortBitWidth != null) {
				newHWPort.setAttribute("bitWidth", oldHWPortBitWidth);
			}
			if (oldHWPortMaster != null) {

				if (Boolean.parseBoolean(oldHWPortMaster.trim())) {
					newHWPort.setAttribute("portType", "initiator");
				} else {
					newHWPort.setAttribute("portType", "responder");
				}
			}

			newHWElement.addContent(newHWPort);

			/*-migrating custom properties */

			migrateCustomProperties(oldHWPort, newHWPort);

		}
	}

	/**
	 * This method is used to migrate Memory element from 0.8.3 to Memory element or
	 * Cache element as per 0.9.0
	 *
	 * Based on the type of MemoryType element (in 0.8.3) -> either Cache element or
	 * Memory element are generated in 0.9.0
	 *
	 * -- If type is CACHE, then Cache element is generated. For other types, Memory
	 * element is generated
	 *
	 *
	 * Inside Memory element (of 0.8.3) accordingly, PreScaler and HwPort elements
	 * are migrated to appropriate elements in 0.9.0
	 *
	 * @param oldHWModelElement Element. JDOM Element equivalent to 0.8.3 Core or
	 *                          ECU or MicroController or System
	 * @param newHWModelElement Element. JDOM Element equivalent to 0.9.0
	 *                          ProcessingUnit or HwStructure (with type as ECU or
	 *                          MicroController or System)
	 */

	private void migrateMemoriesAndCaches(Element oldElement, Element newElement, boolean migrateCache, boolean migrateMemory) {
		List<Element> oldMemoryElementList = getAllMemories(oldElement);

		for (Element oldMemoryElement : oldMemoryElementList) {

			Element newMemoryElement = new Element(MODULES);

			String oldMemoryName = oldMemoryElement.getAttributeValue(NAME);

			if (oldMemoryName != null) {
				newMemoryElement.setAttribute(NAME, oldMemoryName);
			}

			String oldTypeDefinitionName = HelperUtil.getSingleElementNameFromAttributeOrChildeElement(TYPE, oldMemoryElement);

			String newDefinitionType = "MemoryDefinition";

			boolean isElementAddedToTheParent = false;

			if (oldTypeDefinitionName != null) {

				if (migrateCache) {
					if (hwTransformationCache.getNewCacheTypesDefinitionMap().containsKey(oldTypeDefinitionName)) {

						newDefinitionType = "CacheDefinition";

						if (migrateCache && !migrateMemory) {
							// In this scenario, Core object is being migrated
							newMemoryElement.setName("caches");
						}

						newMemoryElement.setAttribute(TYPE, "am:Cache", XSI_NAMESPACE);

						hwTransformationCache.getNewCachesMap().put(HelperUtil.encodeName(oldMemoryName),
								newMemoryElement);

						// Adding memory object to newHW
						newElement.addContent(newMemoryElement);
						isElementAddedToTheParent = true;

					}
				}

				if (migrateMemory) {
					if (hwTransformationCache.getNewMemoryTypesDefinitionMap()
							.containsKey(oldTypeDefinitionName)) {
						newMemoryElement.setAttribute(TYPE, AM_MEMORY, XSI_NAMESPACE);

						hwTransformationCache.getNewMemoriesMap().put(HelperUtil.encodeName(oldMemoryName),
								newMemoryElement);

						// Adding memory object to newHW
						newElement.addContent(newMemoryElement);
						isElementAddedToTheParent = true;
					}
				}

			} else {
				if (migrateMemory) {
					newMemoryElement.setAttribute(TYPE, AM_MEMORY, XSI_NAMESPACE);

					hwTransformationCache.getNewMemoriesMap().put(HelperUtil.encodeName(oldMemoryName),
							newMemoryElement);

					// Adding memory object to newHW
					newElement.addContent(newMemoryElement);
					isElementAddedToTheParent = true;

				}
			}

			if (isElementAddedToTheParent) {

				List<Element> newHWMemoryDefinitions = migrateAttributeorElementData(oldMemoryElement, TYPE, DEFINITION,
						newDefinitionType);
				for (Element newHWMemoryDefinition : newHWMemoryDefinitions) {

					newMemoryElement.addContent(newHWMemoryDefinition);
				}

				migratePrescaler(oldMemoryElement, newMemoryElement);

				/*-migrate ports*/

				migratePorts(oldMemoryElement, newMemoryElement);

				/*-migrating custom properties */

				migrateCustomProperties(oldMemoryElement, newMemoryElement);
			}

		}
	}

	/**
	 * This method is used to migrate Network element from 0.8.3 to
	 * ConnectionHandler element as per 0.9.0
	 *
	 * Inside Memory element (of 0.8.3) accordingly, PreScaler and HwPort elements
	 * are migrated to appropriate elements in 0.9.0
	 *
	 * @param oldHWModelElement Element. JDOM Element equivalent to 0.8.3 ECU or
	 *                          MicroController or System
	 * @param newHWModelElement Element. JDOM Element equivalent to 0.9.0
	 *                          HwStructure( with type as ECU or MicroController or
	 *                          System)
	 */

	private void migrateNetworks(Element oldElement, Element newElement) {
		List<Element> oldNetworkList = getAllNetworks(oldElement);

		for (Element oldNetwork : oldNetworkList) {

			Element newNetwork = new Element(MODULES);

			String oldNetworkName = oldNetwork.getAttributeValue(NAME);

			if (oldNetworkName != null) {
				newNetwork.setAttribute(NAME, oldNetworkName);
			}

			newNetwork.setAttribute(TYPE, "am:ConnectionHandler", XSI_NAMESPACE);

			List<Element> newHWNetworkTypeDefinitions = migrateAttributeorElementData(oldNetwork, TYPE, DEFINITION,
					"ConnectionHandlerDefinition");

			for (Element newHWNetworkTypeDefinition : newHWNetworkTypeDefinitions) {

				newNetwork.addContent(newHWNetworkTypeDefinition);
			}

			migratePrescaler(oldNetwork, newNetwork);

			/*-migrate ports*/

			migratePorts(oldNetwork, newNetwork);

			// Adding network object to newHW
			newElement.addContent(newNetwork);

			/*-migrating custom properties */

			migrateCustomProperties(oldNetwork, newNetwork);
		}
	}

	/**
	 * This method is used to fetch all the Network elements inside a ComplexNode of
	 * AMALTHEA 0.8.3 - > recursively
	 *
	 * @param oldComplexNode Element compatible to Network (as per 0.8.3)
	 * @return List<Element> elements of Network (as per 0.8.3)
	 */
	private List<Element> getAllNetworks(Element oldComplexNode) {
		List<Element> oldNetworkList = new ArrayList<>();

		List<Element> oldComplexNodeList = oldComplexNode.getChildren(NETWORKS);
		oldNetworkList.addAll(oldComplexNodeList);

		populateAllSubNetworks(oldNetworkList, oldComplexNode.getChildren(NETWORKS));
		populateAllSubNetworks(oldNetworkList, oldComplexNode.getChildren(MEMORIES));
		populateAllSubNetworks(oldNetworkList, oldComplexNode.getChildren(COMPONENTS));

		return oldNetworkList;
	}

	/**
	 * This method is used to fetch all the Memory elements inside a ComplexNode of
	 * AMALTHEA 0.8.3 - > recursively
	 *
	 * @param oldComplexNode Element compatible to Memory (as per 0.8.3)
	 * @return List<Element> elements of Memory (as per 0.8.3)
	 */

	private List<Element> getAllMemories(Element oldComplexNode) {
		List<Element> oldMemoryList = new ArrayList<>();

		List<Element> oldComplexNodeList = oldComplexNode.getChildren(MEMORIES);
		oldMemoryList.addAll(oldComplexNodeList);

		populateAllSubMemories(oldMemoryList, oldComplexNode.getChildren(MEMORIES));
		populateAllSubMemories(oldMemoryList, oldComplexNode.getChildren(NETWORKS));
		populateAllSubMemories(oldMemoryList, oldComplexNode.getChildren(COMPONENTS));

		return oldMemoryList;
	}

	/**
	 * This method is used to fetch all the Sub Memory elements inside a complex
	 * node (as per 0.8.3) -> recursively
	 *
	 * @param oldMemoryElements
	 * @param oldComplexNodeElements
	 */
	private void populateAllSubMemories(List<Element> oldMemoryElements, List<Element> oldComplexNodeElements) {
		for (Element oldHWMemory : oldComplexNodeElements) {

			final StringBuilder xpathBufferForMemoryDefinitions = new StringBuilder();

			xpathBufferForMemoryDefinitions.append("./memories");
			xpathBufferForMemoryDefinitions.append("|");
			xpathBufferForMemoryDefinitions.append(".//components/memories");
			xpathBufferForMemoryDefinitions.append("|");
			xpathBufferForMemoryDefinitions.append(".//nestedComponents/memories");
			xpathBufferForMemoryDefinitions.append("|");
			xpathBufferForMemoryDefinitions.append(".//memories/memories");
			xpathBufferForMemoryDefinitions.append("|");
			xpathBufferForMemoryDefinitions.append(".//networks/memories");

			final List<Element> memoryElements = HelperUtil.getXpathResult(oldHWMemory,
					xpathBufferForMemoryDefinitions.toString(), Element.class, XSI_NAMESPACE,
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_090, AM));

			oldMemoryElements.addAll(memoryElements);
		}
	}

	/**
	 * This method is used to recursively fetch all Network elements (compatible as
	 * per 0.8.3)
	 *
	 * @param oldNetworkElements
	 * @param oldComplexNodeElements
	 */
	private void populateAllSubNetworks(List<Element> oldNetworkElements, List<Element> oldComplexNodeElements) {
		for (Element oldHWNetwork : oldComplexNodeElements) {

			final StringBuilder xpathBufferForMemoryDefinitions = new StringBuilder();

			xpathBufferForMemoryDefinitions.append("./networks");
			xpathBufferForMemoryDefinitions.append("|");
			xpathBufferForMemoryDefinitions.append(".//components/networks");
			xpathBufferForMemoryDefinitions.append("|");
			xpathBufferForMemoryDefinitions.append(".//nestedComponents/networks");
			xpathBufferForMemoryDefinitions.append("|");
			xpathBufferForMemoryDefinitions.append(".//memories/networks");
			xpathBufferForMemoryDefinitions.append("|");
			xpathBufferForMemoryDefinitions.append(".//networks/networks");

			final List<Element> networkElements = HelperUtil.getXpathResult(oldHWNetwork,
					xpathBufferForMemoryDefinitions.toString(), Element.class, XSI_NAMESPACE,
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_090, AM));

			oldNetworkElements.addAll(networkElements);

		}
	}

	/**
	 * This method is used to migrate all Quartz elements present in the model scope
	 * to FrequencyDomain elements as per 0.9.0
	 *
	 * @param newHWModel JDOM element ->equivalent to HwModel element of 0.9.0
	 */
	private void migrateAllQuartzes(Element newHWModel) {

		Collection<Element> oldQuartzElementColl = hwTransformationCache.getOldHwQuartzsMap().values();

		for (Element oldQuartzElement : oldQuartzElementColl) {

			Element newHWFrequencyDomain = new Element("domains");

			newHWFrequencyDomain.setAttribute(TYPE, "am:FrequencyDomain", XSI_NAMESPACE);

			String oldHWQuartzName = oldQuartzElement.getAttributeValue(NAME);

			if (oldHWQuartzName != null) {
				newHWFrequencyDomain.setAttribute(NAME, oldHWQuartzName);
			}

			Element oldFrequencyElement = oldQuartzElement.getChild("frequency");

			if (oldFrequencyElement != null) {
				Element newFrequencyDefaultValueElement = oldFrequencyElement.clone();

				newFrequencyDefaultValueElement.detach();

				newFrequencyDefaultValueElement.setName("defaultValue");

				newHWFrequencyDomain.addContent(newFrequencyDefaultValueElement);
			}

			newHWModel.addContent(newHWFrequencyDomain);

			/*-migration of custom properties */
			migrateCustomProperties(oldQuartzElement, newHWFrequencyDomain);

			hwTransformationCache.getNewHwQuartzsFrequencyDomainMap().put(HelperUtil.encodeName(oldHWQuartzName),
					newHWFrequencyDomain);
		}
	}

	/**
	 * This method is used to migrate the ECU elements to HwStructure with type as
	 * ECU(as per 0.9.0)
	 *
	 * During the migration, below elements contained by the ECU are also migrated:
	 * <ul>
	 * <li>Memory to Cache/Memory as per 0.9.0</li>
	 * <li>Network to ConnectionHandler as per 0.9.0</li>
	 * <li>HwPort, ComplexPort to hwPort as per 0.9.0</li>
	 * <li>MicroController to HwStructure as per 0.9.0</li>
	 * </ul>
	 *
	 * @param newHWSystem
	 * @param oldEcu
	 */
	private void migrateECU(Element newHWSystem, Element oldEcu) {
		String oldEcuName = oldEcu.getAttributeValue(NAME);

		hwTransformationCache.getOldEcusMap().put(HelperUtil.encodeName(oldEcuName), oldEcu);

		Element newEcu = new Element(STRUCTURES);

		if (oldEcuName != null) {
			newEcu.setAttribute(NAME, oldEcuName);
		}

		newEcu.setAttribute(STRUCTURE_TYPE, "ECU");

		newHWSystem.addContent(newEcu);

		hwTransformationCache.getNewEcusMap().put(HelperUtil.encodeName(oldEcuName), newEcu);

		/*-migrating memories*/
		migrateMemoriesAndCaches(oldEcu, newEcu, true, true);

		/*-migrating networks*/
		migrateNetworks(oldEcu, newEcu);

		/*-migrate ports*/

		migratePorts(oldEcu, newEcu);

		List<Element> oldHWMicroControllers = oldEcu.getChildren("microcontrollers");

		for (Element oldHWMicroController : oldHWMicroControllers) {

			migrateMicroController(newEcu, oldHWMicroController);
		}

		/*-migration of custom properties */
		migrateCustomProperties(oldEcu, newEcu);
	}

	/**
	 * This method is used to migrate the MicroController to HwStructure with type
	 * as MicroController(as per 0.9.0)
	 *
	 * During the migration, below elements contained by the ECU are also migrated:
	 * <ul>
	 * <li>Memory to Cache/Memory as per 0.9.0</li>
	 * <li>Network to ConnectionHandler as per 0.9.0</li>
	 * <li>HwPort, ComplexPort to hwPort as per 0.9.0</li>
	 * <li>MicroController to HwStructure as per 0.9.0</li>
	 * <li>Core to ProcessingUnit as per 0.9.0</li>
	 * </ul>
	 *
	 * @param newHWSystem
	 * @param oldHWEcu
	 */

	private void migrateMicroController(Element newEcu, Element oldMicroController) {
		String oldMicroControllerName = oldMicroController.getAttributeValue(NAME);

		hwTransformationCache.getOldMicroControllersMap().put(HelperUtil.encodeName(oldMicroControllerName),
				oldMicroController);

		Element newMicroController = new Element(STRUCTURES);

		if (oldMicroControllerName != null) {
			newMicroController.setAttribute(NAME, oldMicroControllerName);
		}

		newMicroController.setAttribute(STRUCTURE_TYPE, "Microcontroller");

		newEcu.addContent(newMicroController);

		hwTransformationCache.getNewMicroControllersMap().put(HelperUtil.encodeName(oldMicroControllerName),
				newMicroController);

		/*-migrating memories*/
		migrateMemoriesAndCaches(oldMicroController, newMicroController, true, true);

		/*-migrating networks*/
		migrateNetworks(oldMicroController, newMicroController);

		/*-migrate ports*/

		migratePorts(oldMicroController, newMicroController);

		/*
		 * if (!hasProcessedCores) {
		 *
		 * hasProcessedCores = true;
		 *
		 * migrateAllCores_modelscope(newHWMicroController); }
		 */
		List<Element> oldHWCores = oldMicroController.getChildren("cores");

		for (Element oldHWCore : oldHWCores) {

			/* migrating cores belonging to specific Microcontroller */

			migrateCore(newMicroController, oldHWCore);

			/*-migrating memories*/

			// TODO: cross check : As in case of new HW Model, core can not contain
			// memories, corresponding memories of core are added to the HWStructure
			// (microcontroller type)
			migrateMemoriesAndCaches(oldHWCore, newMicroController, false, true);

			/*-migrating networks*/
			migrateNetworks(oldHWCore, newMicroController);

		}

		/*-migration of custom properties */
		migrateCustomProperties(oldMicroController, newMicroController);
	}

	/**
	 * This method is used to migrate Core as ProcessingUnit. During the migration
	 * process, based on the ClockRation of the PreScaler -> appropriate
	 * FrequencyDomain is referred to the ProcessingUnit element
	 *
	 * @param newMicroController
	 * @param oldCore
	 */
	private void migrateCore(Element newMicroController, Element oldCore) {
		Element newCore = new Element(MODULES);

		newMicroController.addContent(newCore);

		newCore.setAttribute(TYPE, "am:ProcessingUnit", XSI_NAMESPACE);

		String oldCoreName = oldCore.getAttributeValue(NAME);

		if (oldCoreName != null) {
			newCore.setAttribute(NAME, oldCoreName);
		}

		List<Element> newProcessingUnitList = migrateAttributeorElementData(oldCore, "coreType", DEFINITION,
				"ProcessingUnitDefinition");

		for (Element newProcessingUnit : newProcessingUnitList) {
			newCore.addContent(newProcessingUnit);
		}

		migratePrescaler(oldCore, newCore);

		/*-migrate ports*/

		migratePorts(oldCore, newCore);

		// TODO PROBLEM
		// if local memory of a core is transformed
		// then link to definition is empty
		migrateMemoriesAndCaches(oldCore, newCore, true, false);

		hwTransformationCache.getNewCoresMap().put(HelperUtil.encodeName(oldCoreName), newCore);

		/*-migration of custom properties */
		migrateCustomProperties(oldCore, newCore);
	}

	/**
	 * This method is used to migrate a PreScaler to FrequencyDomain. -- ClockRatio
	 * of the PreScaler is used to refer appropriate FrequencyDomain
	 *
	 * @param oldHWElement e.g: Core, Memory
	 * @param newHWElement
	 */
	private void migratePrescaler(Element oldHWElement, Element newHWElement) {
		Element oldPrescaler = oldHWElement.getChild("prescaler");

		if (oldPrescaler != null) {

			String oldQuartzName = HelperUtil.getSingleElementNameFromAttributeOrChildeElement("quartz",
					oldPrescaler);

			if (oldQuartzName != null) {

				String clockRatio = oldPrescaler.getAttributeValue("clockRatio");

				if (clockRatio == null) {
					clockRatio = "0.0";
				}

				String newFrequencyDomainName = "";
				if ("1.0".equals(clockRatio.trim())) {
					newFrequencyDomainName = oldQuartzName;
				} else {
					// As the clock ratio is not 1.0, in this case -> cloning standard
					// FrequencyDomain and setting the updated Frequency
					newFrequencyDomainName = oldQuartzName + "__" + clockRatio;
				}

				Element newFrequencyDomain = hwTransformationCache.getNewHwQuartzsFrequencyDomainMap()
						.get(newFrequencyDomainName);

				if (newFrequencyDomain == null) {

					Element newFrequencyDomainBaseFrequency = hwTransformationCache
							.getNewHwQuartzsFrequencyDomainMap().get(oldQuartzName);

					if (newFrequencyDomainBaseFrequency != null) {

						/*-getting parent element of existing FrequencyDomain -> HWModel */
						Element parentElement = newFrequencyDomainBaseFrequency.getParentElement();

						newFrequencyDomain = newFrequencyDomainBaseFrequency.clone();

						newFrequencyDomain.detach();

						newFrequencyDomainName = oldQuartzName + "__" + clockRatio;

						newFrequencyDomain.setAttribute(NAME, newFrequencyDomainName);

						/*-Adding newly created FrequencyDomain to HWModel*/

						parentElement.addContent(newFrequencyDomain);

						hwTransformationCache.getNewHwQuartzsFrequencyDomainMap().put((newFrequencyDomainName),
								newFrequencyDomain);

						Element newFrequency = newFrequencyDomain.getChild("defaultValue");

						if (newFrequency != null) {
							Attribute newFrequencyValueAttribute = newFrequency.getAttribute(VALUE);

							if (newFrequencyValueAttribute != null) {

								try {
									String value = newFrequencyValueAttribute.getValue();

									String newFrequencyValue = Double.parseDouble(value) * Double.parseDouble(clockRatio) + "";

									newFrequencyValueAttribute.setValue(newFrequencyValue);

								} catch (Exception e) {
									logger.error("Exception occured during creation of new FrequencyDomain : {0}",
											newFrequencyDomain.getAttributeValue(NAME));
								}
							}
						}
					}
				}

				// updating reference
				if (newFrequencyDomain != null) {
					Element newFrequencyDoaminReference = new Element("frequencyDomain");

					newFrequencyDoaminReference.setAttribute("href",
							AMLT_PREFIX
									+ HelperUtil.encodeNameForReference(newFrequencyDomain.getAttributeValue(NAME))
									+ "?type=FrequencyDomain");

					newHWElement.addContent(newFrequencyDoaminReference);
				}
			}
		}
	}

	private List<Element> migrateAttributeorElementData(Element oldHWElement, String attributeOrChildElementName,
			String newChildElementName, String newChildElementType) {

		List<Element> newHWElements = new ArrayList<>();

		Attribute oldHWAttribute = oldHWElement.getAttribute(attributeOrChildElementName);

		if (oldHWAttribute != null) {

			String oldHWRefElementsValue = oldHWAttribute.getValue();

			StringTokenizer stk = new StringTokenizer(oldHWRefElementsValue);

			while (stk.hasMoreTokens()) {
				String nextToken = stk.nextToken();

				String newHWRefElementName = updateReferenceStringWithNewType(nextToken, newChildElementType);

				Element newHWRefElement = new Element(newChildElementName);

				newHWRefElement.setAttribute("href", AMLT_PREFIX + (newHWRefElementName));

				newHWElements.add(newHWRefElement);

			}

		} else {
			List<Element> oldHWChildElements = oldHWElement.getChildren(attributeOrChildElementName);

			for (Element oldHWChildElement : oldHWChildElements) {

				String oldHWhrefValue = oldHWChildElement.getAttributeValue("href");
				if (oldHWhrefValue != null) {

					String newHWRefElementName = updateReferenceStringWithNewType(oldHWhrefValue, newChildElementType);

					Element newHWRefElement = new Element(newChildElementName);

					newHWRefElement.setAttribute("href", newHWRefElementName);

					newHWElements.add(newHWRefElement);
				}
			}
		}

		oldHWElement.removeAttribute(attributeOrChildElementName);

		oldHWElement.removeChildren(attributeOrChildElementName);

		return newHWElements;
	}

	private String updateReferenceStringWithNewType(String inputString, String newType) {

		if (inputString != null) {
			int startIndex = inputString.indexOf("?type=");

			if (startIndex != -1) {
				String oldHWRefElementName = inputString.substring(0, startIndex);

				return (oldHWRefElementName) + "?type=" + newType;
			}
			return inputString;
		}
		return inputString;
	}

	private void migrateCustomProperties(Element oldHwElement, Element newHwElement) {

		List<Element> customProperties = oldHwElement.getChildren("customProperties");

		for (Element customProperty : customProperties) {

			Element newHwCustomProperty = customProperty.clone();

			newHwCustomProperty.detach();

			newHwElement.addContent(newHwCustomProperty);
		}
	}

	/**
	 * This method is used to get the HWCacheBuilder object
	 *
	 * @param caches The list of all caches.
	 * @return HWCacheBuilder
	 */
	private HWTransformationCache getHWTransformationCache(List<ICache> caches) {
		if (caches == null)
			return null;

		for (final ICache cache : caches) {

			if (cache instanceof HWCacheBuilder) {
				Map<File, Map<String, Object>> cacheMap = cache.getCacheMap();

				if (cacheMap != null && !cacheMap.isEmpty()) {
					Map<String, Object> map = cacheMap.values().iterator().next();

					if (map != null) {
						Object object = map.get("globalCache");
						return (HWTransformationCache) object;
					}
				}
			}
		}

		return new HWTransformationCache();
	}

}
