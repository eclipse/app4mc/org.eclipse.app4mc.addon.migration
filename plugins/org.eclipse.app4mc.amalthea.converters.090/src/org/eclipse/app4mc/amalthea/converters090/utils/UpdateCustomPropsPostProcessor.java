/**
 ********************************************************************************
 * Copyright (c) 2018-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters090.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.IPostProcessor;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(property = { ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.8.3",
		ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.9.0" })

public class UpdateCustomPropsPostProcessor implements IPostProcessor {

	private static final String CORE = "Core";
	private static final String MEMORY = "Memory";
	private static final String VALUE = "value";
	private static final String AMLT_PREFIX = "amlt:/#";
	private static final String HW_STRUCTURE = "HwStructure";

	private static final Namespace XSI_NAMESPACE = AmaltheaNamespaceRegistry.getGenericNamespace("xsi");

	@Reference
	SessionLogger logger;

	@Reference
	private HWCacheBuilder cache;

	@Override
	public void process(Map<File, Document> filename2documentMap) {

		if (logger != null) {
			logger.info("Start UpdateCustomPropsPostProcessor from 0.8.3 to 0.9.0");
		}

		List<String> hwTypes083 = initializeHwTypesList();

		Collection<Document> values = filename2documentMap.values();

		for (Document document : values) {
			Element rootElement = document.getRootElement();

			final List<Element> customPropertyValueElementList = HelperUtil.getXpathResult(rootElement,
					".//customProperties//*[@xsi:type=\"am:ReferenceObject\"]", Element.class, XSI_NAMESPACE);

			for (Element customPropertyValueElement : customPropertyValueElementList) {

				Entry<String, String> entry = HelperUtil.getSingleElementsNameandTypeFromAttributeOrChildeElement(VALUE,
						customPropertyValueElement);

				// As the scope of updating references in CustomProperties object is limited to
				// HWModel : Below condition ensures that the type of the element is part of
				// HWModel in 0.8.3
				if (entry != null && hwTypes083.contains(entry.getValue())) {

					customPropertyValueElement.removeAttribute(VALUE);
					customPropertyValueElement.removeChild(VALUE);

					String updatedTypeAfterMigration = getUpdatedType(entry.getKey(), entry.getValue());

					// If more than one file is supplied for model migration: cross-document
					// references can be used -> due to this reason there is a special convention to
					// generate references with href
					if (values.size() > 1) {
						Element valueElement = new Element(VALUE);

						valueElement.setAttribute("type", "am:" + updatedTypeAfterMigration, XSI_NAMESPACE);

						valueElement.setAttribute("href",
								AMLT_PREFIX + entry.getKey() + "?type=" + updatedTypeAfterMigration);

						customPropertyValueElement.addContent(valueElement);
					} else {
						customPropertyValueElement.setAttribute(VALUE,
								entry.getKey() + "?type=" + updatedTypeAfterMigration);
					}
				}
			}
		}
	}

	/**
	 * This method initializes a list containing HwModel element types from APP4MC
	 * 0.8.3 AMALTHEA model
	 *
	 * @return
	 */
	private List<String> initializeHwTypesList() {

		List<String> ls = new ArrayList<>();

		ls.add("HWModel");
		ls.add("ComplexNode");
		ls.add("HwSystem");
		ls.add("ECU");
		ls.add("Microcontroller");
		ls.add(CORE);
		ls.add(MEMORY);
		ls.add("Network");
		ls.add("Quartz");
		ls.add("HwComponent");
		ls.add("HardwareTypeDescription");
		ls.add("AbstractionType");
		ls.add("SystemType");
		ls.add("ECUType");
		ls.add("MicrocontrollerType");
		ls.add("CoreType");
		ls.add("MemoryType");
		ls.add("NetworkType");
		ls.add("HwPort");
		ls.add("Pin");
		ls.add("ComplexPort");
		ls.add("ComplexPin");
		ls.add("Prescaler");
		ls.add("CrossbarSwitch");
		ls.add("Bus");
		ls.add("Bridge");
		ls.add("LatencyAccessPath");
		ls.add("HwAccessPath");
		ls.add("AccessPathRef");
		ls.add("LatencyConstant");
		ls.add("LatencyDeviation");
		ls.add("HwAccessPathRef");
		ls.add("HwElementRef");

		return ls;
	}

	private String getUpdatedType(String name, String value) {
		if (value == null) {
			return null;
		}

		HWTransformationCache transformationCache = getHWTransformationCache();

		if ("MemoryType".equals(value)) {
			if (transformationCache.getNewCacheTypesDefinitionMap().containsKey(name)) {
				value = "CacheDefinition";
			} else if (transformationCache.getNewMemoriesMap().containsKey(name)) {
				value = "MemoryDefinition";
			}

		} else if (MEMORY.equals(value)) {
			if (transformationCache.getNewCachesMap().containsKey(name)) {
				value = "Cache";
			} else if (transformationCache.getNewMemoriesMap().containsKey(name)) {
				value = MEMORY;
			}
		} else if (CORE.equals(value)) {
			value = "ProcessingUnit";
		} else if ("Network".equals(value)) {
			value = "ConnectionHandler";
		} else if ("HwSystem".equals(value) || "ECU".equals(value) || "Microcontroller".equals(value)) {
			value = HW_STRUCTURE;
		} else if ("CoreType".equals(value)) {
			value = "ProcessingUnitDefinition";
		} else if ("NetworkType".equals(value)) {
			value = "ConnectionHandlerDefinition";
		} else if ("Quartz".equals(value)) {
			value = "FrequencyDomain";
		}

		return value;
	}

	private HWTransformationCache getHWTransformationCache() {
		Map<File, Map<String, Object>> cacheMap = cache.getCacheMap();

		if (cacheMap != null && !cacheMap.isEmpty()) {
			Map<String, Object> map = cacheMap.values().iterator().next();

			if (map != null) {
				Object object = map.get("globalCache");
				return (HWTransformationCache) object;
			}
		}

		return new HWTransformationCache();
	}

}
