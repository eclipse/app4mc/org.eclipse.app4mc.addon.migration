/**
 ********************************************************************************
 * Copyright (c) 2018-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.converters090.utils;

import java.util.HashMap;
import java.util.Map;

import org.jdom2.Element;

public class HWTransformationCache {

	private final Map<String, Element> newSystemsMap = new HashMap<>();
	private final Map<String, Element> newEcusMap = new HashMap<>();
	private final Map<String, Element> oldEcusMap = new HashMap<>();
	private final Map<String, Element> newMicroControllersMap = new HashMap<>();
	private final Map<String, Element> oldMicroControllersMap = new HashMap<>();
	private final Map<String, Element> newCoresMap = new HashMap<>();
	private final Map<String, Element> oldCoresMap = new HashMap<>();
	private final Map<String, Element> newMemoriesMap = new HashMap<>();
	private final Map<String, Element> newMemoryTypesDefinitionMap = new HashMap<>();

	private final Map<String, Element> newCacheTypesDefinitionMap = new HashMap<>();
	private final Map<String, Element> newCachesMap = new HashMap<>();

	private final Map<String, Element> oldMemoryTypesDefinitionMap = new HashMap<>();
	private final Map<String, Element> oldHwQuartzsMap = new HashMap<>();
	private final Map<String, Element> newHwQuartzsFrequencyDomainMap = new HashMap<>();
	private final Map<String, Element> newFeatureCategoriesMap = new HashMap<>();
	private final Map<String, Element> newFeaturesMap = new HashMap<>();

	public Map<String, Element> getNewSystemsMap() {
		return newSystemsMap;
	}

	public Map<String, Element> getNewEcusMap() {
		return newEcusMap;
	}

	public Map<String, Element> getOldEcusMap() {
		return oldEcusMap;
	}

	public Map<String, Element> getNewMicroControllersMap() {
		return newMicroControllersMap;
	}

	public Map<String, Element> getOldMicroControllersMap() {
		return oldMicroControllersMap;
	}

	public Map<String, Element> getNewCoresMap() {
		return newCoresMap;
	}

	public Map<String, Element> getOldCoresMap() {
		return oldCoresMap;
	}

	public Map<String, Element> getNewMemoriesMap() {
		return newMemoriesMap;
	}

	public Map<String, Element> getNewMemoryTypesDefinitionMap() {
		return newMemoryTypesDefinitionMap;
	}

	public Map<String, Element> getNewCacheTypesDefinitionMap() {
		return newCacheTypesDefinitionMap;
	}

	public Map<String, Element> getNewCachesMap() {
		return newCachesMap;
	}

	public Map<String, Element> getOldMemoryTypesDefinitionMap() {
		return oldMemoryTypesDefinitionMap;
	}

	public Map<String, Element> getOldHwQuartzsMap() {
		return oldHwQuartzsMap;
	}

	public Map<String, Element> getNewHwQuartzsFrequencyDomainMap() {
		return newHwQuartzsFrequencyDomainMap;
	}

	public Map<String, Element> getNewFeatureCategoriesMap() {
		return newFeatureCategoriesMap;
	}

	public Map<String, Element> getNewFeaturesMap() {
		return newFeaturesMap;
	}

}
