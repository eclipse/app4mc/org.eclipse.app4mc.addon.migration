/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters080.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * This class is responsible for converting the OS Model elements from 0.7.2 to 0.8.0 version format of AMALTHEA model
 *
 * @author mez2rng
 *
 */
@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.7.2",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.8.0"},
		service = IConverter.class)

public class OSConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {

		logger.info(
				"Migration from 0.7.2 to 0.8.0 : Executing OS converter for model file : {0}", targetFile.getName());


		final Document root = filename2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		moveOsDataConsistencyElement(rootElement);
	}


	/**
	 * Below migration is for the metamodel change w.r.t. OS model (For further details, check : Bug 515090 )
	 *
	 * OsDataConsistency is shifted from OSModel element to OperatingSystem element in APP4MC 0.8.0
	 *
	 * @param rootElement
	 */
	private void moveOsDataConsistencyElement(final Element rootElement) {


		final List<Element> osDataConsistencyElements = HelperUtil.getXpathResult(
				rootElement,
				"./osModel/osDataConsistency",
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		final List<Element> operatingSystemElements = HelperUtil.getXpathResult(
				rootElement,
				"./osModel/operatingSystems",
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		if (!osDataConsistencyElements.isEmpty()) {

			/*- As per the metamodel (0.7.2) there is a possibility to have only one OsDataConsistency inside OsModel*/

			final Element osDataConsistencyElement = osDataConsistencyElements.get(0);

			for (final Element operatingSystemElement : operatingSystemElements) {

				final Element osDataConsistencyElementClone = osDataConsistencyElement.clone();

				/*-As per change in 0.8.0 : OsDataConsistency element is added as a part of OperatingSystem element */

				operatingSystemElement.addContent(osDataConsistencyElementClone);
			}

			logger.warn(
					"OsDataConsistency element is removed from OsModel, and is added (cloned content) to each OperatingSystem element (which is part of corresponding OsModel).");

			/*-As per change in 0.8.0 : OsDataConsistency element is removed from OsModel */
			osDataConsistencyElement.getParentElement().removeContent(osDataConsistencyElement);
		}
	}

}
