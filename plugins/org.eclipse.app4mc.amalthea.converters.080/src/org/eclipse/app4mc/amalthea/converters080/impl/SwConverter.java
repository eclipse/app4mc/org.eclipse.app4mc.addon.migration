/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters080.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * This class is responsible for converting the SW Model elements from 0.7.2 to 0.8.0 version format of AMALTHEA model
 *
 * @author mez2rng
 *
 */
@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.7.2",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.8.0"},
		service = IConverter.class)

public class SwConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {

		logger.info(
				"Migration from 0.7.2 to 0.8.0 : Executing SW converter for model file : {0}", targetFile.getName());

		final Document root = filename2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		updateModeLabel(rootElement);
		updateDataSize(rootElement);
		updateProbabiltitySwitch(rootElement);
	}


	/**
	 * Below migration is for the metamodel change : ProbabiltitySwitch class changed to ProbabilitySwitch (typo fixed)
	 *
	 * @param rootElement
	 */
	private void updateProbabiltitySwitch(final Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();
		xpathBuffer.append("./swModel/tasks//*[@xsi:type=\"am:ProbabiltitySwitch\"]");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/isrs//*[@xsi:type=\"am:ProbabilitySwitch\"]");

		final List<Element> elements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (final Element probabilitySwitchElement : elements) {

			final Attribute attribute = probabilitySwitchElement.getAttribute("type", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			if (attribute != null) {
				attribute.setValue("am:ProbabilitySwitch");

			}
		}
	}

	/**
	 * Below migration is for the metamodel change : Bug 513969 (Inconsistent unit names for data size)
	 *
	 * In DataSizeUnit enum following values are changed: kibit, byte to : Kibit, B
	 *
	 * @param rootElement
	 */
	private void updateDataSize(final Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();
		xpathBuffer.append("./swModel/typeDefinitions[@xsi:type=\"am:BaseTypeDefinition\"]/size");
		xpathBuffer.append("|");
		xpathBuffer.append("./hwModel/memoryTypes/size");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/runnables/runnableItems//transmissionPolicy/chunkSize");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/runnables/size");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/labels/size");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/modeLabels/size");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/channels/size");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/customEntities/size");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/processPrototypes/size");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/isrs/size");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/tasks/size");

		final List<Element> elements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (final Element dataSizeElement : elements) {

			final Attribute attribute = dataSizeElement.getAttribute("unit");

			if (attribute != null) {
				final String value = attribute.getValue();
				if ("kibit".equals(value)) {
					attribute.setValue("Kibit");
				}
				else if ("byte".equals(value)) {
					attribute.setValue("B");
				}
			}
		}
	}

	/**
	 * Below migration is for the metamodel change : Bug 513976 (Simplify Modes Handling).
	 *
	 * Migration is required : As mode reference is not serialized in the model (as it is set as transient in the
	 * metamodel definition).
	 *
	 *
	 * @param rootElement
	 */
	private void updateModeLabel(final Element rootElement) {

		final StringBuilder labelXpathBuffer = new StringBuilder();

		labelXpathBuffer.append("./swModel/modeLabels");

		final List<Element> elements = HelperUtil.getXpathResult(
				rootElement,
				labelXpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (final Element modeLabel : elements) {

			final Attribute modeAttribute = modeLabel.getAttribute("mode");

			if (modeAttribute != null) {
				modeLabel.removeAttribute(modeAttribute);
			}
			else {
				final Element modeElement = modeLabel.getChild("mode");
				if (modeElement != null) {
					modeLabel.removeChild("mode");
				}
			}
		}
	}

}
