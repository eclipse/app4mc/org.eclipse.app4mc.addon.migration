/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters080.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * This class is responsible for converting the Stimuli Model elements from 0.7.2 to 0.8.0 version format of AMALTHEA
 * model
 *
 * @author mez2rng
 *
 */
@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.7.2",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.8.0"},
		service = IConverter.class)

public class StimuliConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {

		logger.info(
				"Migration from 0.7.2 to 0.8.0 : Executing Stimuli converter for model file : {0}", targetFile.getName());

		final Document root = filename2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();


		updateSignedTime(rootElement);

		updateSignedTimeObject(rootElement);
	}


	/**
	 * Below migration is for the metamodel change : SignedTimeObject class is removed -> instead of it TimeObject class
	 * is used
	 *
	 * For details w.r.t. metamodel change, see : Bug 514334
	 *
	 * @param rootElement
	 */
	private void updateSignedTimeObject(final Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();
		xpathBuffer.append(".//customProperties//@xsi:type[.=\"am:SignedTimeObject\"]");

		final List<Attribute> types = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Attribute.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));


		for (final Attribute signedTimeObjectAttribute : types) {
			signedTimeObjectAttribute.setValue("am:TimeObject");
		}
	}

	/**
	 * Below migration is for the metamodel change : SignedTime class is removed -> instead of it Time class is used
	 *
	 * For details w.r.t. metamodel change, see : Bug 514334
	 *
	 * @param rootElement
	 */
	private void updateSignedTime(final Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();
		xpathBuffer.append("./stimuliModel/stimuli/stimulusDeviation//*[@xsi:type=\"am:SignedTime\"]");

		final List<Element> elements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (final Element signedTimeElement : elements) {

			final Attribute attribute = signedTimeElement.getAttribute("type", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			if (attribute != null) {
				attribute.setValue("am:Time");

			}
		}
	}

}
