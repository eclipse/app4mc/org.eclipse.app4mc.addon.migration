/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters080.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * This class is responsible for converting the PropertyConstraints Model elements from 0.7.2 to 0.8.0 version format of
 * AMALTHEA model
 *
 * @author mez2rng
 *
 */
@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.7.2",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.8.0"},
		service = IConverter.class)

public class PropertyConstraintsConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {

		logger.info(
				"Migration from 0.7.2 to 0.8.0 : Executing PropertyConstraints converter for model file : {0}", targetFile.getName());


		final Document root = filename2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		removeHwConstraintElement(rootElement);
	}


	/**
	 * Below migration is for the metamodel change w.r.t. PropertyConstraints model (For further details, check : Bug
	 * 514779 )
	 *
	 * HwCoreConstraint and HwMemoryConstraint classes are removed and there is no direct equivalent of these classes as
	 * per 0.8.0.
	 *
	 * -- Based on this change the data associated to HwCoreConstraint and HwMemoryConstraint is removed from the model
	 * (as a reference to the users, removed data is stored as a custom property)
	 *
	 * @param rootElement
	 */
	private void removeHwConstraintElement(final Element rootElement) {

		final XMLOutputter xmlOutputter = new XMLOutputter();

		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./propertyConstraintsModel/allocationConstraints/hwConstraint");
		xpathBuffer.append("|");
		xpathBuffer.append("./propertyConstraintsModel/mappingConstraints/hwConstraint");


		final List<Element> elements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (final Element hwConstraintElement : elements) {

			final String xmlString = xmlOutputter.outputString(hwConstraintElement);

			final Element parentElement = hwConstraintElement.getParentElement();

			final Element customPropertyElement = new Element("customProperties");
			customPropertyElement.setAttribute("key",
					"hwConstraint (element removed during Migration of Model to 0.8.0 version)");

			final Element valueElement = new Element("value");
			valueElement.setAttribute("type", "am:StringObject", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));
			valueElement.setAttribute("value", xmlString);

			customPropertyElement.addContent(valueElement);

			parentElement.addContent(customPropertyElement);

			logger.warn("hwConstraint tag is removed from {0}. Below is the content of hwConstraint object: \n{1}", parentElement.getName(), xmlString);

			/*- removing HWConstraint model element -> as there is no equivalent of it in AMALTHEA 0.8.0 version */
			parentElement.removeContent(hwConstraintElement);
		}
	}

}
