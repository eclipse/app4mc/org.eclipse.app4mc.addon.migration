/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters200.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=1.2.0",
		service = {ICache.class, SchedulerCache.class} )

public class SchedulerCache implements ICache {

	private static final File FOLDER_SCOPE = new File("folder_scope");
	private static final String STANDARD_ALGORITHM_NAMES = "standard-algorithm-names";
	private static final String STANDARD_PARAMETER_NAMES = "standard-parameter-names";
	private static final String EXTENDED_PARAMETER_NAMES = "extended-parameter-names";
	private static final String USER_SPECIFIC_ALGORITHMS = "user-specific-algorithms";
	private static final String IS_FIRST_FILE = "is_first_file";

	@Reference
	SessionLogger logger;

	private final Map<File, Map<String, Object>> map = new HashMap<>();

	@Override
	public void buildCache(Map<File, Document> fileDocumentMapping) {

		if (logger != null) {
			logger.info("Build up SchedulerCache for 1.2.0");
		}

		fillCaches(fileDocumentMapping);
	}

	@Override
	public Map<File, Map<String, Object>> getCacheMap() {
		return this.map;
	}

	@Override
	public void clearCacheMap() {
		this.map.clear();
	}

	public Map<String, Object> getFolderScopeCache() {
		return map.computeIfAbsent(FOLDER_SCOPE, k -> new HashMap<>());
	}

	@SuppressWarnings("unchecked")
	public Set<String> getStandardAlgorithmCache() {
		return (Set<String>) getFolderScopeCache().computeIfAbsent(STANDARD_ALGORITHM_NAMES, k -> new HashSet<>());
	}

	@SuppressWarnings("unchecked")
	public Set<String> getStandardParameterCache() {
		return (Set<String>) getFolderScopeCache().computeIfAbsent(STANDARD_PARAMETER_NAMES, k -> new HashSet<>());
	}

	@SuppressWarnings("unchecked")
	public Set<String> getExtendedParameterCache() {
		return (Set<String>) getFolderScopeCache().computeIfAbsent(EXTENDED_PARAMETER_NAMES, k -> new HashSet<>());
	}

	@SuppressWarnings("unchecked")
	public Map<String, List<String>> getUserSpecificAlgorithmCache() {
		return (Map<String, List<String>>) getFolderScopeCache().computeIfAbsent(USER_SPECIFIC_ALGORITHMS, k -> new HashMap<>());
	}

	public boolean isFirstFile() {
		return (boolean) getFolderScopeCache().computeIfAbsent(IS_FIRST_FILE, k -> true);
	}

	public void setFirstFile(boolean flag) {
		getFolderScopeCache().put(IS_FIRST_FILE, flag);
	}

	private void fillCaches(Map<File, Document> fileDocumentMapping) {
		// common namespaces
		final Namespace am = AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_120, "am");
		final Namespace xsi = AmaltheaNamespaceRegistry.getGenericNamespace("xsi");

		final Set<String> standardAlgorithmCache = getStandardAlgorithmCache();
		final Set<String> standardParameterCache = getStandardParameterCache();
		final Set<String> extendedParameterCache = getExtendedParameterCache();
		final Map<String, List<String>> userSpecificAlgorithmCache = getUserSpecificAlgorithmCache();

		// scan all documents
		for (Entry<File, Document> entry : fileDocumentMapping.entrySet()) {
			Document doc = entry.getValue();

			if (doc == null) {
				continue;
			}

			Element rootElement = doc.getRootElement();

			// *** Collect scheduling algorithms

			List<Element> algorithmElements = HelperUtil.getXpathResult(
					rootElement,
					"./osModel/operatingSystems/taskSchedulers/schedulingAlgorithm" + "|" +
					"./osModel/operatingSystems/interruptControllers/schedulingAlgorithm",
					Element.class, am, xsi);

			int counter = 0; // for user specific schedulers

			for (Element elem : algorithmElements) {
				String type = elem.getAttributeValue("type", xsi);

				if ("am:UserSpecificSchedulingAlgorithm".equals(type)) {
					// store: unique name -> parameters
					counter++;
					String algorithmName = type.substring(3) + String.valueOf(counter);
					List<String> algorithmParameters = new ArrayList<>();

					// collect parameter extensions
					for (Element param : elem.getChildren("parameterExtensions")) {
						String parameterName = param.getAttributeValue("key");
						algorithmParameters.add(parameterName);
						// store extended parameter name
						extendedParameterCache.add(parameterName);
					}
					// store user specific algorithm
					userSpecificAlgorithmCache.put(algorithmName, algorithmParameters);
				} else {
					// store default algorithm name
					standardAlgorithmCache.add(type.substring(3));	// add type without prefix "am:"
				}
			}

			// *** SchedulingParameters: Add standard parameters if present

			List<Element> parameterElements = HelperUtil.getXpathResult(
					rootElement,
					"./osModel/operatingSystems/taskSchedulers/parentAssociation/schedulingParameters" + "|" +
							"./mappingModel/taskAllocation/schedulingParameters",
							Element.class, am, xsi);

			for (Element elem : parameterElements) {
				String priorityValue = elem.getAttributeValue("priority");
				if (priorityValue != null && (! "0".equals(priorityValue))) {
					standardParameterCache.add("priority");
				}
				if (elem.getChild("minBudget") != null) {
					standardParameterCache.add("minBudget");
				}
				if (elem.getChild("maxBudget") != null) {
					standardParameterCache.add("maxBudget");
				}
				if (elem.getChild("replenishment") != null) {
					standardParameterCache.add("replenishment");
				}
			}

			// *** ParameterExtensions: Collect extended parameters

			List<Element> extendedParameterElements = HelperUtil.getXpathResult(
					rootElement,
					"./osModel/operatingSystems/taskSchedulers/parentAssociation/parameterExtensions" + "|" +
					"./mappingModel/taskAllocation/parameterExtensions",
					Element.class, am, xsi);

			for (Element elem : extendedParameterElements) {
				String name = elem.getAttributeValue("key");
				extendedParameterCache.add(name);
			}
		}
	}

}
