/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters200.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=1.2.0",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=2.0.0"
		},
		service = IConverter.class)

public class ConditionConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> fileDocumentMapping, List<ICache> caches) {
		logger.info("Migration from {0} to {1} : Executing Condition converter for model file : {2}",
				getInputModelVersion(), getOutputModelVersion(), targetFile.getName());

		basicConvert(targetFile, fileDocumentMapping);
	}

	private void basicConvert(File file, Map<File, Document> map) {
		// get the components with interfaces
		final Document document = map.get(file);
		if (document == null) {
			return;
		}

		final Element rootElement = document.getRootElement();

		updateConditions(rootElement);
		updateWhileLoopConditions(rootElement);
	}

	private void updateConditions(Element rootElement) {
		final Namespace am = AmaltheaNamespaceRegistry.getNamespace(getInputModelVersion(), "am");
		final Namespace xsi = AmaltheaNamespaceRegistry.getGenericNamespace("xsi");

		final StringBuilder xpathBuilder = new StringBuilder();
		 xpathBuilder.append("./swModel/runnables/executionCondition/entries");
		 xpathBuilder.append("|");
		 xpathBuilder.append("./stimuliModel/stimuli/executionCondition/entries");

		final List<Element> entries = HelperUtil.getXpathResult(rootElement, xpathBuilder.toString(), Element.class, am, xsi);

		if (entries == null) {
			return;
		}

		// Change type ModeConditionConjunction to ConditionConjunction

		for (Element entry : entries) {
			Attribute typeAttr = entry.getAttribute("type", xsi);
			if ("am:ModeConditionConjunction".equals(typeAttr.getValue())) {
				typeAttr.setValue("am:ConditionConjunction");
			}
		}
	}

	private void updateWhileLoopConditions(Element rootElement) {
		final Namespace am = AmaltheaNamespaceRegistry.getNamespace(getInputModelVersion(), "am");
		final Namespace xsi = AmaltheaNamespaceRegistry.getGenericNamespace("xsi");

		final StringBuilder xpathBuilder = new StringBuilder();
		xpathBuilder.append("./swModel/tasks/activityGraph/items//condition/entries");
		xpathBuilder.append("|");
		xpathBuilder.append("./swModel/isrs/activityGraph/items//condition/entries");
		xpathBuilder.append("|");
		xpathBuilder.append("./swModel/runnables/activityGraph/items//condition/entries");
		final List<Element> entries = HelperUtil.getXpathResult(rootElement, xpathBuilder.toString(), Element.class, am, xsi);

		if (entries == null) {
			return;
		}

		// Change type ModeConditionConjunction to ConditionConjunction

		for (Element entry : entries) {
			Attribute typeAttr = entry.getAttribute("type", xsi);
			if ("am:ModeConditionConjunction".equals(typeAttr.getValue())) {
				Element condition = entry.getParentElement();
				Element item = condition.getParentElement();
				if ("am:WhileLoop".equals(item.getAttributeValue("type", xsi))) {
					typeAttr.setValue("am:ConditionConjunction");
				}
			}
		}
	}

}
