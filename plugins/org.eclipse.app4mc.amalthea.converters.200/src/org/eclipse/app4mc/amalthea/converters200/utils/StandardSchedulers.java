/**
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch - initial API and implementation
 */

package org.eclipse.app4mc.amalthea.converters200.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StandardSchedulers {

	private static final String ALGORITHM_PARAMETERS = "\nAlgorithm parameters:\n";
	private static final String PROCESS_PARAMETERS = "\nProcess parameters:\n";
	private static final String OPTIONS = "\nOptions:\n";
	private static final String REFERENCE = "\nReference:\n";
	private static final String NO_ITEMS = " -\n";
	private static final String PASSES_PARAMETERS_UPWARDS = " - passes parameters upwards\n";
	private static final String HAS_EXACTLY_ONE_CHILD = " - has exactly one child\n";
	private static final String REQUIRES_PARENT_SCHEDULER = " - requires parent scheduler\n";

	private static final String LIU_LAYLAND_1973 = // http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.36.8216
			" - Liu, Chung Laung, and James W. Layland (1973).\n" +
			"   \"Scheduling Algorithms for Multiprogramming in a Hard-Real-Time Environment\"\n" +
			"   Journal of the ACM (JACM) 20.1 (1973): 46-61.\n";
	private static final String SPRUNT_SHA_LEHOCZKY_1989 = // http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.916.4332
			" - Sprunt, Brinkley, Lui Sha, and John Lehoczky (1989).\n" +
			"   \"Aperiodic Task Scheduling for Hard Real-Time Systems\"\n" +
			"   Real-Time Systems 1(1):27-60, DOI:10.1007/BF02341920\n";
	private static final String AUDSLEY_1990 = // http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.53.8928
			" - Audsley, Neil (1990).\n" +
			"   \"Deadline Monotonic Scheduling\"\n";
	private static final String STROSNIDER_LEHOCZKY_SHA_1995 =
			" - Strosnider, Jay K., John P. Lehoczky, and Lui Sha (1995).\n" +
			"   \"The Deferrable Server Algorithm for Enhanced Aperiodic Responsiveness in Hard Real-Time Environments\"\n"+
			"   IEEE Transactions on Computers 44.1 (1995): 73-91.\n";
	private static final String ABENI_BUTTAZZO_1998 = // http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.638.3683
			" - Abeni, Luca, and Giorgio Buttazzo (1998).\n" +
			"   \"Integrating Multimedia Applications in Hard Real-Time Systems\"\n" +
			"   Proceedings 19th IEEE Real-Time Systems Symposium (Cat. No. 98CB36279). IEEE, 1998.\n";
	private static final String CACCAMO_BUTTAZZO_SHA_2000 =
			" - M. Caccamo, G. Buttazzo and Lui Sha (2000).\n" +
			"   \"Capacity sharing for overrun control\"\n" +
			"   Proceedings 21st IEEE Real-Time Systems Symposium, 2000, pp. 295-304, doi: 10.1109/REAL.2000.896018.\n";
	private static final String ANDERSON_SRINIVASAN_2001 =
			" - J. H. Anderson and A. Srinivasan.\n" +
			"   \"Mixed Pfair/ERfair scheduling of asynchronous periodic tasks\"\n" +
			"   Proceedings 13th Euromicro Conference on Real-Time Systems, 2001, pp. 76-85, doi: 10.1109/EMRTS.2001.934004.\n";
	private static final String BUTTAZZO_2004 =
			" - Buttazzo, Giorgio (2004).\n" +
			"   \"Real-Time Computing Systems - Predictable Scheduling Algorithms and Applications\"\n" +
			"   Springer Publications, Chapter 5, DOI:10.1007/0-387-27578-9\n";
	private static final String OSEK_2005 =
			" - OSEK/VDX - Operating System Specification 2.2.3 (2005)\n";
	private static final String DEUBZER_MARGULL_MOTTOK =
			" - Deubzer, Michael & Margull, Ulrich & Mottok, Juergen & Niemetz, M. & Wirrer, G. (2010).\n" +
			"   \"Efficient scheduling of reliable automotive multi-core systems with pd2 by weakening erfair tasksystem requirements\"\n" +
			"   Lecture Notes in Informatics (LNI), Proceedings - Series of the Gesellschaft fur Informatik (GI). 53-68.\n";
	private static final String ZOUAOUI_BOUSSAID_ABDELLATIF_2019 =
			" - Zouaoui, Sonia & Boussaid, Lotfi & Abdellatif, Mtibaa (2019).\n" +
			"   \"Priority based round robin (PBRR) CPU scheduling algorithm\"\n" +
			"   International Journal of Electrical and Computer Engineering (IJECE),\n" +
			"   9. 190. 10.11591/ijece.v9i1.pp190-202.\n";

	// Suppress default constructor
	private StandardSchedulers() {
		throw new IllegalStateException("Utility class");
	}

	public enum Algorithm {
		// *** ADD PREDEFINED SCHEDULER DEFINITIONS HERE ***
		GROUPING_SERVER( // only for migration
				"GroupingServer",
				"This is not a scheduler algorithm. Schedulers using this definition\n"
				+ "act as a logical grouping of tasks/child-schedulers, e.g. a partition\n"
				+ "for some tasks for budget accounting reasons.\n\n"
				+ "This scheduler does not take any scheduling decisions,\n"
				+ "and a parent scheduler is mandatory.\n"
				+ ALGORITHM_PARAMETERS
				+ " - capacity [1] Time\n"
				+ "      The fixed budget that can be used by processes.\n"
				+ "      It will be replenished periodically.\n"
				+ " - period [1] Time\n"
				+ "      Amount of time after which the capacity will be replenished.\n"
				+ PROCESS_PARAMETERS
				+ NO_ITEMS
				+ OPTIONS
				+ PASSES_PARAMETERS_UPWARDS
				+ REQUIRES_PARENT_SCHEDULER,
				new Parameter[] { Parameter.CAPACITY, Parameter.PERIOD },
				new Parameter[] {},
				false, true, true),
		PRIORITY_BASED(
				"PriorityBased",
				"???\n"
				+ ALGORITHM_PARAMETERS
				+ NO_ITEMS
				+ PROCESS_PARAMETERS
				+ " - priority [1] Integer\n"
				+ "      The priority of the process (a higher value means a higher priority).\n"
				+ OPTIONS
				+ NO_ITEMS,
				new Parameter[] {},
				new Parameter[] { Parameter.PRIORITY },
				false, false, false),

		OSEK(
				"OSEK",
				"OSEK compliant Scheduling. A fixed priority preemptive scheduling algorithm\n"
				+ "with task groups. Tasks belonging to the same task group are scheduled\n"
				+ "cooperatively (they do not preempt each other), preemptive otherwise.\n"
				+ "Tasks with the same priority also behave cooperatively.\n"
				+ ALGORITHM_PARAMETERS
				+ NO_ITEMS
				+ PROCESS_PARAMETERS
				+ " - priority [1] Integer\n"
				+ "      The priority of the process (a higher value means a higher priority).\n"
				+ " - taskGroup [1] Integer\n"
				+ "      The OSEK task group number (if for two processes the number is equal,\n"
				+ "      that means they are in the same task group).\n"
				+ OPTIONS
				+ NO_ITEMS
				+ REFERENCE
				+ OSEK_2005,
				new Parameter[] {},
				new Parameter[] { Parameter.PRIORITY, Parameter.TASK_GROUP },
				false, false, false),
		FIXED_PRIORITY_PREEMPTIVE(
				"FixedPriorityPreemptive",
				"Fixed Priority Preemptive Scheduling (e.g. AUTOSAR),\n"
				+ "same as OSEK but without task groups.\n"
				+ ALGORITHM_PARAMETERS
				+ NO_ITEMS
				+ PROCESS_PARAMETERS
				+ " - priority [1] Integer\n"
				+ "      The priority of the process (a higher value means a higher priority).\n"
				+ OPTIONS
				+ NO_ITEMS
				+ REFERENCE
				+ LIU_LAYLAND_1973,
				new Parameter[] {},
				new Parameter[] { Parameter.PRIORITY },
				false, false, false),
		FIXED_PRIORITY_PREEMPTIVE_WITH_BUDGET_ENFORCEMENT( // only for migration
				"FixedPriorityPreemptiveWithBudgetEnforcement",
				"Works like the Fixed Priority Preemptive Scheduling. But it is possible\n"
				+ "to put budget boundaries on the execution. Prevents low priority tasks \n"
				+ "from starving if a higher priority task is trying to constantly occupy the\n"
				+ "CPU (safety insurance, bounding the execution time of sporadic loads).\n"
				+ ALGORITHM_PARAMETERS
				+ NO_ITEMS
				+ PROCESS_PARAMETERS
				+ " - priority [1] Integer\n"
				+ "      The priority of the process (a higher value means a higher priority).\n"
				+ " - minBudget [1] Time\n"
				+ "      The guaranteed amount of budget available to the process within the replenishment period.\n"
				+ " - maxBudget [1] Time\n"
				+ "      The upper bound of the budget available to the process within the replenishment period.\n"
				+ "      Sets a limit to the usage of a process even if the CPU would be idle\n"
				+ "      (important for algorithms with payback mechanisms).\n"
				+ " - replenishment [1] Time\n"
				+ "      The periodic time interval after which the budget is set to the configured value.\n"
				+ OPTIONS
				+ NO_ITEMS,
				new Parameter[] {},
				new Parameter[] { Parameter.PRIORITY, Parameter.MIN_BUDGET, Parameter.MAX_BUDGET, Parameter.REPLENISHMENT },
				false, false, false),
		DEADLINE_MONOTONIC( // only for migration
				"DeadlineMonotonic",
				"This is not a scheduling algorithm, it only describes how to derive priorities for a\n"
				+ "fixed priority scheduler: Task with the shortest deadline gets the highest priority.\n"
				+ ALGORITHM_PARAMETERS
				+ NO_ITEMS
				+ PROCESS_PARAMETERS
				+ " - deadline [1] Time\n"
				+ "      The time after each activation at which the process must finish.\n"
				+ OPTIONS
				+ NO_ITEMS
				+ REFERENCE
				+ AUDSLEY_1990,
				new Parameter[] {},
				new Parameter[] { Parameter.DEADLINE },
				false, false, false),
		RATE_MONOTONIC( // only for migration
				"RateMonotonic",
				"This is not a scheduling algorithm, it only describes how to derive priorities for a\n"
				+ "fixed priority scheduler: Task with the shortest period gets the highest priority.\n"
				+ ALGORITHM_PARAMETERS
				+ NO_ITEMS
				+ PROCESS_PARAMETERS
				+ " - period [1] Integer\n"
				+ "      The time span after the previous activation at which\n"
				+ "      the next instance of the process shall be activated.\n"
				+ OPTIONS
				+ NO_ITEMS
				+ REFERENCE
				+ LIU_LAYLAND_1973,
				new Parameter[] {},
				new Parameter[] { Parameter.PERIOD },
				false, false, false),

		EARLIEST_DEADLINE_FIRST(
				"EarliestDeadlineFirst",
				"Earliest Deadline First (EDF): The task with the closest deadline in relation\n"
				+ "to the current point in time will be scheduled next.\n"
				+ ALGORITHM_PARAMETERS
				+ NO_ITEMS
				+ PROCESS_PARAMETERS
				+ " - deadline [1] Time\n"
				+ "      The time after each activation at which the process must finish.\n"
				+ OPTIONS
				+ NO_ITEMS
				+ REFERENCE
				+ LIU_LAYLAND_1973,
				new Parameter[] {},
				new Parameter[] { Parameter.DEADLINE },
				false, false, false),
		LEAST_LOCAL_REMAINING_EXECUTION_TIME_FIRST( // only for migration
				"LeastLocalRemainingExecutionTimeFirst",
				"Least Local Remaining Execution-time First (LLREF): Task with the\n"
				+ "smallest local remaining execution time will be scheduled next.\n"
				+ ALGORITHM_PARAMETERS
				+ NO_ITEMS
				+ PROCESS_PARAMETERS
				+ " - executionTime [1] Time\n"
				+ "      The time which the process will use until it is finished\n"
				+ "      (usually the worst case execution time is used here in order\n"
				+ "      to guarantee that the process can finish).\n"
				+ OPTIONS
				+ NO_ITEMS,
				new Parameter[] {},
				new Parameter[] { Parameter.EXECUTION_TIME },
				false, false, false),
		PRIORITY_BASED_ROUND_ROBIN(
				"PriorityBasedRoundRobin",
				"Round Robin scheduling algorithm assigns equally sized time slices\n"
				+ "to each process that it schedules. The priority describes the order\n"
				+ "in which the processes will be executed. If two processes have the\n"
				+ "same priority, the order of these two is random (non-deterministic).\n"
				+ ALGORITHM_PARAMETERS
				+ " - timeSliceLength [1] Time\n"
				+ "      Length of each time slice.\n"
				+ PROCESS_PARAMETERS
				+ " - priority [1] Integer\n"
				+ "      The priority of the process (a higher value means a higher priority).\n"
				+ OPTIONS
				+ NO_ITEMS
				+ REFERENCE
				+ ZOUAOUI_BOUSSAID_ABDELLATIF_2019,
				new Parameter[] { Parameter.TIME_SLICE_LENGTH },
				new Parameter[] { Parameter.PRIORITY },
				false, false, false),

		P_FAIR_PD2(
				"PFairPD2",
				"Proportionate Fair PD2 Scheduling (Pfair-PD2).\n"
				+ ALGORITHM_PARAMETERS
				+ " - quantSize [0..1] Time = 1ns\n"
				+ "      Length of the minimum schedulable time slot used in Pfair scheduling.\n"
				+ "      It is assumed that execution times are an integer multiple of this\n"
				+ "      time slot length.\n"
				+ PROCESS_PARAMETERS
				+ NO_ITEMS
				+ OPTIONS
				+ NO_ITEMS
				+ REFERENCE
				+ ANDERSON_SRINIVASAN_2001,
				new Parameter[] { Parameter.QUANT_SIZE },
				new Parameter[] {},
				false, false, false),
		PARTLY_P_FAIR_PD2(
				"PartlyPFairPD2",
				"Partly Proportionate Fair PD2 Scheduling (PPfair-PD2).\n"
				+ ALGORITHM_PARAMETERS
				+ " - quantSize [0..1] Time = 1ns\n"
				+ "      Length of the minimum schedulable time slot used in Pfair scheduling.\n"
				+ "      It is assumed that execution times are an integer multiple of this\n"
				+ "      time slot length.\n"
				+ PROCESS_PARAMETERS
				+ NO_ITEMS
				+ OPTIONS
				+ NO_ITEMS,
				new Parameter[] { Parameter.QUANT_SIZE },
				new Parameter[] {},
				false, false, false),
		EARLY_RELEASE_FAIR_PD2(
				"EarlyReleaseFairPD2",
				"Early Release Fair PD2 Scheduling (ERfair-PD2).\n"
				+ ALGORITHM_PARAMETERS
				+ " - quantSize [0..1] Time = 1ns\n"
				+ "      Length of the minimum schedulable time slot used in Pfair scheduling.\n"
				+ "      It is assumed that execution times are an integer multiple of this\n"
				+ "      time slot length.\n"
				+ PROCESS_PARAMETERS
				+ NO_ITEMS
				+ OPTIONS
				+ NO_ITEMS
				+ REFERENCE
				+ DEUBZER_MARGULL_MOTTOK,
				new Parameter[] { Parameter.QUANT_SIZE },
				new Parameter[] {},
				false, false, false),
		PARTLY_EARLY_RELEASE_FAIR_PD2(
				"PartlyEarlyReleaseFairPD2",
				"Partly Early Release Fair PD2 Scheduling (P-ERfair-PD2).\n"
				+ ALGORITHM_PARAMETERS
				+ " - quantSize [0..1] Time = 1ns\n"
				+ "      Length of the minimum schedulable time slot used in Pfair scheduling.\n"
				+ "      It is assumed that execution times are an integer multiple of this\n"
				+ "      time slot length.\n"
				+ PROCESS_PARAMETERS
				+ NO_ITEMS
				+ OPTIONS
				+ NO_ITEMS,
				new Parameter[] { Parameter.QUANT_SIZE },
				new Parameter[] {},
				false, false, false),

		DEFERRABLE_SERVER(
				"DeferrableServer",
				"Deferrable Server (DS): provides a fixed budget,\n"
				+ "in which the budget replenishment is done periodically.\n"
				+ ALGORITHM_PARAMETERS
				+ " - capacity [1] Time\n"
				+ "      The fixed budget that can be used by processes.\n"
				+ "      It will be replenished periodically.\n"
				+ " - period [1] Time\n"
				+ "      Amount of time after which the capacity will be replenished.\n"
				+ PROCESS_PARAMETERS
				+ NO_ITEMS
				+ OPTIONS
				+ HAS_EXACTLY_ONE_CHILD
				+ REQUIRES_PARENT_SCHEDULER
				+ REFERENCE
				+ STROSNIDER_LEHOCZKY_SHA_1995,
				new Parameter[] { Parameter.CAPACITY, Parameter.PERIOD },
				new Parameter[] {},
				true, false, true),
		POLLING_SERVER(
				"PollingServer",
				"Polling Server (PS): provides a fixed budget periodically that is only\n"
				+ "available at pre-defined times. If the process is not using the budget\n"
				+ "at that point in time the budget is lost.\n"
				+ ALGORITHM_PARAMETERS
				+ " - capacity [1] Time\n"
				+ "      The fixed budget that can be used by processes (usually directly\n"
				+ "      after it has been replenished). The capacity will be consumed even\n"
				+ "      if there is no process using it. It will be replenished periodically.\n"
				+ " - period [1] Time\n"
				+ "      Amount of time after which the capacity will be replenished.\n"
				+ PROCESS_PARAMETERS
				+ NO_ITEMS
				+ OPTIONS
				+ HAS_EXACTLY_ONE_CHILD
				+ REQUIRES_PARENT_SCHEDULER
				+ REFERENCE
				+ BUTTAZZO_2004,
				new Parameter[] { Parameter.CAPACITY, Parameter.PERIOD },
				new Parameter[] {},
				true, false, true),
		SPORADIC_SERVER(
				"SporadicServer",
				"Sporadic Server (SS): provides a fixed budget, in which the budget replenishment\n"
				+ "is performed with a pre-defined replenishment delay after it was consumed.\n"
				+ ALGORITHM_PARAMETERS
				+ " - capacity [1] Time\n"
				+ "      The fixed budget that can be used by processes. It will be replenished after\n"
				+ "      the specified amount of time has passed since it has last been consumed.\n"
				+ " - replenishmentDelay [1] Time\n"
				+ "      Amount of time after which the capacity will be replenished\n"
				+ "      after it has last been consumed.\n"
				+ PROCESS_PARAMETERS
				+ NO_ITEMS
				+ OPTIONS
				+ HAS_EXACTLY_ONE_CHILD
				+ REQUIRES_PARENT_SCHEDULER
				+ REFERENCE
				+ SPRUNT_SHA_LEHOCZKY_1989,
				new Parameter[] { Parameter.CAPACITY, Parameter.REPLENISHMENT_DELAY },
				new Parameter[] {},
				true, false, true),
		CONSTANT_BANDWIDTH_SERVER(
				"ConstantBandwidthServer",
				"Constant Bandwidth Server (CBS): provides a fixed utilization for\n"
				+ "executing jobs, in which the deadline for execution is independent\n"
				+ "on the execution time of jobs.\n"
				+ ALGORITHM_PARAMETERS
				+ " - capacity [1] Time\n"
				+ "      The fixed budget that can be used by processes.\n"
				+ "      It will be replenished periodically.\n"
				+ " - period [1] Time\n"
				+ "      Amount of time after which the capacity will be replenished.\n"
				+ PROCESS_PARAMETERS
				+ NO_ITEMS
				+ OPTIONS
				+ HAS_EXACTLY_ONE_CHILD
				+ REQUIRES_PARENT_SCHEDULER
				+ REFERENCE
				+ ABENI_BUTTAZZO_1998,
				new Parameter[] { Parameter.CAPACITY, Parameter.PERIOD },
				new Parameter[] {},
				true, false, true),
		CONSTANT_BANDWIDTH_SERVER_WITH_CAPACITY_SHARING(
				"ConstantBandwidthServerWithCapacitySharing",
				"Constant Bandwidth Server (CBS) with capacity sharing (CASH).\n"
				+ "Consumes residual slack from other servers (work conserving).\n"
				+ ALGORITHM_PARAMETERS
				+ " - capacity [1] Time\n"
				+ "      The fixed budget that can be used by processes.\n"
				+ "      It will be replenished periodically.\n"
				+ " - period [1] Time\n"
				+ "      Amount of time after which the capacity will be replenished.\n"
				+ PROCESS_PARAMETERS
				+ NO_ITEMS
				+ OPTIONS
				+ HAS_EXACTLY_ONE_CHILD
				+ REQUIRES_PARENT_SCHEDULER
				+ REFERENCE
				+ CACCAMO_BUTTAZZO_SHA_2000,
				new Parameter[] { Parameter.CAPACITY, Parameter.PERIOD },
				new Parameter[] {},
				true, false, true);

		private final String algorithmName;
		private final String description;
		private final List<Parameter> algorithmParameters;
		private final List<Parameter> processParameters;
		private final boolean hasExactlyOneChild;
		private final boolean passesParametersUpwards;
		private final boolean requiresParentScheduler;

		// private enum constructor
		private Algorithm(String algorithmName, String description,
				Parameter[] algorithmParameters, Parameter[] processParameters,
				boolean hasExactlyOneChild, boolean passesParametersUpwards, boolean requiresParentScheduler) {
			this.algorithmName = algorithmName;
			this.description = description;
			this.algorithmParameters = Arrays.asList(algorithmParameters);
			this.processParameters = Arrays.asList(processParameters);
			this.hasExactlyOneChild = hasExactlyOneChild;
			this.passesParametersUpwards = passesParametersUpwards;
			this.requiresParentScheduler = requiresParentScheduler;
		}

		public String getAlgorithmName() {
			return algorithmName;
		}

		public String getDescription() {
			return description;
		}

		public List<Parameter> getAlgorithmParameters() {
			return algorithmParameters;
		}

		public List<String> getAlgorithmParameterNames() {
			return algorithmParameters.stream().map(Parameter::getParameterName).collect(Collectors.toList());
		}

		public List<Parameter> getProcessParameters() {
			return processParameters;
		}

		public List<String> getProcessParameterNames() {
			return processParameters.stream().map(Parameter::getParameterName).collect(Collectors.toList());
		}

		public boolean hasExactlyOneChild() {
			return hasExactlyOneChild;
		}

		public boolean passesParametersUpwards() {
			return passesParametersUpwards;
		}

		public boolean requiresParentScheduler() {
			return requiresParentScheduler;
		}
	}

	public enum Parameter {
		// *** ADD PREDEFINED PARAMETER DEFINITIONS HERE ***
		PRIORITY("priority", Type.INTEGER, false, true),
		TASK_GROUP("taskGroup", Type.INTEGER, false, true),
		MIN_BUDGET("minBudget", Type.TIME, false, true),
		MAX_BUDGET("maxBudget", Type.TIME, false, true),
		REPLENISHMENT("replenishment", Type.TIME, false, true),
		DEADLINE("deadline", Type.TIME, false, true),
		PERIOD("period", Type.TIME, false, true),
		EXECUTION_TIME("executionTime", Type.TIME, false, true),
		QUANT_SIZE("quantSize", Type.TIME, false, false, "1 ns"),
		TIME_SLICE_LENGTH("timeSliceLength", Type.TIME, false, true),
		CAPACITY("capacity", Type.TIME, false, true),
		REPLENISHMENT_DELAY("replenishmentDelay", Type.TIME, false, true);

		private final String parameterName;
		private final Type type;
		private final boolean many;
		private final boolean mandatory;
		private final String defaultValue;

		// private enum constructors
		private Parameter(String parameterName, Type type, boolean many, boolean mandatory) {
			this(parameterName, type, many, mandatory, null);
		}

		private Parameter(String parameterName, Type type, boolean many, boolean mandatory, String defaultValue) {
			this.parameterName = parameterName;
			this.type = type;
			this.many = many;
			this.mandatory = mandatory;
			this.defaultValue = defaultValue;
		}

		public String getParameterName() {
			return parameterName;
		}

		public Type getType() {
			return type;
		}

		public boolean isMany() {
			return many;
		}

		public boolean isMandatory() {
			return mandatory;
		}

		public String getDefaultValue() {
			return defaultValue;
		}
	}

	public enum Type {
		INTEGER("Integer"),
		FLOAT("Float"),
		BOOL("Bool"),
		TIME("Time"),
		STRING("String");

		private final String typeName;

		// private enum constructor
		private Type(String typeName) {
			this.typeName = typeName;
		}

		public String getTypeName() {
			return typeName;
		}
	}

	private static final Map<String, Parameter> PARAMETERS;
	private static final Map<String, Algorithm> ALGORITHMS;

	static {
		PARAMETERS = Arrays.stream(Parameter.values())
				.collect(Collectors.toMap(Parameter::getParameterName, Function.identity()));
		ALGORITHMS = Arrays.stream(Algorithm.values())
				.collect(Collectors.toMap(Algorithm::getAlgorithmName, Function.identity()));
	}

	public static Algorithm getAlgorithm(String algorithmName) {
		return ALGORITHMS.get(algorithmName);
	}

	public static Parameter getParameter(String parameterName) {
		return PARAMETERS.get(parameterName);
	}

	public static List<Parameter> getAllParametersOfAlgorithm(String algorithmName) {
		List<Parameter> result = new ArrayList<>();

		Algorithm algo = ALGORITHMS.get(algorithmName);
		if (algo != null) {
			result.addAll(algo.getAlgorithmParameters());
			result.addAll(algo.getProcessParameters());
		}

		return result;
	}

	public static List<Parameter> getAlgorithmParametersOfAlgorithm(String algorithmName) {
		List<Parameter> result = new ArrayList<>();

		Algorithm algo = ALGORITHMS.get(algorithmName);
		if (algo != null) {
			result.addAll(algo.getAlgorithmParameters());
		}

		return result;
	}

	public static List<Parameter> getProcessParametersOfAlgorithm(String algorithmName) {
		List<Parameter> result = new ArrayList<>();

		Algorithm algo = ALGORITHMS.get(algorithmName);
		if (algo != null) {
			result.addAll(algo.getProcessParameters());
		}

		return result;
	}

}
