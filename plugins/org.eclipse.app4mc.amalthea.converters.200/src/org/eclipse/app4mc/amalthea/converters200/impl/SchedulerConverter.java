/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters200.impl;

import java.io.File;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.amalthea.converters200.utils.SchedulerCache;
import org.eclipse.app4mc.amalthea.converters200.utils.SchedulerConverterUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=1.2.0",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=2.0.0"
		},
		service = IConverter.class)

public class SchedulerConverter extends AbstractConverter {
	private static final String KEY = "key";
	private static final String VALUE = "value";
	private static final String UNIT = "unit";
	private static final String EXTENDED = "extended: ";
	private static final String SCHEDULING_ALGORITHM = "schedulingAlgorithm";
	private static final String SCHEDULING_PARAMETERS = "schedulingParameters";
	private static final String PARAMETER_EXTENSIONS = "parameterExtensions";
	private static final String PRIORITY = "priority";
	private static final String MIN_BUDGET = "minBudget";
	private static final String MAX_BUDGET = "maxBudget";
	private static final String REPLENISHMENT2 = "replenishment";

	@Reference
	SessionLogger logger;

	private SchedulerCache myCache;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> fileDocumentMapping, List<ICache> caches) {
		logger.info("Migration from {0} to {1} : Executing Scheduler converter for model file : {2}",
				getInputModelVersion(), getOutputModelVersion(), targetFile.getName());

		myCache = getFilteredCaches(caches, SchedulerCache.class).stream().findFirst().orElse(null);

		basicConvert(targetFile, fileDocumentMapping);
	}

	private void basicConvert(File file, Map<File, Document> map) {
		// get current document and root element
		final Document document = map.get(file);
		if (document == null) {
			return;
		}

		final Element rootElement = document.getRootElement();

		addSchedulerDefinitions(rootElement);
		convertSchedulingAlgorithms(rootElement);
		convertParameterValues(rootElement);

		// after migration of first file -> set flag
		myCache.setFirstFile(false);
	}

	/**
	 * Adds all (cached) definitions of schedulers and parameters to the first file
	 */
	private void addSchedulerDefinitions(Element rootElement) {
		if (myCache == null || rootElement == null || ! myCache.isFirstFile()) return;

		Set<String> standardAlgorithmNames = myCache.getStandardAlgorithmCache();
		Set<Entry<String, List<String>>> extendedAlgorithmEntries = myCache.getUserSpecificAlgorithmCache().entrySet();
		Set<String> standardParameterNames = myCache.getStandardParameterCache();
		Set<String> extendedParameterNames = myCache.getExtendedParameterCache();

		if (standardAlgorithmNames.isEmpty()
			&& extendedAlgorithmEntries.isEmpty()
			&& standardParameterNames.isEmpty()
			&& extendedParameterNames.isEmpty()) return;

		// *** get or create osModel (for first file only)

		Element osModel = rootElement.getChild("osModel");
		if (osModel == null) {
			osModel =  new Element("osModel");
			rootElement.addContent(osModel);
		}

		// *** add scheduler definitions (for first file only)

		for (String name : sortedList(standardAlgorithmNames)) {
			String newName = SchedulerConverterUtil.getNewSchedulerName(name);

			// add standard scheduler definition
			SchedulerConverterUtil.addSchedulerDefinition(osModel, newName);

			// add parameter names of scheduler for later processing
			standardParameterNames.addAll(SchedulerConverterUtil.getStandardParameterNames(newName));
		}

		for (Entry<String, List<String>> entry : sortedEntryList(extendedAlgorithmEntries)) {
			String name = entry.getKey();
			List<String> params = entry.getValue().stream()
					.map(s -> EXTENDED + s)
					.collect(Collectors.toList());
			// add custom scheduler definition with algorithm parameters
			SchedulerConverterUtil.addSchedulerDefinition(osModel, name, null, params, null, null);
		}

		// *** add scheduling parameter definitions (for first file only)

		for (String name : sortedList(standardParameterNames)) {
			// add standard parameter definition
			SchedulerConverterUtil.addParameterDefinition(osModel, name);
		}
		for (String name : sortedList(extendedParameterNames)) {
			// add extended parameter definition
			SchedulerConverterUtil.addParameterDefinition(osModel, EXTENDED + name);
		}
	}

	/**
	 * For all schedulers:
	 * <ul>
	 *   <li>remove child element "schedulingAlgorithm"</li>
	 *   <li>add reference "definition" to scheduler definition
	 * </ul>
	 */
	private void convertSchedulingAlgorithms(Element rootElement) {
		if (myCache == null || rootElement == null) return;

		// common namespaces
		final Namespace am = AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_120, "am");
		final Namespace xsi = AmaltheaNamespaceRegistry.getGenericNamespace("xsi");

		// *** Convert all scheduling algorithms (of schedulers)

		List<Element> algorithmElements = HelperUtil.getXpathResult(
				rootElement,
				"./osModel/operatingSystems/taskSchedulers/schedulingAlgorithm" + "|" +
				"./osModel/operatingSystems/interruptControllers/schedulingAlgorithm",
				Element.class, am, xsi);

		for (Element elem : algorithmElements) {
			Element parent = elem.getParentElement();
			String type = elem.getAttributeValue("type", xsi).substring(3);	// type without prefix "am:"

			// remove child element "schedulingAlgorithm"
			parent.removeChild(SCHEDULING_ALGORITHM);

			// add link to definition
			String definition = findSchedulerDefinitionName(elem, type);
			String link = HelperUtil.encodeName(definition) + "?type=SchedulerDefinition";

			// store parameter extensions of (user specific) algorithm within parent
			for (Element paramExt : elem.getChildren(PARAMETER_EXTENSIONS)) {
				String key = paramExt.getAttributeValue(KEY);
				String value = paramExt.getAttributeValue(VALUE);
				addSchedulingParameter(parent, EXTENDED + key, "am:StringObject", value, null);
			}

			if (myCache.isFirstFile()) {
				// add local reference
				// (example: definition="OSEK?type=SchedulerDefinition")

				parent.setAttribute("definition", link);
			} else {
				// add external reference
				// (example: <definition href="amlt:/#SporadicServer?type=SchedulerDefinition"/>)

				Element definitionElement = new Element("definition");
				definitionElement.setAttribute("href", "amlt:/#" + link);
				parent.addContent(definitionElement);
			}
		}

	}

	private void convertParameterValues(Element rootElement) {
		if (myCache == null || rootElement == null) return;

		// common namespaces
		final Namespace am = AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_120, "am");
		final Namespace xsi = AmaltheaNamespaceRegistry.getGenericNamespace("xsi");

		// *** Replace standard parameters (SchedulingParameters)

		List<Element> parameterElements = HelperUtil.getXpathResult(
				rootElement,
				"./osModel/operatingSystems/taskSchedulers/parentAssociation/schedulingParameters" + "|" +
						"./mappingModel/taskAllocation/schedulingParameters",
						Element.class, am, xsi);

		for (Element elem : parameterElements) {
			Element parent = elem.getParentElement();

			// remove old single child element "schedulingParameters"
			parent.removeChild(SCHEDULING_PARAMETERS);

			// add new multiple child elements "schedulingParameters"
			String priorityValue = elem.getAttributeValue(PRIORITY);
			if (priorityValue != null && (! priorityValue.isEmpty())) {
				addIntegerSchedulingParameter(parent, PRIORITY, priorityValue);
			}

			Element minBudget = elem.getChild(MIN_BUDGET);
			if (minBudget != null) {
				addTimeSchedulingParameter(parent, MIN_BUDGET, minBudget);
			}

			Element maxBudget = elem.getChild(MAX_BUDGET);
			if (maxBudget != null) {
				addTimeSchedulingParameter(parent, MAX_BUDGET, maxBudget);
			}

			Element replenishment = elem.getChild(REPLENISHMENT2);
			if (replenishment != null) {
				addTimeSchedulingParameter(parent, REPLENISHMENT2, replenishment);
			}
		}

		// *** Replace extended parameters (ParameterExtensions)

		List<Element> extendedParameterElements = HelperUtil.getXpathResult(
				rootElement,
				"./osModel/operatingSystems/taskSchedulers/parentAssociation/parameterExtensions" + "|" +
				"./mappingModel/taskAllocation/parameterExtensions",
				Element.class, am, xsi);

		for (Element elem : extendedParameterElements) {
			Element parent = elem.getParentElement();

			String key = elem.getAttributeValue(KEY);
			String value = elem.getAttributeValue(VALUE);

			// remove old child element "parameterExtensions"
			parent.removeChild(PARAMETER_EXTENSIONS);

			// add new child elements "schedulingParameters"
			addSchedulingParameter(parent, EXTENDED + key, "am:StringObject", value, null);
		}

	}

	// private helper methods

	private String findSchedulerDefinitionName(Element algorithmElement, String type) {
		if ("UserSpecificSchedulingAlgorithm".equals(type)) {
			// collect parameter names
			List<String> parameters = algorithmElement.getChildren(PARAMETER_EXTENSIONS).stream()
				.map(param -> param.getAttributeValue(KEY))
				.collect(Collectors.toList());

			// find the algorithm with the correct parameters
			for (Entry<String, List<String>> entry : myCache.getUserSpecificAlgorithmCache().entrySet()) {
				if (entry.getValue().equals(parameters)) {
					return entry.getKey();
				}
			}
			return null;
		} else {
			// standard case: use type as definition name
			// (and replace old names)
			return SchedulerConverterUtil.getNewSchedulerName(type);
		}
	}

	private <T> List<T> sortedList(Collection<T> collection) {
		return collection.stream()
				.sorted()
				.collect(Collectors.toList());
	}

	private List<Entry<String, List<String>>> sortedEntryList(Collection<Entry<String, List<String>>> entries) {
		return entries.stream()
				.sorted(Comparator.comparing(Entry::getKey))
				.collect(Collectors.toList());
	}

	private void addIntegerSchedulingParameter(Element parent, String definition, String value) {
		addSchedulingParameter(parent, definition, "am:IntegerObject", value, null);
	}

	private void addTimeSchedulingParameter(Element parent, String definition, Element timeElement) {
		addSchedulingParameter(parent, definition, "am:Time", timeElement.getAttributeValue(VALUE), timeElement.getAttributeValue(UNIT));
	}

	private void addSchedulingParameter(Element parent, String definition, String type, String value, String unit) {
		// create parameter element
		Element parameterElement = new Element(SCHEDULING_PARAMETERS);
		parent.addContent(parameterElement);

		// add parameter key
		String keyLink = HelperUtil.encodeName(definition) + "?type=SchedulingParameterDefinition";
		if (myCache.isFirstFile()) {
			// add local key reference
			parameterElement.setAttribute(KEY, keyLink);
		} else {
			// add external key reference
			Element keyElement = new Element(KEY);
			keyElement.setAttribute("href", "amlt:/#" + keyLink);
			parameterElement.addContent(keyElement);
		}

		// add parameter value
		Element valueElement = new Element(VALUE);
		valueElement.setAttribute("type", type, AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));
		if (value != null) {
			valueElement.setAttribute(VALUE, value);
		}
		if (unit != null) {
			valueElement.setAttribute(UNIT, unit);
		}
		parameterElement.addContent(valueElement);
	}

}
