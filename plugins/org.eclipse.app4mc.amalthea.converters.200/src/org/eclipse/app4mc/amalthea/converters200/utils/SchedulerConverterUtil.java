/**
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch - initial API and implementation
 */

package org.eclipse.app4mc.amalthea.converters200.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters200.utils.StandardSchedulers.Algorithm;
import org.eclipse.app4mc.amalthea.converters200.utils.StandardSchedulers.Parameter;
import org.eclipse.app4mc.amalthea.converters200.utils.StandardSchedulers.Type;
import org.jdom2.Element;

public class SchedulerConverterUtil {

	private SchedulerConverterUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static List<String> getStandardParameterNames(String algorithmName) {
		List<String> result = new ArrayList<>();

		for (Parameter param :  StandardSchedulers.getAllParametersOfAlgorithm(algorithmName)) {
			result.add(param.getParameterName());
		}

		return result;
	}

	public static void addParameterDefinition(Element osModel, String parameterName) {
		// defaults (if no standard definition is available)
		Type parameterType = Type.STRING;
		boolean parameterMany = false;
		boolean parameterMandatory = false;
		String parameterDefaultValue = null;

		Parameter param = StandardSchedulers.getParameter(parameterName);
		if (param != null) {
			// parameter has standard definition
			parameterType = param.getType();
			parameterMany = param.isMany();
			parameterMandatory = param.isMandatory();
			parameterDefaultValue = param.getDefaultValue();
		}

		Element paramDefElement = new Element("schedulingParameterDefinitions");
		osModel.addContent(paramDefElement);

		String idValue = HelperUtil.encodeName(parameterName) + "?type=SchedulingParameterDefinition";
		paramDefElement.setAttribute("id", idValue, AmaltheaNamespaceRegistry.getGenericNamespace("xmi"));
		paramDefElement.setAttribute("name", parameterName);
		paramDefElement.setAttribute("type", parameterType.getTypeName());
		paramDefElement.setAttribute("many", String.valueOf(parameterMany));
		paramDefElement.setAttribute("mandatory", String.valueOf(parameterMandatory));

		if (parameterDefaultValue != null) {
			Element paramDefDefaultElement = new Element("defaultValue");
			paramDefElement.addContent(paramDefDefaultElement);

			paramDefDefaultElement.setAttribute("type", buildTypeString(parameterType), AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));
			if (parameterType == Type.TIME) {
				Pattern p = Pattern.compile("(-?\\d+)\\s?(s|ms|us|ns|ps)");
				Matcher m = p.matcher(parameterDefaultValue);
				if(m.matches()) {
					String timeValue = m.group(1);
					String timeUnit = m.group(2);
					paramDefDefaultElement.setAttribute("value", timeValue);
					paramDefDefaultElement.setAttribute("unit", timeUnit);
				}
			} else {
				paramDefDefaultElement.setAttribute("value", parameterDefaultValue);
			}
		}
	}

	private static String buildTypeString(Type type) {
		switch (type) {
		case TIME: return "am:Time";
		case BOOL: return "am:BooleanObject";
		case INTEGER: return "am:IntegerObject";
		case FLOAT: return "am:FloatObject";
		case STRING: return "am:StringObject";
		default:
			return "";
		}
	}

	public static void addSchedulerDefinition(Element osModel, String algorithmName) {
		Algorithm algorithm = StandardSchedulers.getAlgorithm(getNewSchedulerName(algorithmName));

		if (algorithm == null) {
			// use name of unknown scheduler
			addSchedulerDefinition(osModel, algorithmName, null, null, null, null);
		} else {
			// use values of standard scheduler
			String name = algorithm.getAlgorithmName();
			String description = algorithm.getDescription();
			List<String> paramNames1 = algorithm.getAlgorithmParameterNames();
			List<String> paramNames2 = algorithm.getProcessParameterNames();
			List<Boolean> options = Arrays.asList(algorithm.hasExactlyOneChild(), algorithm.passesParametersUpwards(), algorithm.requiresParentScheduler());

			addSchedulerDefinition(osModel, name, description, paramNames1, paramNames2, options);
		}
	}

	public static void addSchedulerDefinition(Element osModel, String name, String description, List<String> algorithmParameters, List<String> processParameters, List<Boolean> options) {
		Element schedulerDefElement = new Element("schedulerDefinitions");
		osModel.addContent(schedulerDefElement);

		String idValue = HelperUtil.encodeName(name) + "?type=SchedulerDefinition";
		schedulerDefElement.setAttribute("id", idValue, AmaltheaNamespaceRegistry.getGenericNamespace("xmi"));

		// name
		schedulerDefElement.setAttribute("name", name);

		// description
		if ((description != null) && (! description.isEmpty())) {
			schedulerDefElement.setAttribute("description", description);
		}

		// Parameters
		if ((algorithmParameters != null) && (! algorithmParameters.isEmpty())) {
			schedulerDefElement.setAttribute("algorithmParameters", buildParametersString(algorithmParameters));
		}
		if ((processParameters != null) && (! processParameters.isEmpty())) {
			schedulerDefElement.setAttribute("processParameters", buildParametersString(processParameters));
		}

		// options
		if ((options != null) && (options.size() == 3)) {
			schedulerDefElement.setAttribute("hasExactlyOneChild", options.get(0).toString());
			schedulerDefElement.setAttribute("passesParametersUpwards", options.get(1).toString());
			schedulerDefElement.setAttribute("requiresParentScheduler", options.get(2).toString());
		}
	}

	private static String buildParametersString(List<String> parameters) {
		return parameters.stream()
			.map(p -> HelperUtil.encodeName(p) + "?type=SchedulingParameterDefinition")
			.collect(Collectors.joining(" "));
	}

	public static String getNewSchedulerName(String oldName) {
		// the following names of standard schedulers were changed in 2.0.0
		if ("Grouping".equals(oldName)) return "GroupingServer";
		if ("PollingPeriodicServer".equals(oldName)) return "PollingServer";
		if ("ConstantBandwidthServerWithCASH".equals(oldName)) return "ConstantBandwidthServerWithCapacitySharing";

		return oldName;
	}
}
