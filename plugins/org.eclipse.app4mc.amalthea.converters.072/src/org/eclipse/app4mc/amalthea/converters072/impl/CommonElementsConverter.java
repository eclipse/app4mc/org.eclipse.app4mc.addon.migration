/**
 ********************************************************************************
 * Copyright (c) 2017-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters072.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.7.1",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.7.2"},
		service = IConverter.class)

public class CommonElementsConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> fileName2documentMap, List<ICache> caches) {

		logger.info("Migration from 0.7.1 to 0.7.2 : Executing Common Elements converter for model file : {0}",
				targetFile.getName());

		final Document root = fileName2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		updateCommonElements(rootElement);
	}

	/**
	 * Bug 510086 : As per the model changes in 0.7.2 ==>
	 *
	 * @param rootElement
	 */
	private void updateCommonElements(final Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./componentsModel/tags");
		xpathBuffer.append("|");
		xpathBuffer.append("./hwModel/tags");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/tags");

		final List<Element> tagElements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		if (!tagElements.isEmpty()) {

			logger.warn(
					"Tag element's from Components Model, HW Model, SW Model are shifted to CommonElements model");

			Element commonElements = rootElement.getChild("commonElements");

			// check if "commonElements" tag is already existing. If not then create a Element
			if (commonElements == null) {
				commonElements = new Element("commonElements");
				rootElement.addContent(commonElements);
			}

			for (final Element tagElement : tagElements) {
				final Element parentElement = tagElement.getParentElement();
				// removing Tag Elements from its parent and associating it to commonElements Tag
				parentElement.removeContent(tagElement);
				commonElements.addContent(tagElement);
			}
		}
	}

}
