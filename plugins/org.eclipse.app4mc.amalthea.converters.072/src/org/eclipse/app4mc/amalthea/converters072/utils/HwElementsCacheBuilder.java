/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters072.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.7.1")

public class HwElementsCacheBuilder implements ICache {

	@Reference
	SessionLogger logger;

	private final HashMap<File, Map<String, Object>> map = new HashMap<>();

	@Override
	public void buildCache(final Map<File, Document> fileDocumentMapping) {

		if (logger != null) {
			logger.info("Build up HwElementsCache for 0.7.1");
		}

		for (Entry<File, Document> entry : fileDocumentMapping.entrySet()) {
			File targetFile = entry.getKey();
			Document document = entry.getValue();

			if (document == null) {
				// log error message
				continue;
			}

			Element rootElement = document.getRootElement();

			/*- Start : Cache initialization */
			Map<String, Object> fileCache = map.get(targetFile);
			if (fileCache == null) {
				fileCache = new HashMap<>();
				map.put(targetFile, fileCache);
			}

			ArrayList<String> coreNames = new ArrayList<>();
			fileCache.put(HwElementsCacheEnum.CORE_NAMES.name(), coreNames);

			ArrayList<String> coreTypeNames = new ArrayList<>();
			fileCache.put(HwElementsCacheEnum.CORE_TYPE_NAMES.name(), coreTypeNames);

			ArrayList<String> memoryNames = new ArrayList<>();
			fileCache.put(HwElementsCacheEnum.MEMORY_NAMES.name(), memoryNames);

			ArrayList<String> memoryTypeNames = new ArrayList<>();
			fileCache.put(HwElementsCacheEnum.MEMORY_TYPE_NAMES.name(), memoryTypeNames);

			fileCache.put(HwElementsCacheEnum.HW_MODEL.name(), null);

			/*- End : Cache initialization */

			List<Element> hwModelElements = HelperUtil.getXpathResult(
					rootElement,
					"./hwModel",
					Element.class,
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_070, "am"));

			if (! hwModelElements.isEmpty()) {
				fileCache.put(HwElementsCacheEnum.HW_MODEL.name(), hwModelElements.get(0));
			}

			StringBuilder xpathBuilder = new StringBuilder();

			xpathBuilder.append("./hwModel//memories");
			xpathBuilder.append("|");
			xpathBuilder.append("./hwModel//cores");
			xpathBuilder.append("|");
			xpathBuilder.append("./hwModel/coreTypes");
			xpathBuilder.append("|");
			xpathBuilder.append("./hwModel/memoryTypes");

			List<Element> hwElements = HelperUtil.getXpathResult(
					rootElement,
					xpathBuilder.toString(),
					Element.class,
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_070, "am"));

			for (Element hwElement : hwElements) {

				if ("memories".equals(hwElement.getName())) {
					memoryNames.add(hwElement.getAttributeValue("name"));
				}
				else if ("cores".equals(hwElement.getName()) || "coreTypes".equals(hwElement.getName()) || "memoryTypes".equals(hwElement.getName())) {
					coreNames.add(hwElement.getAttributeValue("name"));
				}
			}
		}
	}

	@Override
	public Map<File, Map<String, Object>> getCacheMap() {
		return this.map;
	}

	@Override
	public void clearCacheMap() {

		this.map.clear();
	}

}
