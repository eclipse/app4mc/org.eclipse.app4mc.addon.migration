/**
 ********************************************************************************
 * Copyright (c) 2017-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters072.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.7.1",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.7.2"},
		service = IConverter.class)

public class ConfigConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> fileName2documentMap, List<ICache> caches) {

		logger.info("Migration from 0.7.1 to 0.7.2 : Executing Config Model converter for model file : {0}",
				targetFile.getName());

		final Document root = fileName2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		updateEventConfig(rootElement);
	}

	/**
	 * Bug 510086 : As per the model changes in 0.7.2 ==>
	 *
	 * EventConfigLink and EventConfigElement are removed <br>
	 * ==> Instead (in 0.7.2) EntityEvent is referred inside EventConfig object.
	 *
	 * @param rootElement
	 */
	private void updateEventConfig(final Element rootElement) {

		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./configModel/eventsToTrace");

		final List<Element> eventsToTracelements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		if (! eventsToTracelements.isEmpty()) {

			Element eventModel = rootElement.getChild("eventModel");

			// check if "eventModel" tag is already existing. If not then create a Element
			if (eventModel == null) {
				eventModel = new Element("eventModel");
				rootElement.addContent(eventModel);
			}

			for (final Element eventsToTracelement : eventsToTracelements) {

				final String tagType = eventsToTracelement.getAttributeValue("type", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

				eventsToTracelement.removeAttribute("type", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

				if ("am:EventConfigElement".equals(tagType)) {

					final Element eventElement = eventsToTracelement.getChild("event");
					if (eventElement != null) {

						// changing the event tag name to events

						eventElement.setName("events");

						// Removing Event element from EventConfigElement
						eventsToTracelement.removeContent(eventElement);

						// Shifting Event element (from config model) to EventModel
						eventModel.addContent(eventElement);

						// removing the EventConfigElement defintion as it no longer has any valid data

						eventsToTracelement.getParent().removeContent(eventsToTracelement);
					}
				}
			}
		}
	}

}
