/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters072.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.7.1",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.7.2"},
		service = IConverter.class)

public class ConstraintsConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> fileName2documentMap, List<ICache> caches) {

		logger.info("Migration from 0.7.1 to 0.7.2 : Executing Constraints converter for model file : {0}",
				targetFile.getName());

		final Document root = fileName2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		updateAffinityConstraints(rootElement);
	}

	private void updateAffinityConstraints(final Element rootElement) {

		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./constraintsModel/affinityConstraints");

		final List<Element> affinityConstraintElements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (final Element affinityConstraintElement : affinityConstraintElements) {

			final String type = affinityConstraintElement.getAttributeValue("type", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			if (type != null) {
				Element childElement = null;
				if ("am:RunnablePairingConstraint".equals(type)) {

					childElement = affinityConstraintElement.getChild("runnables");
				}
				else if ("am:ProcessPairingConstraint".equals(type)) {
					childElement = affinityConstraintElement.getChild("processes");

				}
				else if ("am:DataPairingConstraint".equals(type)) {
					childElement = affinityConstraintElement.getChild("labels");

				}

				if (childElement != null) {
					childElement.setName("group");
				}

				// in 0.7.2, TargetProcess, TargetCallSequence elements are removed.
				final Element targetElement = affinityConstraintElement.getChild("target");

				if (targetElement != null) {

					final String targetType = targetElement.getAttributeValue("type", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

					if (targetType != null
							&& ("am:TargetCallSequence".equals(targetType) || "am:TargetProcess".equals(targetType))) {

						affinityConstraintElement.removeContent(targetElement);
					}
				}
			}
		}
	}

}
