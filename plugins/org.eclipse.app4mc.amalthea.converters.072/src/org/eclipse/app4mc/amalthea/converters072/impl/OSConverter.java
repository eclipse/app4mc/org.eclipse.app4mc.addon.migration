/**
 ********************************************************************************
 * Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters072.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Parent;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.7.1",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.7.2",
			"service.ranking:Integer=90"},
		service = IConverter.class)

public class OSConverter extends AbstractConverter {

	private static final String LEVEL = "level";
	private static final String OS_DATA_CONSISTENCY = "osDataConsistency";

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {

		logger.info(
				"Migration from 0.7.1 to 0.7.2 : Executing OS converter for model file : {0}", targetFile.getName());

		final Document root = filename2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		updateAffinityConstraints(rootElement);

		updateOsExecutionInstructions(rootElement);

		updateOsBuffering(rootElement);
	}

	/**
	 * This method is used to migrate the OsBuffering element as per the changes in 0.7.2.
	 *
	 * Below are the steps which are followed
	 *
	 * <pre>
	 *  * OsBuffering element is replaced with element OsDataConsistency
	 *  		Case 1: If (OsBuffering::runnableLevel || OsBuffering::processLevel || OsBuffering::scheduleSectionLevel) =="true" then
	 *  					- OsDataConsistency object is created and the mode is set as "automaticProtection"
	 *  						- DataStability object is created inside OsDataConsistency and below properties are set
	 *  								- enabled=true
	 *  								- algorithm = OsBuffering::bufferingAlgorithm
	 *  								- accessMultiplicity = multipleAccesses
	 *  								- level = <respective OsBuffering flag>
	 *         Case 2: If (OsBuffering::runnableLevel || OsBuffering::processLevel || OsBuffering::scheduleSectionLevel) =="false" then
	 *         				- OsDataConsistency object is created and the mode is set as "noProtection"
	 *
	 *         Case 3: If OsBuffering element is not present then
	 *         				- OsDataConsistency object is created and the mode is set as "_undefined_" (i.e. default)
	 *
	 * </pre>
	 *
	 * @param rootElement
	 */
	private void updateOsBuffering(final Element rootElement) {

		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./osModel/osBuffering");

		final List<Element> osBufferingElements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		if (!osBufferingElements.isEmpty()) {

			for (final Element osBufferingElement : osBufferingElements) {

				final boolean isRunnableLevel = Boolean.parseBoolean(osBufferingElement.getAttributeValue("runnableLevel"));
				final boolean isProcessLevel = Boolean.parseBoolean(osBufferingElement.getAttributeValue("processLevel"));
				final boolean isScheduleSectionLevel = Boolean.parseBoolean(osBufferingElement.getAttributeValue("scheduleSectionLevel"));
				final String bufferingAlgorithm = osBufferingElement.getAttributeValue("bufferingAlgorithm");

				if (isRunnableLevel || isProcessLevel || isScheduleSectionLevel) {

					final Parent osModelElement = osBufferingElement.getParent();
					osModelElement.removeContent(osBufferingElement);

					final Element osDataConsistencyElement = new Element(OS_DATA_CONSISTENCY);
					osDataConsistencyElement.setAttribute("mode", "automaticProtection");

					final Element dataStabilityElement = new Element("dataStability");
					osDataConsistencyElement.addContent(dataStabilityElement);

					dataStabilityElement.setAttribute("enabled", "true");
					dataStabilityElement.setAttribute("algorithm",
							bufferingAlgorithm != null ? bufferingAlgorithm : "");
					dataStabilityElement.setAttribute("accessMultiplicity", "multipleAccesses");

					if (isProcessLevel) {
						dataStabilityElement.setAttribute(LEVEL, "process");
					} else if (isRunnableLevel) {
						dataStabilityElement.setAttribute(LEVEL, "runnable");
					} else if (isScheduleSectionLevel) {
						dataStabilityElement.setAttribute(LEVEL, "scheduleSection");
					}

					osModelElement.addContent(osDataConsistencyElement);

				} else {
					// this is the case where all the booleans are resulting false

					final Parent osModelElement = osBufferingElement.getParent();
					osModelElement.removeContent(osBufferingElement);
					final Element osDataConsistencyElement = new Element(OS_DATA_CONSISTENCY);
					osDataConsistencyElement.setAttribute("mode", "noProtection");

					osModelElement.addContent(osDataConsistencyElement);
				}
			}

		} else {
			// this is the case where OSBuffering is not present

			final Element osModelElement = rootElement.getChild("osModel");

			if (osModelElement != null) {
				final Element osDataConsistencyElement = new Element(OS_DATA_CONSISTENCY);
				osModelElement.addContent(osDataConsistencyElement);
			}
		}
	}

	private void updateOsExecutionInstructions(final Element rootElement) {


		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./osModel/operatingSystems/taskSchedulers/schedulingUnit/instructions");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/operatingSystems/interruptControllers/schedulingUnit/instructions");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiSendMessage");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiTerminateTask");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiSchedule");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiRequestResource");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiReleaseResource");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiSetEvent");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiWaitEvent");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiClearEvent");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiActivateTask");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/apiOverhead/apiEnforcedMigration");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/isrCategory1Overhead/preExecutionOverhead");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/isrCategory1Overhead/postExecutionOverhead");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/isrCategory2Overhead/preExecutionOverhead");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/osOverheads/isrCategory2Overhead/postExecutionOverhead");

		final List<Element> instructionElements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (final Element instructionElement : instructionElements) {

			final Attribute attribute = instructionElement.getAttribute("type", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			if (attribute != null) {
				final String value = attribute.getValue();
				if (value != null) {
					if ("am:OsExecutionInstructionsConstant".equals(value)) {
						attribute.setValue("am:InstructionsConstant");
					} else if ("am:OsExecutionInstructionsDeviation".equals(value)) {
						attribute.setValue("am:InstructionsDeviation");
					}

				}
			}
		}
	}

	private void updateAffinityConstraints(final Element rootElement) {

		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./osModel/semaphores");

		final List<Element> semaphoreElements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (final Element semaphoreElement : semaphoreElements) {

			final Attribute attribute = semaphoreElement.getAttribute("initalValue");

			if (attribute != null) {
				attribute.setName("initialValue");
			}
		}
	}

}
