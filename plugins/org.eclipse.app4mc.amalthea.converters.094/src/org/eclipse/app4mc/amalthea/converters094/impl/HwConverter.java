/**
 ********************************************************************************
 * Copyright (c) 2019, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters094.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.9.3",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.9.4"},
		service = IConverter.class)

public class HwConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {

		logger.info("Migration from 0.9.3 to 0.9.4 : Executing Hw converter for model file : {0}", targetFile.getName());

		final Document document = filename2documentMap.get(targetFile);
		if (document == null) {
			return;
		}

		final Element rootElement = document.getRootElement();

		updatePortInterface(rootElement);
	}

	private void updatePortInterface(final Element rootElement) {
		final String xpath = "./hwModel/structures//ports[@portInterface=\"ABH\"]";

		final List<Element> portInterfaceElements = HelperUtil.getXpathResult(
				rootElement,
				xpath,
				Element.class,
				AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_094, "am"),
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (Element portInterfaceElement : portInterfaceElements) {

			Attribute portInterfaceAttribute = portInterfaceElement.getAttribute("portInterface");
			portInterfaceAttribute.setValue("AHB");
		}
	}

}
