/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.converters.headless.app;

import java.util.Arrays;

import org.eclipse.osgi.service.environment.EnvironmentInfo;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * Immediate component that gets activated once the Equinox
 * {@link EnvironmentInfo} is available. Triggers the migration process based on
 * the command line arguments. Via -console parameter it is also possible to
 * simply start the application in an interactive mode by keeping the OSGi
 * console open. Otherwise the application will immediately closed after
 * execution.
 * <p>
 * <b>Note:</b><br>
 * Even when starting the app with the Equinox OSGi framework via bnd launcher,
 * the {@link EnvironmentInfo} will be available. But it will not contain the
 * necessary information that typically gets set the the Equinox launcher.
 * Therefore the migration start will not be processed if the
 * {@link EnvironmentInfo} is not initialized properly.
 * </p>
 */
@Component(immediate = true)
public class EquinoxMigrationStarter extends AbstractMigrationStarter {

	/**
	 * Launcher arguments provided by the Equinox launcher.
	 */
	@Reference
	EnvironmentInfo environmentInfo;

	@Reference
	void setModelMigrationCommand(ModelMigrationCommand command) {
		this.command = command;
	}

	@Activate
	void activate() {
		// only process model migration activation if the EnvironmentInfo is initialized
		// properly
		if (this.environmentInfo.getNonFrameworkArgs() != null) {
			// check if a product or an application was started
			// if yes this starter should do nothing
			boolean isProduct =
					Arrays.stream(environmentInfo.getNonFrameworkArgs()).anyMatch("-product"::equals)
					|| this.environmentInfo.getProperty("eclipse.product") != null;
			boolean isApplication =
					Arrays.stream(environmentInfo.getNonFrameworkArgs()).anyMatch("-application"::equals)
					|| this.environmentInfo.getProperty("eclipse.application") != null;

			if (!isProduct && !isApplication) {
				// check if -console or -consoleLog was provided as launch argument
				boolean isInteractive = Arrays.stream(environmentInfo.getFrameworkArgs()).anyMatch("-console"::equals);
				boolean showConsoleLog = Arrays.stream(environmentInfo.getFrameworkArgs()).anyMatch("-consoleLog"::equals);

				activate(this.environmentInfo.getNonFrameworkArgs(), isInteractive, showConsoleLog);
			}
		}
	}
}
