/**
 ********************************************************************************
 * Copyright (c) 2019, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.converters.headless.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;

/**
 * Abstract implementation for migration starter components. Contains the logic
 * for inspecting the command line arguments and starts the migration.
 */
public class AbstractMigrationStarter {

	/**
	 * The ModelMigrationCommand that does the actual migration task.
	 */
	protected ModelMigrationCommand command;

	/**
	 * Start the migration process based on the given parameter.
	 *
	 * @param args           The non-framework command line arguments.
	 * @param isInteractive  <code>true</code> if the OSGi console should be opened
	 *                       and stay open.
	 * @param showConsoleLog <code>true</code> if a separate shell should be opened
	 *                       and stay open until user input to verify the outputs
	 *                       when starting via Equinox launcher executable.
	 */
	// Suppress the sonar warnings:
	// S2677 - "read" and "readLine" return values should be used
	//         the return value of BufferedReader#readline() is actually not needed in this case
	//         as we only block the process until any user input and the input is not evaluated
	// S106  - Standard outputs should not be used directly to log anything
	//         System.out and System.err are used to provide feedback in a command line interface
	@SuppressWarnings( {"squid:S2677" , "squid:S106"})
	protected void activate(String[] args, boolean isInteractive, boolean showConsoleLog) {
		if (args != null && args.length > 0) {
			String migrationVersion = "latest";
			boolean recursive = false;
			boolean noBackup = false;
			ArrayList<String> filenameList = new ArrayList<>();

			// check if a version parameter was used and collect the filenames to convert
			for (int i = 0; i < args.length; i++) {
				String arg = args[i];

				if (arg.startsWith("-v") || arg.startsWith("--version")) {
					i++;
					migrationVersion = args[i];
				} else if (arg.startsWith("-r") || arg.startsWith("--recursive")) {
					recursive = true;
				} else if (arg.startsWith("-nb") || arg.startsWith("--nobackup")) {
					noBackup = true;
				} else if (arg.startsWith("/?") || arg.startsWith("-h") || arg.startsWith("--help")) {
					// only print the help and exit
					System.out.println();
					System.out.println("APP4MC Amalthea Model Migration " + ModelVersion.getLatestVersion());
					System.out.println();
					System.out.println("[-v, --version <model_version>] [-r, --recursive] [-nb, --nobackup] <filename>");
					System.out.println();
					System.out.println("Options:");
					System.out.println("\t-v, --version  \tThe model version to which the model should be migrated to [optional].");
					System.out.println("\t\t\tIf not provided the latest supported version will be used.");
					System.out.println();
					System.out.println("\t-r, --recursive\tIf folders should be traversed recursively [optional].");
					System.out.println("\t\t\tIf not provided only the files in the given folder will be migrated.");
					System.out.println();
					System.out.println("\t-nb, --nobackup\tIf backup files should be created before the migration process [optional].");
					System.out.println("\t\t\tIf not provided a backup file will be created for every model file.");
					System.out.println();
					System.out.println("\t-h, --help, /?\tShow this help.");
					System.out.println();
					System.out.println("Parameter:");
					System.out.println("\tfilename\tThe filename of the model file or the folder that contains model files to migrate");
					System.exit(0);
				} else if (!arg.startsWith("-") && arg.trim().length() > 0){
					filenameList.add(arg);
				}
			}

			for (String file : filenameList) {
				command.convert(migrationVersion, recursive, noBackup, file);
			}

			// This code is used in case of using the equinox launcher executable with
			// -consoleLog parameter. With the -consoleLog parameter a separate shell is
			// opened. To avoid that it is closed immediately a simple input is requested to
			// close, so a user can inspect the outputs.
			if (showConsoleLog) {
				System.out.println();
				System.out.println("***** Press Enter to exit *****");
				// just wait for a Enter
				try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
					reader.readLine();
				} catch (IOException e) {
					System.err.println("Error on waiting for user input");
					e.printStackTrace(System.err);
				}
			}
		}

		if (!isInteractive) {
			// shutdown the application if no console was opened and this component
			// triggered the migration
			System.exit(0);
		}
	}

}
