/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.converters.headless.app;

import java.util.Arrays;
import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * Immediate component that gets activated once the launcher arguments provided
 * by the bnd launcher are available. Triggers the migration process based on
 * the command line arguments. Via -console parameter it is also possible to
 * simply start the application in an interactive mode by keeping the OSGi
 * console open. Otherwise the application will immediately closed after
 * execution.
 */
@Component(immediate = true)
public class BndMigrationStarter extends AbstractMigrationStarter {

	/**
	 * Launcher arguments provided by the bnd launcher.
	 */
	String[] launcherArgs;

	@Reference(target = "(launcher.arguments=*)")
	void setLauncherArguments(Object object, Map<String, Object> map) {
		this.launcherArgs = (String[]) map.get("launcher.arguments");
	}

	@Reference
	void setModelMigrationCommand(ModelMigrationCommand command) {
		this.command = command;
	}

	@Activate
	void activate() {
		String console = System.getProperty("osgi.console");
		boolean isInteractive =  console != null && console.length() == 0;

		// clear launcher arguments from possible framework parameter
		String[] args = Arrays.stream(launcherArgs)
				.filter(arg -> !"-console".equals(arg) && !"-consoleLog".equals(arg))
				.toArray(String[]::new);

		activate(args, isInteractive, false);
	}
}
