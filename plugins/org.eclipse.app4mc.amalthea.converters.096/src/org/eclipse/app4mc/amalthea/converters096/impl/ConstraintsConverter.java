/**
 ********************************************************************************
 * Copyright (c) 2019, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters096.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.9.5",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.9.6"},
		service = IConverter.class)

public class ConstraintsConverter extends AbstractConverter {

	private static final Namespace XSI_NAMESPACE = AmaltheaNamespaceRegistry.getGenericNamespace("xsi");

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File file, Map<File, Document> map, List<ICache> caches) {

		logger.info("Migration from 0.9.5 to 0.9.6 : Executing Constraints converter for model file : {0}", file.getName());

		basicConvert(file, map);
	}

	public void basicConvert(final File file, final Map<File, Document> map) {

		final Document document = map.get(file);
		if (document == null) {
			return;
		}

		final Element rootElement = document.getRootElement();
		updateEventChains(rootElement);
	}

	private void updateEventChains(Element rootElement) {
		 final String xpath = "./constraintsModel/eventChains";
		 final List<Element> eventChains = HelperUtil.getXpathResult(
				 rootElement,
				 xpath,
				 Element.class,
				 AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), "am"),
				 XSI_NAMESPACE);
				 boolean hasSegments = false;
				 for (Element element : eventChains) {
			 hasSegments = false;
			List<Element> segments = new ArrayList<>(element.getChildren("segments"));
			if(!segments.isEmpty()) {
				//convert segments to items
				convertToItems(segments, element,"sequence");
				hasSegments = true;
			}

			List<Element> strands = new ArrayList<>(element.getChildren("strands"));
			if(hasSegments && !strands.isEmpty()) {
				logger.info("EventChains Migration from 0.9.5 to 0.9.6 : Removing strands because Segments are already migrated and both cannot exist at a time in 0.9.6.");
				for (Element strand : strands) {
					strand.detach();
				}
				continue;
			}

			if(!strands.isEmpty()) {
				//convert strands to items
				convertToItems(strands, element,"parallel");
			}
		}
	}

	private void convertToItems(List<Element> segments,Element eventChain,String itemType) {

		//create itemType
		Attribute itemTypeAttr = new Attribute("itemType",itemType);
		eventChain.setAttribute(itemTypeAttr);

		for (Element element : segments) {
			element.setName("items");
			if("am:EventChainContainer".equals(element.getAttributeValue("type", XSI_NAMESPACE))) {
				Element subevent = element.getChild("eventChain");
				subevent.setAttribute("minItemsCompleted", "1");

				eventChain.setAttribute("minItemsCompleted", "1");
			}
		}
	}

}
