/**
 ********************************************************************************
 * Copyright (c) 2019, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters096.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.9.5",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.9.6"},
		service = IConverter.class)

public class SwConverter extends AbstractConverter {

	private static final String AM = "am";
	private static final String XSI = "xsi";
	private static final String KEY = "key";
	private static final String VALUE = "value";

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {
		logger.info("Migration from 0.9.5 to 0.9.6 : Executing Sw converter for model file : {0}", targetFile.getName());
		basicConvert(targetFile, filename2documentMap);
	}

	public void basicConvert(final File file, final Map<File, Document> map) {

		final Document document = map.get(file);
		if (document == null) {
			return;
		}

		final Element rootElement = document.getRootElement();

		updateCustomPropsToCounter(rootElement);

		updateModeConditions(rootElement);
	}

	private void updateModeConditions(Element rootElement) {
		updateModeCondition(rootElement);
		updateModeConditionConjunction(rootElement);
	}

	private void updateModeCondition(Element rootElement) {
		 final StringBuilder xpathBuffer = new StringBuilder();

		 xpathBuffer.append("./swModel/tasks/callGraph/items//entries[@xsi:type=\"am:ModeCondition\"]");
		 xpathBuffer.append("|");
		 xpathBuffer.append("./swModel/isrs/callGraph/items//entries[@xsi:type=\"am:ModeCondition\"]");
		 xpathBuffer.append("|");
		 xpathBuffer.append("./swModel/runnables/callGraph/items//entries[@xsi:type=\"am:ModeCondition\"]");
		 xpathBuffer.append("|");
		 xpathBuffer.append("./swModel/runnables/executionCondition//entries[@xsi:type=\"am:ModeCondition\"]");
		 xpathBuffer.append("|");
		 xpathBuffer.append("./stimuliModel/stimuli/executionCondition//entries[@xsi:type=\"am:ModeCondition\"]");

		 final List<Element> modeCondition = HelperUtil.getXpathResult(
				 rootElement,
				 xpathBuffer.toString(),
				 Element.class,
				 AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), AM),
				 AmaltheaNamespaceRegistry.getGenericNamespace(XSI));

		 for (Element element : modeCondition) {
			element.setAttribute("type", "am:ModeValueCondition", AmaltheaNamespaceRegistry.getGenericNamespace(XSI));
		}
	}

	private void updateModeConditionConjunction(Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();

		 xpathBuffer.append("./swModel/tasks/callGraph/items//entries[@xsi:type=\"am:ModeConditionConjunction\"]");
		 xpathBuffer.append("|");
		 xpathBuffer.append("./swModel/isrs/callGraph/items//entries[@xsi:type=\"am:ModeConditionConjunction\"]");
		 xpathBuffer.append("|");
		 xpathBuffer.append("./swModel/runnables/callGraph/items//entries[@xsi:type=\"am:ModeConditionConjunction\"]");
		 xpathBuffer.append("|");
		 xpathBuffer.append("./swModel/runnables/executionCondition//entries[@xsi:type=\"am:ModeConditionConjunction\"]");
		 xpathBuffer.append("|");
		 xpathBuffer.append("./stimuliModel/stimuli/executionCondition//entries[@xsi:type=\"am:ModeConditionConjunction\"]");

		 final List<Element> modeConditions = HelperUtil.getXpathResult(
				 rootElement,
				 xpathBuffer.toString(),
				 Element.class,
				 AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), AM),
				 AmaltheaNamespaceRegistry.getGenericNamespace(XSI));

		 for (Element element : modeConditions) {
			 List<Element> entries = element.getChildren("entries");
			 for (Element entry : entries) {
				entry.setAttribute("type", "am:ModeValueCondition", AmaltheaNamespaceRegistry.getGenericNamespace(XSI));
			}
		}
	}

	private void updateCustomPropsToCounter(Element rootElement) {
		//bring back counter for RunnableCall if the custom properties have counter info.

		StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./swModel/tasks/callGraph//items[@xsi:type=\"am:RunnableCall\"]/customProperties[@key=\"counter-prescaler\"]");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/isrs/callGraph//items[@xsi:type=\"am:RunnableCall\"]/customProperties[@key=\"counter-prescaler\"]");
		xpathBuffer.append("|");
		xpathBuffer.append("./swModel/runnables/callGraph//items[@xsi:type=\"am:RunnableCall\"]/customProperties[@key=\"counter-prescaler\"]");

		final List<Element> customProps = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getNamespace(getOutputModelVersion(), AM),
				AmaltheaNamespaceRegistry.getGenericNamespace(XSI));

		for (Element element : customProps) {
			Element parentElement = element.getParentElement();

			List<Element> customProperties = new ArrayList<>(parentElement.getChildren("customProperties"));
			//create counter
			Element counter = new Element("counter");
			parentElement.addContent(counter);

			for (Element prop : customProperties) {
				if ("counter-prescaler".equals(prop.getAttributeValue(KEY))) {
					counter.setAttribute("prescaler", prop.getChild(VALUE).getAttributeValue(VALUE));
					//remove custom prop
					prop.detach();
					continue;
				}

				if ("counter-offset".equals(prop.getAttributeValue(KEY))) {
					counter.setAttribute("offset", prop.getChild(VALUE).getAttributeValue(VALUE));
					//remove custom prop
					prop.detach();
				}
			}
		}
	}
}
