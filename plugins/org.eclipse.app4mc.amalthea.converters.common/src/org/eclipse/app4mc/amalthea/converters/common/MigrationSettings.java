/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.jdom2.Document;

public class MigrationSettings implements AutoCloseable {

	private List<MigrationInputFile> migModelFiles = new ArrayList<>();

	private String inputModelVersion;

	private String migrationModelVersion = ModelVersion.getLatestVersion();

	private String outputDirectoryLocation;

	private File project;

	private boolean createBackupFile = true;

	public File getProject() {
		return this.project;
	}

	public void setProject(final File project) {
		this.project = project;
		if (this.project != null) {
			setOutputDirectoryLocation(this.project.getAbsolutePath() + File.separator + "_migration");
		}
	}

	public String getOutputDirectoryLocation() {
		return this.outputDirectoryLocation;
	}

	public void setOutputDirectoryLocation(final String outputDirectoryLocation) {
		this.outputDirectoryLocation = outputDirectoryLocation;
	}

	public List<MigrationInputFile> getMigModelFiles() {
		return this.migModelFiles;
	}

	public void setMigModelFiles(final List<MigrationInputFile> migModelFiles) {
		this.migModelFiles = migModelFiles;
	}

	public String getInputModelVersion() {
		return this.inputModelVersion;
	}

	public void setInputModelVersion(final String inputModelVersion) {
		this.inputModelVersion = inputModelVersion;
	}

	public String getMigrationModelVersion() {
		return this.migrationModelVersion;
	}

	public void setMigrationModelVersion(final String migrationModelVersion) {
		this.migrationModelVersion = migrationModelVersion;
	}

	public boolean isCreateBackupFile() {
		return createBackupFile;
	}

	public void setCreateBackupFile(boolean createBackupFile) {
		this.createBackupFile = createBackupFile;
	}

	public Map<File, Document> getMigModelFilesMap() {

		final HashMap<File, Document> migModelsMap = new HashMap<>();

		for (final MigrationInputFile migModelFile : this.migModelFiles) {
			Document document = migModelFile.getDocument();
			if (document != null) {
				migModelsMap.put(migModelFile.getFile(), document);
			}
		}

		return migModelsMap;
	}

	@Override
	public synchronized void close() {
		this.migModelFiles.forEach(MigrationInputFile::dispose);
	}
}
