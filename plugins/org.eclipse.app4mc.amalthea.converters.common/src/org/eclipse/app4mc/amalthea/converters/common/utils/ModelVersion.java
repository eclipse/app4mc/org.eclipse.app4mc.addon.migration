/**
 ********************************************************************************
 * Copyright (c) 2015-2024 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Enumeration of Amalthea model versions. Needs to be maintained in ascending order to support ordered processing.
 *
 * <p>
 * <b>Note: </b> New versions need to be added add the end of the enumeration definition!
 * </p>
 */
public enum ModelVersion {

	VERSION_070("0.7.0"),
	VERSION_071("0.7.1"),
	VERSION_072("0.7.2"),
	VERSION_080("0.8.0"),
	VERSION_081("0.8.1"),
	VERSION_082("0.8.2"),
	VERSION_083("0.8.3"),
	VERSION_090("0.9.0"),
	VERSION_091("0.9.1"),
	VERSION_092("0.9.2"),
	VERSION_093("0.9.3"),
	VERSION_094("0.9.4"),
	VERSION_095("0.9.5"),
	VERSION_096("0.9.6"),
	VERSION_097("0.9.7"),
	VERSION_098("0.9.8"),
	VERSION_099("0.9.9"),

	VERSION_100("1.0.0"),
	VERSION_110("1.1.0"),
	VERSION_120("1.2.0"),

	VERSION_200("2.0.0"),
	VERSION_210("2.1.0"),
	VERSION_220("2.2.0"),

	VERSION_300("3.0.0"),
	VERSION_310("3.1.0"),
	VERSION_320("3.2.0"),
	VERSION_330("3.3.0");

	private final String value;

	private ModelVersion(String v) {
		this.value = v;
	}

	/**
	 *
	 * @return The String representation of this {@link ModelVersion}.
	 */
	public String getVersion() {
		return this.value;
	}

	/**
	 *
	 * @param version The version string for which the {@link ModelVersion} is
	 *                requested.
	 * @return The {@link ModelVersion} for the given version String or
	 *         <code>null</code> if the given input is not a valid model version.
	 */
	public static ModelVersion getModelVersion(String version) {
		return Arrays.stream(ModelVersion.values())
				.filter(v -> v.value.equals(version))
				.findFirst()
				.orElse(null);
	}

	/**
	 * Returns all {@link ModelVersion}s before the given {@link ModelVersion}.
	 *
	 * @param version The {@link ModelVersion} for which the previous
	 *                {@link ModelVersion}s are requested.
	 * @param include <code>true</code> if the given {@link ModelVersion} should be
	 *                included in the result, <code>false</code> if not.
	 * @return All {@link ModelVersion}s before the given {@link ModelVersion}.
	 */
	public static ModelVersion[] getVersionsBefore(ModelVersion version, boolean include) {
		List<ModelVersion> versions = new ArrayList<>();
		ModelVersion[] allVersions = ModelVersion.values();
		for (int i = 0; i < (include ? version.ordinal() + 1 : version.ordinal()); i++) {
			versions.add(allVersions[i]);
		}
		return versions.toArray(new ModelVersion[0]);

	}

	/**
	 *
	 * @return The version numbers of all versions that are supported for migration.
	 */
	public static List<String> getAllSupportedVersions() {
		final List<String> allSupportedVersions = new ArrayList<>();

		ModelVersion[] allVersions = ModelVersion.values();
		for (ModelVersion version : allVersions) {
			allSupportedVersions.add(version.value);
		}

		return allSupportedVersions;
	}

	/**
	 *
	 * @param version The version string to check.
	 * @return <code>true</code> if the given version is a valid version number,
	 *         <code>false</code> if it is invalid.
	 */
	public static boolean isValidVersion(String version) {
		if (version != null) {
			// check if the given version is contained in the collection of supported
			// versions
			return getAllSupportedVersions().contains(version);
		}
		return false;
	}

	/**
	 *
	 * @return The String representation of the latest {@link ModelVersion}.
	 */
	public static String getLatestVersion() {
		ModelVersion[] values = ModelVersion.values();
		return values[values.length-1].value;
	}
}
