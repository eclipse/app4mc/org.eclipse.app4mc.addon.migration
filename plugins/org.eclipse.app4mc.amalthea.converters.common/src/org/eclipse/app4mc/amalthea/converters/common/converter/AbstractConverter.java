/**
 ********************************************************************************
 * Copyright (c) 2019-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common.converter;

import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;

/**
 * This class is responsible for converting the namespace of the AMALTHEA model.
 */
public abstract class AbstractConverter implements IConverter {

	public static final String AMALTHEA = "Amalthea";

	private String inputModelVersion;
	private String outputModelVersion;

	protected void activate(Map<String, Object> properties) {
		this.inputModelVersion = properties.get(ServiceConstants.INPUT_MODEL_VERSION_PROPERTY).toString();
		this.outputModelVersion = properties.get(ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY).toString();
	}

	@Override
	public String getInputModelVersion() {
		return this.inputModelVersion;
	}

	@Override
	public String getOutputModelVersion() {
		return this.outputModelVersion;
	}

}
