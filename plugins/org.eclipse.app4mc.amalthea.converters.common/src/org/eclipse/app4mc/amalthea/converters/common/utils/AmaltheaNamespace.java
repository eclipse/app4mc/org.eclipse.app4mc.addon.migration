/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common.utils;

import java.util.LinkedHashMap;
import java.util.stream.Collectors;

import org.jdom2.Namespace;

public class AmaltheaNamespace {

	private final LinkedHashMap<String, String> prefixUriMapping = new LinkedHashMap<>();

	public final void registerMapping(String prefix, String uri) {
		this.prefixUriMapping.put(prefix, uri);
	}

	public final boolean hasPrefix(String prefix) {
		return this.prefixUriMapping.containsKey(prefix);
	}

	public final String valueOf(String prefix) {
		return this.prefixUriMapping.get(prefix);
	}

	public final Namespace namespaceFor(String prefix) {
		String uri = valueOf(prefix);
		if (uri != null) {
			return Namespace.getNamespace(prefix, uri);
		}
		return Namespace.getNamespace("", "");
	}

	public final Namespace[] getAllNamespaces() {
		return prefixUriMapping.entrySet().stream()
			.map(e -> Namespace.getNamespace(e.getKey(), e.getValue()))
			.collect(Collectors.toList()).toArray(new Namespace[0]);
	}
}
