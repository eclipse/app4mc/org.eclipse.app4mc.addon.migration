/**
 ********************************************************************************
 * Copyright (c) 2019, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common.converter;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

/**
 * This class is responsible for converting the namespace of the AMALTHEA model.
 * It is implemented as a component factory that gets created for every
 * input/output version in ModelMigration.
 */
@Component(factory = ServiceConstants.NAMESPACE_CONVERTER_FACTORY)
public class NamespaceConverter extends AbstractConverter {

	BundleContext context;

	@Activate
	protected void activate(Map<String, Object> properties, BundleContext context) {
		super.activate(properties);
		this.context = context;
	}

	@Override
	public void convert(File targetFile, Map<File, Document> fileName2documentMap, List<ICache> caches) {

		ServiceReference<SessionLogger> serviceReference = null;
		SessionLogger logger = null;
		if (context != null) {
			// the factory is created on startup to be able to create NamespaceConverter per configuration
			// a SessionLogger is created per migration session
			// therefore we can not use @Reference in the factory, as the reference can't be satisfied before a session is started
			serviceReference = context.getServiceReference(SessionLogger.class);
			logger = context.getService(serviceReference);

			logger.info("Migration from {0} to {1} : Executing Namespace converter for model file : {2}",
					getInputModelVersion(), getOutputModelVersion(), targetFile.getName());
		}

		Document document = fileName2documentMap.get(targetFile);
		if (document == null) {
			return;
		}

		Element rootElement = document.getRootElement();

		ModelVersion inputVersion = ModelVersion.getModelVersion(getInputModelVersion());
		ModelVersion outputVersion = ModelVersion.getModelVersion(getOutputModelVersion());

		if (inputVersion != null && outputVersion != null) {
			HelperUtil.updateRootElementNamespaces(
					rootElement,
					inputVersion,
					outputVersion);
		} else if (logger != null) {
			logger.error("input model version {0} or output model version {1} are invalid!", getInputModelVersion(), getOutputModelVersion());
		}

		if (context != null) {
			context.ungetService(serviceReference);
		}
	}

}
