/**
 ********************************************************************************
 * Copyright (c) 2018-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common.converter;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.jdom2.Document;
import org.jdom2.Element;

public abstract class AbstractRootElementConverter extends AbstractConverter {

	protected static final Map<String, String> SUB_MODEL_REPLACEMENT_NAMES;
	static {
		Map<String, String> tmpMap = new HashMap<>();
		tmpMap.put("CommonElements", "commonElements");
		tmpMap.put("ComponentsModel", "componentsModel");
		tmpMap.put("ConfigModel", "configModel");
		tmpMap.put("ConstraintsModel", "constraintsModel");
		tmpMap.put("EventModel", "eventModel");
		tmpMap.put("HWModel", "hwModel");
		tmpMap.put("MappingModel", "mappingModel");
		tmpMap.put("OSModel", "osModel");
		tmpMap.put("PropertyConstraintsModel", "propertyConstraintsModel");
		tmpMap.put("StimuliModel", "stimuliModel");
		tmpMap.put("SWModel", "swModel");
		SUB_MODEL_REPLACEMENT_NAMES = Collections.unmodifiableMap(tmpMap);
	}

	/**
	 * This method is used to remove the default namespaces associated to the Root
	 * Element and also change the name of it -> so that it can be added as a
	 * sub-tag inside "Amalthea root element"
	 *
	 * @param rootElement Element This is the root element of the model file (where
	 *                    Amalthea is not root element)
	 */
	protected void updateCurrentRootTagProps(final Element rootElement) {

		HelperUtil.removeDefaultAttribs(rootElement);

		final String oldName = rootElement.getName();
		final String newName = SUB_MODEL_REPLACEMENT_NAMES.get(oldName);

		if (newName != null) {
			rootElement.setName(newName);
		}
	}

	protected void doConvert(final Document document) {
		if (document == null) return;

		final Element rootElement = document.getRootElement();
		final String rootTagName = rootElement.getName();

		if (!AMALTHEA.equals(rootTagName) && SUB_MODEL_REPLACEMENT_NAMES.containsKey(rootTagName)) {

			final Element newRootElement = new Element(AMALTHEA);

			HelperUtil.copyAllNameSpaces(rootElement, newRootElement);
			updateCurrentRootTagProps(rootElement);

			document.removeContent();

			newRootElement.addContent(rootElement);
			document.addContent(newRootElement);
		}
	}

}
