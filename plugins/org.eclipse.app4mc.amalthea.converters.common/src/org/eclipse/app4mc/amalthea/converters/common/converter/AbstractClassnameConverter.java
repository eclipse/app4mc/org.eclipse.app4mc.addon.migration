/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common.converter;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.jdom2.Document;

/**
 * Abstract implementation of an {@link IConverter} that renames classnames from
 * a previous model version to the current. Subclasses need to implement
 * {@link #getClassnameMapping()} to specify the mapping from old value to new
 * value.
 */
public abstract class AbstractClassnameConverter extends AbstractConverter {

	@Override
	public void convert(File targetFile, Map<File, Document> fileName2documentMap, List<ICache> caches) {
		// TODO Implement classname replace

	}

	/**
	 * Returns the mapping from old classname to new classname, which will be used
	 * to perform a global replace.
	 *
	 * @return The mapping from old classname to new classname.
	 */
	protected abstract Map<String, String> getClassnameMapping();

}
