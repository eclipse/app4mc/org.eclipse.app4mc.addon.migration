/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common.base;

import java.io.File;
import java.util.Map;

import org.jdom2.Document;

/**
 * Interface for cache implementations. The provided cache map contains the
 * information per file.
 */
public interface ICache {

	/**
	 * Build the cache from the provided {@link File} to {@link Document} mapping.
	 *
	 * @param fileDocumentMapping The mapping from {@link File} to {@link Document}
	 *                            that should be used to build up the cache.
	 */
	public void buildCache(Map<File, Document> fileDocumentMapping);

	/**
	 * Returns the cache map.
	 *
	 * @return The cache map that contains the cached objects per {@link File}.
	 */
	public Map<File, Map<String, Object>> getCacheMap();

	/**
	 * Clear the local cache map.
	 */
	public void clearCacheMap();
}
