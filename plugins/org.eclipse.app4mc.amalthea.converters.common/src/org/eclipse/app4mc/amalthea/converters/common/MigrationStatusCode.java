/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common;

public final class MigrationStatusCode {

	private MigrationStatusCode() {
		// empty default constructor
	}

	public static final int OK = 10;

	public static final int UNSUPPORTED_MODEL_VERSIONS = 20;

	public static final int ERROR = 30;

	public static final int CANCEL = 40;
}
