/**
 ********************************************************************************
 * Copyright (c) 2019-2024 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common.utils;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.jdom2.Namespace;

public final class AmaltheaNamespaceRegistry {

	private static final LinkedHashMap<String, AmaltheaNamespace> REGISTRY = new LinkedHashMap<>();
	private static final String GENERIC_NAMESPACE = "generic";

	static {
		AmaltheaNamespace generic = new AmaltheaNamespace();
		generic.registerMapping("xsi", "http://www.w3.org/2001/XMLSchema-instance");
		generic.registerMapping("xmi", "http://www.omg.org/XMI");
		REGISTRY.put(GENERIC_NAMESPACE, generic);

		AmaltheaNamespace ns070 = new AmaltheaNamespace();
		ns070.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.7.0");
		REGISTRY.put(ModelVersion.VERSION_070.getVersion(), ns070);

		AmaltheaNamespace ns071 = new AmaltheaNamespace();
		ns071.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.7.1");
		REGISTRY.put(ModelVersion.VERSION_071.getVersion(), ns071);

		AmaltheaNamespace ns072 = new AmaltheaNamespace();
		ns072.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.7.2");
		REGISTRY.put(ModelVersion.VERSION_072.getVersion(), ns072);

		AmaltheaNamespace ns080 = new AmaltheaNamespace();
		ns080.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.8.0");
		REGISTRY.put(ModelVersion.VERSION_080.getVersion(), ns080);

		AmaltheaNamespace ns081 = new AmaltheaNamespace();
		ns081.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.8.1");
		REGISTRY.put(ModelVersion.VERSION_081.getVersion(), ns081);

		AmaltheaNamespace ns082 = new AmaltheaNamespace();
		ns082.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.8.2");
		REGISTRY.put(ModelVersion.VERSION_082.getVersion(), ns082);

		AmaltheaNamespace ns083 = new AmaltheaNamespace();
		ns083.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.8.3");
		REGISTRY.put(ModelVersion.VERSION_083.getVersion(), ns083);

		AmaltheaNamespace ns090 = new AmaltheaNamespace();
		ns090.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.9.0");
		REGISTRY.put(ModelVersion.VERSION_090.getVersion(), ns090);

		AmaltheaNamespace ns091 = new AmaltheaNamespace();
		ns091.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.9.1");
		REGISTRY.put(ModelVersion.VERSION_091.getVersion(), ns091);

		AmaltheaNamespace ns092 = new AmaltheaNamespace();
		ns092.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.9.2");
		REGISTRY.put(ModelVersion.VERSION_092.getVersion(), ns092);

		AmaltheaNamespace ns093 = new AmaltheaNamespace();
		ns093.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.9.3");
		REGISTRY.put(ModelVersion.VERSION_093.getVersion(), ns093);

		AmaltheaNamespace ns094 = new AmaltheaNamespace();
		ns094.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.9.4");
		REGISTRY.put(ModelVersion.VERSION_094.getVersion(), ns094);

		AmaltheaNamespace ns095 = new AmaltheaNamespace();
		ns095.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.9.5");
		REGISTRY.put(ModelVersion.VERSION_095.getVersion(), ns095);

		AmaltheaNamespace ns096 = new AmaltheaNamespace();
		ns096.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.9.6");
		REGISTRY.put(ModelVersion.VERSION_096.getVersion(), ns096);

		AmaltheaNamespace ns097 = new AmaltheaNamespace();
		ns097.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.9.7");
		REGISTRY.put(ModelVersion.VERSION_097.getVersion(), ns097);

		AmaltheaNamespace ns098 = new AmaltheaNamespace();
		ns098.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.9.8");
		REGISTRY.put(ModelVersion.VERSION_098.getVersion(), ns098);

		AmaltheaNamespace ns099 = new AmaltheaNamespace();
		ns099.registerMapping("am", "http://app4mc.eclipse.org/amalthea/0.9.9");
		REGISTRY.put(ModelVersion.VERSION_099.getVersion(), ns099);

		AmaltheaNamespace ns100 = new AmaltheaNamespace();
		ns100.registerMapping("am", "http://app4mc.eclipse.org/amalthea/1.0.0");
		REGISTRY.put(ModelVersion.VERSION_100.getVersion(), ns100);

		AmaltheaNamespace ns110 = new AmaltheaNamespace();
		ns110.registerMapping("am", "http://app4mc.eclipse.org/amalthea/1.1.0");
		REGISTRY.put(ModelVersion.VERSION_110.getVersion(), ns110);

		AmaltheaNamespace ns120 = new AmaltheaNamespace();
		ns120.registerMapping("am", "http://app4mc.eclipse.org/amalthea/1.2.0");
		REGISTRY.put(ModelVersion.VERSION_120.getVersion(), ns120);

		AmaltheaNamespace ns200 = new AmaltheaNamespace();
		ns200.registerMapping("am", "http://app4mc.eclipse.org/amalthea/2.0.0");
		REGISTRY.put(ModelVersion.VERSION_200.getVersion(), ns200);

		AmaltheaNamespace ns210 = new AmaltheaNamespace();
		ns210.registerMapping("am", "http://app4mc.eclipse.org/amalthea/2.1.0");
		REGISTRY.put(ModelVersion.VERSION_210.getVersion(), ns210);

		AmaltheaNamespace ns220 = new AmaltheaNamespace();
		ns220.registerMapping("am", "http://app4mc.eclipse.org/amalthea/2.2.0");
		REGISTRY.put(ModelVersion.VERSION_220.getVersion(), ns220);
		
		AmaltheaNamespace ns300 = new AmaltheaNamespace();
		ns300.registerMapping("am", "http://app4mc.eclipse.org/amalthea/3.0.0");
		REGISTRY.put(ModelVersion.VERSION_300.getVersion(), ns300);
		
		AmaltheaNamespace ns310 = new AmaltheaNamespace();
		ns310.registerMapping("am", "http://app4mc.eclipse.org/amalthea/3.1.0");
		REGISTRY.put(ModelVersion.VERSION_310.getVersion(), ns310);

		AmaltheaNamespace ns320 = new AmaltheaNamespace();
		ns320.registerMapping("am", "http://app4mc.eclipse.org/amalthea/3.2.0");
		REGISTRY.put(ModelVersion.VERSION_320.getVersion(), ns320);

		AmaltheaNamespace ns330 = new AmaltheaNamespace();
		ns330.registerMapping("am", "http://app4mc.eclipse.org/amalthea/3.3.0");
		REGISTRY.put(ModelVersion.VERSION_330.getVersion(), ns330);
	}

	/**
	 *
	 * @param namespace The namespace for which the ModelVersion is searched.
	 * @return The {@link ModelVersion} for the given {@link Namespace}.
	 */
	public static ModelVersion getModelVersion(Namespace namespace) {
		for (ModelVersion version : ModelVersion.values()) {
			if (isNamespaceAvailable(version, namespace)) {
				return version;
			}
		}

		return null;
	}

	/**
	 *
	 * @param version The ModelVersion for the requested Namespace.
	 * @param prefix  The namespace prefix.
	 * @return The namespace for the given version and prefix.
	 */
	public static Namespace getNamespace(ModelVersion version, String prefix) {
		return getNamespace(version.getVersion(), prefix);
	}

	/**
	 *
	 * @param version The String representation of the ModelVersion for which the
	 *                Namespace is requested.
	 * @param prefix  The namespace prefix.
	 * @return The namespace for the given version and prefix.
	 */
	public static Namespace getNamespace(String version, String prefix) {
		AmaltheaNamespace ns = REGISTRY.get(version);
		if (ns != null) {
			return ns.namespaceFor(prefix);
		}
		return Namespace.getNamespace("", "");
	}

	public static Namespace[] getAllNamespacesFor(ModelVersion version) {
		AmaltheaNamespace ns = REGISTRY.get(version.getVersion());
		if (ns != null) {
			return ns.getAllNamespaces();
		}
		return new Namespace[] { Namespace.getNamespace("", "") };
	}

	public static Namespace[] getAllNamespacesBefore(ModelVersion version, boolean include, boolean includeGeneric) {
		ModelVersion[] versionsBefore = ModelVersion.getVersionsBefore(version, include);
		List<Namespace> collect = Arrays.stream(versionsBefore)
			.map(AmaltheaNamespaceRegistry::getAllNamespacesFor)
			.flatMap(Arrays::stream)
			.collect(Collectors.toList());

		if (includeGeneric) {
			AmaltheaNamespace generic = REGISTRY.get(GENERIC_NAMESPACE);
			if (generic != null) {
				collect.addAll(0, Arrays.asList(generic.getAllNamespaces()));
			}
		}

		if (!collect.isEmpty()) {
			return collect.toArray(new Namespace[0]);
		}

		return new Namespace[] { Namespace.getNamespace("", "") };
	}

	public static Namespace[] getAllNamespaces() {
		List<Namespace> collect = REGISTRY.entrySet().stream()
			.map(Map.Entry::getValue)
			.map(AmaltheaNamespace::getAllNamespaces)
			.flatMap(Arrays::stream)
			.collect(Collectors.toList());

		if (!collect.isEmpty()) {
			return collect.toArray(new Namespace[0]);
		}
		return new Namespace[] { Namespace.getNamespace("", "") };
	}

	public static boolean isNamespaceAvailable(ModelVersion version, Namespace namespace) {
		if (namespace != null) {
			String prefix = namespace.getPrefix();
			String uri = namespace.getURI();

			AmaltheaNamespace ns = REGISTRY.get(version.getVersion());
			if (ns != null && uri.equals(ns.valueOf(prefix))) {
				return true;
			}
		}

		return false;
	}

	public static boolean isPrefixForVersion(ModelVersion version, String prefix) {
		AmaltheaNamespace ns = REGISTRY.get(version.getVersion());
		return (ns != null && ns.hasPrefix(prefix));
	}

	public static Namespace getGenericNamespace(String prefix) {
		return getNamespace(GENERIC_NAMESPACE, prefix);
	}

	private AmaltheaNamespaceRegistry() {
		// empty constructor for helper class
	}
}
