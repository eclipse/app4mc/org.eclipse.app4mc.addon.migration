/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.base.IModelMigration;
import org.eclipse.app4mc.amalthea.converters.common.base.IPostProcessor;
import org.osgi.service.component.ComponentFactory;
import org.osgi.service.component.ComponentInstance;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(factory = ServiceConstants.MODEL_MIGRATION_FACTORY)
public class ModelMigration implements IModelMigration {

	private String inputModelVersion;
	private String outputModelVersion;

	@Reference
	List<ICache> caches;

	@Reference(target = "(component.factory=" + ServiceConstants.NAMESPACE_CONVERTER_FACTORY + ")")
	ComponentFactory<IConverter> factory;

	@Reference
	List<IConverter> converter;

	@Reference
	List<IPostProcessor> postProcessor;

	@Activate
	void activate(Map<String, Object> properties) {
		this.inputModelVersion = properties.get(ServiceConstants.INPUT_MODEL_VERSION_PROPERTY).toString();
		this.outputModelVersion = properties.get(ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY).toString();
	}

	@Override
	public List<ICache> getCaches() {
		return Collections.unmodifiableList(this.caches);
	}

	@Override
	public List<IConverter> getConverter() {
		ArrayList<IConverter> result = new ArrayList<>(this.converter);
		Collections.reverse(result);

		// add the namespace converter
		Dictionary<String, Object> properties = new Hashtable<>();
		properties.put(ServiceConstants.INPUT_MODEL_VERSION_PROPERTY, getInputModelVersion());
		properties.put(ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY, getOutputModelVersion());
		ComponentInstance<IConverter> newInstance = factory.newInstance(properties);
		IConverter namespaceConverter = newInstance.getInstance();

		result.add(namespaceConverter);

		return result;
	}

	@Override
	public List<IPostProcessor> getPostProcessor() {
		return Collections.unmodifiableList(this.postProcessor);
	}

	@Override
	public String getInputModelVersion() {
		return this.inputModelVersion;
	}

	@Override
	public String getOutputModelVersion() {
		return this.outputModelVersion;
	}

}
