/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;

public class MigrationInputFile {

	private String modelVersion = "";

	private boolean selectedFile;

	private File file;

	private File originalFile;

	private String projectRelativePath;

	private Document document;

	private boolean zipFile;

	/*- this field is used to mark ModelFiles, if they have different versions than other files */
	private boolean isVersionDifferent;

	private SessionLogger logger;

	public SessionLogger getSessionLogger() {
		return this.logger;
	}

	public void setSessionLogger(SessionLogger logger) {
		this.logger = logger;
	}

	public String getModelVersion() {
		return this.modelVersion;
	}

	public void setModelVersion(String modelVersion) {
		this.modelVersion = modelVersion;
	}

	public boolean isSelectedFile() {
		return this.selectedFile;
	}

	public void setSelectedFile(boolean isSelectedFile) {
		this.selectedFile = isSelectedFile;
	}

	public File getFile() {
		if (this.file == null && MigrationHelper.isZipFile(this.originalFile)) {
			try {
				this.file = MigrationHelper.temporaryUnzip(this.originalFile);
			} catch (IOException e) {
				if (logger != null) {
					logger.error("Failed to unzip model file {}", this.originalFile.getAbsolutePath(), e);
				}
			}
		}
		return this.file;
	}

	public void setFile(File file, File project) {
		if (MigrationHelper.isZipFile(file)) {
			this.zipFile = true;
		} else {
			this.zipFile = false;
			this.file = file;
		}
		this.originalFile = file;
		this.projectRelativePath = project.toURI().relativize(file.toURI()).getPath();
	}

	public String getProjectRelativePath() {
		return this.projectRelativePath;
	}

	public Document getDocument() {
		if (this.document == null) {
			try {
				this.document = HelperUtil.loadFile(getFile().getAbsolutePath(), logger);
			} catch (Exception e) {
				if (logger != null) {
					logger.error("Failed to load model file {}", getFile().getAbsolutePath(), e);
				}
			}
		}
		return this.document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public boolean isVersionDifferent() {
		return this.isVersionDifferent;
	}

	public void setVersionDifferent(boolean isVersionDifferent) {
		this.isVersionDifferent = isVersionDifferent;
	}

	public boolean isZipFile() {
		return zipFile;
	}

	public File getOriginalFile() {
		return originalFile;
	}

	public void dispose() {
		if (this.zipFile && this.file != null && this.file.exists()) {
			try {
				Files.delete(Paths.get(this.file.getAbsolutePath()));
			} catch (IOException e) {
				if (logger != null) {
					logger.error("Failed to delete temporary unzipped file {}", this.file.getAbsolutePath(), e);
				}
			}
		}
	}
}
