/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters.common;

/**
 * Constants used for APP4MC migration Declarative Services.
 */
public final class ServiceConstants {

	private ServiceConstants() {
		// private empty constructor for helper class
	}

	/**
	 * Service property key to define the input model version.
	 */
	public static final String INPUT_MODEL_VERSION_PROPERTY = "input_model_version";

	/**
	 * Service property key to define the output model version.
	 */
	public static final String OUTPUT_MODEL_VERSION_PROPERTY = "output_model_version";

	/**
	 * Factory identifier of the component factory for ModelMigration components.
	 */
	public static final String MODEL_MIGRATION_FACTORY = "org.eclipse.app4mc.amalthea.modelmigration.factory";

	/**
	 * Factory identifier of the component factory for NamespaceConverter components.
	 */
	public static final String NAMESPACE_CONVERTER_FACTORY = "org.eclipse.app4mc.amalthea.namespaceconverter.factory";
}
