
h3. Supported versions for model Migration

Model migration functionality provides a possibility to migrate the models (_created from previous releases of AMALTHEA_ ) to the latest versions.

_Only forward migration is supported_.

**AMALTHEA meta model versions information**

* Oldest version to migrate is **APP4MC 0.7.0**<br/><br/>Beginning with that version, the AMALTHEA meta model is a part of the official Eclipse project "APP4MC":https://www.eclipse.org/app4mc/.

**Model migration**

As described above, only forward migration is supported by the AMALTHEA model migration utility.

Model migration utility migrates the specified model sequentially to the next versions (step by step) till expected version is reached.

p{padding: 10px; background: #fafafa; border: 1px solid #888; border-radius: 5px}. _%{color:brown}**Hint for APP4MC 0.9.3 and newer:**<br/>Migration of Amalthea models belonging to legacy versions (ITEA "1.0.3, 1.1.0, 1.1.1") is no longer supported.%_

Below figure represents the steps involved in the migration of model from 0.7.0 version to APP4MC 0.8.1 version:

!(scale)../pictures/migration_flow.png!


