/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters100.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.9.9",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=1.0.0"
		},
		service = IConverter.class)

public class EventModelConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> fileDocumentMapping, List<ICache> caches) {
		logger.info("Migration from {0} to {1} : Executing EventModel converter for model file : {2}",
				getInputModelVersion(), getOutputModelVersion(), targetFile.getName());

		basicConvert(targetFile, fileDocumentMapping);
	}

	private void basicConvert(File file, Map<File, Document> map) {
		// get the components with interfaces
		final Document document = map.get(file);
		if (document == null) {
			return;
		}

		final Element rootElement = document.getRootElement();
		updateProcessEventType(rootElement);
	}

	private void updateProcessEventType(Element rootElement) {
		final Namespace am = AmaltheaNamespaceRegistry.getNamespace(getInputModelVersion(), "am");
		final Namespace xsi = AmaltheaNamespaceRegistry.getGenericNamespace("xsi");

		final String xpathStr = "./eventModel/events";

		final List<Element> events = HelperUtil.getXpathResult(rootElement, xpathStr, Element.class, am, xsi);

		if (events == null) {
			return;
		}

		// Remove deadline from ProcessEvent and ProcessChainEvent

		for (Element event : events) {
			String type = event.getAttributeValue("type", xsi);
			if ("am:ProcessEvent".equals(type) || "am:ProcessChainEvent".equals(type)) {
				Attribute attribute = event.getAttribute("eventType");
				if (attribute != null && "deadline".equals(attribute.getValue())) {
					logger.warn("{0}: Event type 'deadline' has been removed. 'deadline' is no longer supported in version {1}.",
							type, getOutputModelVersion());

					event.removeAttribute(attribute);
				}
			}
		}
	}

}
