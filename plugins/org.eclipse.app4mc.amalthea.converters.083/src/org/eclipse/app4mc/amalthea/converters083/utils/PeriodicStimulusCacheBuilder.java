/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters083.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.8.2")

public class PeriodicStimulusCacheBuilder implements ICache {

	public static final String CACHE_KEY = "PeriodicStimulus_Containing_Clock";

	private final HashMap<File, Map<String, Object>> map = new HashMap<>();

	@Reference
	SessionLogger logger;

	@Override
	public void buildCache(final Map<File, Document> fileDocumentMapping) {

		if (logger != null) {
			logger.info("Build up PeriodicStimulusCache for 0.8.2");
		}

		for (Entry<File, Document> entry : fileDocumentMapping.entrySet()) {
			File targetFile = entry.getKey();
			Document document1 = entry.getValue();

			if (document1 == null) {
				// log error message
				continue;
			}

			Element rootElement = document1.getRootElement();

			/*- Start : Cache initialization */

			Map<String, Object> fileCache = map.get(targetFile);
			if (fileCache == null) {
				fileCache = new HashMap<>();
				map.put(targetFile, fileCache);
			}

			ArrayList<String> periodicStimulusElementNames = new ArrayList<>();
			fileCache.put(CACHE_KEY, periodicStimulusElementNames);

			StringBuilder xpathBuilder = new StringBuilder();
			xpathBuilder.append("./stimuliModel/stimuli[@xsi:type=\"am:PeriodicStimulus\"]");
			List<Element> periodicStimulusElements = HelperUtil.getXpathResult(
					rootElement,
					xpathBuilder.toString(),
					Element.class,
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_083, "am"),
					AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			for (Element periodicStimusElement : periodicStimulusElements) {
				if((periodicStimusElement.getChild("clock")!=null)  || (periodicStimusElement.getAttribute("clock")!=null))  {
					String attributeValue = periodicStimusElement.getAttributeValue("name");
					if(attributeValue!=null) {
						periodicStimulusElementNames.add(attributeValue);
					}
				}
			}
		}
	}

	@Override
	public Map<File, Map<String, Object>> getCacheMap() {
		return this.map;
	}

	@Override
	public void clearCacheMap() {
		this.map.clear();
	}
}
