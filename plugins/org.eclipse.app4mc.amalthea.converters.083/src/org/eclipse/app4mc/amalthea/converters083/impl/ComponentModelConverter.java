/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters083.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * This class is responsible for converting the HW Model elements from 0.8.2 to 0.8.3 version format of AMALTHEA model
 *
 * @author zmeer
 *
 */
@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.8.2",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.8.3"},
		service = IConverter.class)

public class ComponentModelConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> fileName2documentMap, List<ICache> caches) {

		logger.info("Migration from 0.8.2 to 0.8.3 : Executing Component model converter for model file : {0}",
				targetFile.getName());


		final Document root = fileName2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		updateInterfacePort(rootElement);
	}

	/**
	 * This method is used to change the reference of FInterfacePort to InterfacePort , check : Bug 528932)
	 *
	 *
	 * @param rootElement
	 *            Amalthea root element
	 */
	private void updateInterfacePort(Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./componentsModel/components/ports");


		final List<Element> portElements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		//Setting xsi:type value of Port
		for (Element portElement : portElements) {
			Attribute typeAttribute = portElement.getAttribute("type", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			if(typeAttribute !=null) {
				String value = typeAttribute.getValue();
				if("am:FInterfacePort".equals(value)) {
					typeAttribute.setValue("am:InterfacePort");
				}
			}

		}

		/*========== updating references ==================*/

		final StringBuilder xpathBufferForReferences = new StringBuilder();

		xpathBufferForReferences.append("./componentsModel/components/groundedPorts");

		xpathBufferForReferences.append("|");

		xpathBufferForReferences.append("./componentsModel/components/connectors/sourcePort");

		xpathBufferForReferences.append("|");

		xpathBufferForReferences.append("./componentsModel/components/connectors/targetPort");

		xpathBufferForReferences.append("|");

		xpathBufferForReferences.append("./componentsModel/systems/groundedPorts");

		xpathBufferForReferences.append("|");

		xpathBufferForReferences.append("./componentsModel/systems/connectors/sourcePort");

		xpathBufferForReferences.append("|");

		xpathBufferForReferences.append("./componentsModel/systems/connectors/targetPort");



		final List<Element> elementsReferringPorts = HelperUtil.getXpathResult(
				rootElement,
				xpathBufferForReferences.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (Element elementReferringPort : elementsReferringPorts) {

			Attribute portAttribute = elementReferringPort.getAttribute("port");

			if(portAttribute==null) {

				Element portElement = elementReferringPort.getChild("port");

				if(portElement!=null) {

					/*
					 * This is the case where Port element from other file is referenced
					 * In this case, below scenarios should be considered:
					 * 1.type updation
					 * 2.reference href updation
					 */

					//type updation

					Attribute typeAttribute = portElement.getAttribute("type", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

					if(typeAttribute !=null) {
						String value = typeAttribute.getValue();
						if("am:FInterfacePort".equals(value)) {
							typeAttribute.setValue("am:InterfacePort");
						}
					}
					//href updation

					Attribute hrefAttribute = portElement.getAttribute("href");

					if(hrefAttribute!=null) {
						String value = hrefAttribute.getValue();

						value=value.replace("=FInterfacePort", "=InterfacePort");
						hrefAttribute.setValue(value);
					}
				}
			} else {
				//referring to local ports

				String value = portAttribute.getValue();

				value=value.replace("=FInterfacePort", "=InterfacePort");
				portAttribute.setValue(value);
			}
		}
	}
}
