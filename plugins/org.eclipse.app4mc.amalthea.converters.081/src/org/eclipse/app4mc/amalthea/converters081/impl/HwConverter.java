/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters081.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Parent;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * This class is responsible for converting the HW Model elements from 0.8.0 to 0.8.1 version format of AMALTHEA model
 *
 * @author mez2rng
 *
 */
@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.8.0",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.8.1"},
		service = IConverter.class)

public class HwConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {

		logger.info("Migration from 0.8.0 to 0.8.1 : Executing Hardware converter for model file : {0}",
				targetFile.getName());

		final Document root = filename2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		removeXAccessPattern(rootElement);

		updateQuartz(rootElement);
	}

	/**
	 * This method is used to move all Quartz elements to a single global list inside Hardware model i.e. directly inside HwSystem element (For further
	 * details, check : Bug 518069 )
	 *
	 *
	 * @param rootElement
	 *            Amalthea root element
	 */
	private void updateQuartz(Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();
		xpathBuffer.append("./hwModel/system//quartzes");

		final List<Element> hwSystemElements = HelperUtil.getXpathResult(
				rootElement,
				"./hwModel/system",
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		final List<Element> quartzElements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		Element hwSystemElement = null;

		if (!hwSystemElements.isEmpty()) {
			hwSystemElement = hwSystemElements.get(0);
		}

		for (Element quartzElement : quartzElements) {
			// removing content of quartz element from existing parent
			Parent parent = quartzElement.getParent();

			if (parent != null) {
				// In case of nested Quartz, elements would have been removed from their parents
				parent.removeContent(quartzElement);
			}

			// changing the tag name of Quartz
			quartzElement.setName("quartzes");

			// setting Quartz element to HwSystem
			if (hwSystemElement != null) {
				hwSystemElement.addContent(quartzElement);
			}

			Attribute nameAttribute = quartzElement.getAttribute("name");

			String quartzName = nameAttribute != null ? nameAttribute.getValue() : "";

			logger.info("Moved Quartz element {0} as a child element of HwSystem", quartzName);

			// removing sub-elements which are no longer valid as per 0.8.1
			boolean removedComponents = quartzElement.removeChild("components");
			if (removedComponents) {
				logger.warn("-- Removed all HwComponent objects defined inside Quartz : {0}", quartzName);
			}
			boolean removedMemories = quartzElement.removeChild("memories");

			if (removedMemories) {
				logger.warn("-- Removed all Memory objects defined inside Quartz : {0}", quartzName);
			}
			boolean removedNetworks = quartzElement.removeChild("networks");
			if (removedNetworks) {
				logger.warn("-- Removed all Network objects defined inside Quartz : {0}", quartzName);
			}
			boolean removedPorts = quartzElement.removeChild("ports");
			if (removedPorts) {
				logger.warn("-- Removed all HwPort objects defined inside Quartz : {0}", quartzName);
			}
			boolean removedPrescalers = quartzElement.removeChild("prescaler");
			if (removedPrescalers) {
				logger.warn("-- Removed all Prescaler objects defined inside Quartz : {0}", quartzName);
			}
			boolean removedQuartzes = quartzElement.removeChild("quartzes");

			if (removedQuartzes) {
				logger.warn("-- Moved all Quartz objects defined inside Quartz : {0} as elements inside HwSystem", quartzName);
			}
		}
	}

	/**
	 * This method is used for the migration of MemmoryType element present inside the Hardware model (For further
	 * details, check : Bug 518068 )
	 *
	 * As the metamodel change in 0.8.1: xAccessPattern attribute is removed from the MemoryType element
	 *
	 * @param rootElement
	 *            Amalthea root element
	 */
	private void removeXAccessPattern(final Element rootElement) {
		final List<Element> memoryTypeElements = HelperUtil.getXpathResult(
				rootElement,
				"./hwModel/memoryTypes",
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (final Element memoryTypeElement : memoryTypeElements) {
			memoryTypeElement.removeAttribute("xAccessPattern");

			logger.info("xAccessPattern attribute and its value are removed from MemoryType: {0}", memoryTypeElement.getAttributeValue("name"));
		}
	}

}
