/**
 ********************************************************************************
 * Copyright (c) 2015-2018 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters081.utils;

import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.jdom2.Element;

public final class HelperUtils081 {

	private HelperUtils081() {
		// empty default constructor
	}

	/**
	 * This method is used to add CustomProperty to the parent Element, with the supplied key and value
	 * @param parentElement Element for which CustomProperty shall be added to
	 * @param key
	 * @param value
	 */
	public static void addCustomProperty(Element parentElement, String key, String value){
		if(value == null){
			return ;
		}

		Element customPropertiesElement=new Element("customProperties");
		customPropertiesElement.setAttribute("key", key);
		Element valueElement=new Element("value");
		valueElement.setAttribute("type", "am:StringObject", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));
		valueElement.setAttribute("value", value);
		customPropertiesElement.addContent(valueElement);
		parentElement.addContent(customPropertiesElement);
	}
}
