/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters081.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters081.utils.HelperUtils081;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * This class is responsible for converting the OS Model elements from 0.8.0 to 0.8.1 version format of AMALTHEA model
 *
 * @author mez2rng
 *
 */
@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.8.0",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.8.1"},
		service = IConverter.class)

public class OSConverter extends AbstractConverter {

	private static final String KEY = "key";
	private static final String VALUE = "value";
	private static final String TYPE = "type";
	private static final String CUSTOM_PROPERTIES = "customProperties";
	private static final String SCHEDULE_UNIT_PRIORITY = "scheduleUnitPriority";

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {

		logger.info("Migration from 0.8.0 to 0.8.1 : Executing OS converter for model file : {0}",
				targetFile.getName());


		final Document root = filename2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		updateOSModel(rootElement);
	}

	/**
	 * This method is used to migrate the OS model data
	 *
	 * 1. AlgorithmParameter elements to ParameterExtension inside UserSpecificSchedulingAlgorithm : For further
	 * details, check : Bug 518070
	 *
	 * 2. Removal of SchedulingUnit from TaskScheduler and InterruptContoller elements. For further details check :
	 *
	 *
	 * @param rootElement
	 *            Amalthea root element
	 */
	private void updateOSModel(Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./osModel/operatingSystems/interruptControllers");
		xpathBuffer.append("|");
		xpathBuffer.append("./osModel/operatingSystems/taskSchedulers");


		final List<Element> sourceElements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (Element schedulerElement : sourceElements) {

			//Changing model based on : Bug 518070

			final List<Element> algorithmParameterElements = HelperUtil.getXpathResult(
					schedulerElement,
					"./schedulingAlgorithm[@xsi:type=\"am:UserSpecificSchedulingAlgorithm\"]/parameter",
					Element.class,
					AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

			 for (Element algorithmParameterElement : algorithmParameterElements) {
				 algorithmParameterElement.setName("parameterExtensions");
			}

			 /*====================== Changing model based on : Bug 519746 ======================*/

			//Adding customProperty "scheduleUnitPriority"

			String scheduleUnitPriority= schedulerElement.getAttributeValue(SCHEDULE_UNIT_PRIORITY);

			if(scheduleUnitPriority!=null && !"0".equals(scheduleUnitPriority)){
				HelperUtils081.addCustomProperty(schedulerElement, SCHEDULE_UNIT_PRIORITY, schedulerElement.getAttributeValue(SCHEDULE_UNIT_PRIORITY));
			}

			//removal of scheduleUnitPriority attribute
			schedulerElement.removeAttribute(SCHEDULE_UNIT_PRIORITY);

			Element schedulingUnitElement = schedulerElement.getChild("schedulingUnit");

			if(schedulingUnitElement !=null){

				logger.warn("SchedulingUnit removed from Scheduler : {0}", schedulerElement.getAttributeValue("name"));

				String schedulingUnitType = schedulingUnitElement.getAttributeValue(TYPE, AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

				if(schedulingUnitType !=null){

					if("am:SchedulingHWUnit".equals(schedulingUnitType)){

						moveCustomPropertiesOfSchedulingUnit(schedulerElement, schedulingUnitElement,"SchedulingHWUnit_CustomProperty__");

						//Adding customProperty "delay"
						Element delayElement = schedulingUnitElement.getChild("delay");
						if(delayElement!=null){
							delayElement.detach();

							Element customPropertiesElement=new Element(CUSTOM_PROPERTIES);

							customPropertiesElement.setAttribute(KEY, "SchedulingHWUnit___delay");

							delayElement.setName(VALUE);

							delayElement.setAttribute(TYPE, "am:TimeObject", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

							//adding as a value to CustomProperty
							customPropertiesElement.addContent(delayElement);

							//adding customProperty to Scheduler Eleemnt
							schedulerElement.addContent(customPropertiesElement);

							HelperUtils081.addCustomProperty(schedulerElement, "SchedulingHWUnit___delay", schedulerElement.getAttributeValue("delay"));
						}


					}else if("am:SchedulingSWUnit".equals(schedulingUnitType)){

						moveCustomPropertiesOfSchedulingUnit(schedulerElement, schedulingUnitElement,"SchedulingSWUnit_CustomProperty__");

						/*- fetching all Instruction elements and associating to RunnableInstructions */
						List<Element> instructionElements = schedulingUnitElement.getChildren("instructions");

						if (!instructionElements.isEmpty()) {
							logger.warn("-- Instructions inside SchedulingSWUnit are migrated to RunnableInstructions element");
						}

						for (Element instructionElement : instructionElements) {

							Element clone = instructionElement.clone();

							clone.setName("default");

							Element computationItemElement=new Element("computationItems");

							computationItemElement.setAttribute(TYPE, "am:RunnableInstructions", AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

							computationItemElement.addContent(clone);

							schedulerElement.addContent(computationItemElement);

						}

						//Adding customProperty "priority"

						String priority=schedulingUnitElement.getAttributeValue("priority");

						if (priority != null && !"0".equals(priority)) {
							HelperUtils081.addCustomProperty(schedulerElement, "SchedulingSWUnit___priority", priority);
						}

						// removing interruptController
						if (schedulingUnitElement.removeAttribute("interruptController") || schedulingUnitElement.removeChild("interruptController")) {
							logger.warn("-- InterruptController inside SchedulingSWUnit is removed, as there is no equivalent element for it in AMALTHEA 0.8.1");
						}
					}
				}

				/*- removing SchedulingHWUnit --*/

				schedulerElement.removeChild("schedulingUnit");
			}
		}
	}

	private void moveCustomPropertiesOfSchedulingUnit(Element schedulerElement, Element schedulingUnitElement, String prefix) {

		List<Element> customPropertyElements = schedulingUnitElement.getChildren(CUSTOM_PROPERTIES);

		for (Element customProperty : customPropertyElements) {

			Element cloneCustomProperty = customProperty.clone();

			Attribute keyAttribute = cloneCustomProperty.getAttribute(KEY);

			if(keyAttribute !=null){
				String value = keyAttribute.getValue();
				keyAttribute.setValue(prefix+value);
			}

			/*- adding CustomProperty to Scheduler */

			schedulerElement.addContent(cloneCustomProperty);
		}
	}
}
