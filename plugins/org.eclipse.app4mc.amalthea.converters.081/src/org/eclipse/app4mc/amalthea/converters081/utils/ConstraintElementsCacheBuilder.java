/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters081.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.amalthea.converters.common.utils.ModelVersion;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		property = ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.8.0")

public class ConstraintElementsCacheBuilder implements ICache {

	private final HashMap<File, Map<String, Object>> map = new HashMap<>();

	@Reference
	SessionLogger logger;

	@Override
	public void buildCache(final Map<File, Document> fileDocumentMapping) {

		if (logger != null) {
			logger.info("Build up ConstraintElementsCache for 0.8.0");
		}

		for (Entry<File, Document> entry : fileDocumentMapping.entrySet()) {
			File targetFile = entry.getKey();
			Document document = entry.getValue();

			if (document == null) {
				// log error message
				continue;
			}

			final Element rootElement = document.getRootElement();

			/*- Start : Cache initialization */

			Map<String, Object> fileCache = map.get(targetFile);
			if (fileCache == null) {
				fileCache = new HashMap<>();
				map.put(targetFile, fileCache);
			}

			ArrayList<String> rootEventChainNames = new ArrayList<>();

			fileCache.put(ConstraintElementsCacheEnum.ROOT_EVENTCHAIN_NAMES.name(), rootEventChainNames);

			/*- End : Cache initialization */

			List<Element> rootEventChainElements = HelperUtil.getXpathResult(
					rootElement,
					"./constraintsModel/eventChains",
					Element.class,
					AmaltheaNamespaceRegistry.getNamespace(ModelVersion.VERSION_070, "am"));

			/*- ================START: Collecting root level eventchain elements ======================== */

			for (Element element : rootEventChainElements) {
				String eventChainName = element.getAttributeValue("name");
				if (eventChainName != null) {
					rootEventChainNames.add(HelperUtil.encodeName(eventChainName));
				}
			}

			/*- ================END: Collecting root level eventchain elements ======================== */

		}
	}

	@Override
	public Map<File, Map<String, Object>> getCacheMap() {
		return this.map;
	}

	@Override
	public void clearCacheMap() {
		this.map.clear();
	}
}
