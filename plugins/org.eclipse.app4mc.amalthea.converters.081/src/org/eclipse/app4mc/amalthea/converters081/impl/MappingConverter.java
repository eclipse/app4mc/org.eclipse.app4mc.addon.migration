/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.converters081.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.converters.common.ServiceConstants;
import org.eclipse.app4mc.amalthea.converters.common.base.ICache;
import org.eclipse.app4mc.amalthea.converters.common.base.IConverter;
import org.eclipse.app4mc.amalthea.converters.common.converter.AbstractConverter;
import org.eclipse.app4mc.amalthea.converters.common.utils.AmaltheaNamespaceRegistry;
import org.eclipse.app4mc.amalthea.converters.common.utils.HelperUtil;
import org.eclipse.app4mc.util.sessionlog.SessionLogger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * This class is responsible for converting the Mapping Model elements from 0.8.0 to 0.8.1 version format of AMALTHEA model
 *
 * @author mez2rng
 *
 */
@Component(
		property = {
			ServiceConstants.INPUT_MODEL_VERSION_PROPERTY + "=0.8.0",
			ServiceConstants.OUTPUT_MODEL_VERSION_PROPERTY + "=0.8.1"},
		service = IConverter.class)

public class MappingConverter extends AbstractConverter {

	@Reference
	SessionLogger logger;

	@Override
	@Activate
	protected void activate(Map<String, Object> properties) {
		super.activate(properties);
	}

	@Override
	public void convert(File targetFile, Map<File, Document> filename2documentMap, List<ICache> caches) {

		logger.info("Migration from 0.8.0 to 0.8.1 : Executing Mapping converter for model file : {0}",
				targetFile.getName());

		final Document root = filename2documentMap.get(targetFile);

		if (root == null) {
			return;
		}
		final Element rootElement = root.getRootElement();

		updateCoreAllocation(rootElement);

		updateSchedulerAllocation(rootElement);
	}

	/**
	 * This method is used to migrate the MappingModel data (priority element from TaskAllocation should be moved to SchedulingParameters element: For further
	 * details, check : Bug 511284, 518070 )
	 *
	 *
	 * @param rootElement
	 *            Amalthea root element
	 */
	private void updateSchedulerAllocation(Element rootElement) {

		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./mappingModel/taskAllocation");

		final List<Element> taskAllocationElements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		boolean priorityAddedAsSchedulingParameter=false;

		for (Element taskAllocationElement : taskAllocationElements) {

			Attribute priorityAttribute = taskAllocationElement.getAttribute("priority");

			if(priorityAttribute !=null){

				String value = priorityAttribute.getValue();

				/*-- removing attribute based on the metamodel changes introduced in 0.8.1 --*/

				taskAllocationElement.removeAttribute(priorityAttribute);

				if(!"0".equals(value)){

					priorityAddedAsSchedulingParameter=true;

					Element schedulingParametersElement=new Element("schedulingParameters");

					schedulingParametersElement.setAttribute("priority", value);

					taskAllocationElement.addContent(schedulingParametersElement);
				}
			}
		}

		if(priorityAddedAsSchedulingParameter){
			logger.info("Priority is removed from TaskAllocation elements and added as a attribute in corresponding SchedulingParameters element");
		}
	}

	/**
	 * This method is used to migrate the MappingModel data (CoreAllocations elements to SchedulerAllocation : For further
	 * details, check : Bug 518070  )
	 *
	 *
	 * @param rootElement
	 *            Amalthea root element
	 */
	private void updateCoreAllocation(Element rootElement) {
		final StringBuilder xpathBuffer = new StringBuilder();

		xpathBuffer.append("./mappingModel/coreAllocation");

		final List<Element> coreAllocationElements = HelperUtil.getXpathResult(
				rootElement,
				xpathBuffer.toString(),
				Element.class,
				AmaltheaNamespaceRegistry.getGenericNamespace("xsi"));

		for (Element coreAllocationElement : coreAllocationElements) {

			coreAllocationElement.setName("schedulerAllocation");

			Attribute coreAttribute = coreAllocationElement.getAttribute("core");

			Element coreElement = coreAllocationElement.getChild("core");

			if(coreAttribute!=null){
				coreAttribute.setName("responsibility");
			}else if(coreElement!=null){
				coreElement.setName("responsibility");
			}
		}
	}

}
