/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.util.sessionlog.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.eclipse.app4mc.util.sessionlog.SessionLogEntry;
import org.eclipse.app4mc.util.sessionlog.SessionLogWriter;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.log.Logger;
import org.osgi.service.log.LoggerFactory;

/**
 * {@link SessionLogWriter} implementation that write the session log entries to a {@link File}.
 */
@Component
public class SessionLogFileWriter implements SessionLogWriter {

	LoggerFactory factory;
	Logger logger;

	@Override
	public void write(File sessionLogFile, List<SessionLogEntry> log) {
		Path parentPath = Paths.get(sessionLogFile.getAbsolutePath()).getParent();
		if (!Files.exists(parentPath)) {
			try {
				Files.createDirectories(parentPath);
			} catch (IOException e) {
				if (this.logger != null) {
					this.logger.error("Failed to create the output folder for the session log file", e);
				}
			}
		}

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(sessionLogFile))) {
			for (SessionLogEntry logEntry : log) {
				writer.append(logEntry.message);
				writer.newLine();
				if (logEntry.throwable != null) {
					writer.append(logEntry.getStackTrace());
					writer.newLine();
				}
			}
		} catch (IOException e) {
			if (this.logger != null) {
				this.logger.error("Failed to write session log file!", e);
			} else {
				// fallback if no LogService is available
				e.printStackTrace();
			}
		}
	}

	@Reference(cardinality = ReferenceCardinality.OPTIONAL, policy = ReferencePolicy.DYNAMIC)
	void setLogger(LoggerFactory factory) {
		this.factory = factory;
		this.logger = factory.getLogger(getClass());
	}

	void unsetLogger(LoggerFactory loggerFactory) {
		if (this.factory == loggerFactory) {
			this.factory = null;
			this.logger = null;
		}
	}

}
