
# Eclipse APP4MC - Migration

This is the repository of the Eclipse APP4MC migration component.

## Build

The project uses Maven/Tycho to build.

The build creates a P2 update site and an executable jar.  
The results are located in the folder _./releng/org.eclipse.app4mc.converters.product/target/_

From the root:

```
$ mvn clean verify
```
You need Maven 3.6.3 or higher to perform the POM-less Tycho build.

## License

[Eclipse Public License (EPL) v2.0][1]

[1]: https://www.eclipse.org/legal/epl-2.0/